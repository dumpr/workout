-- Adminer 4.2.0 MySQL dump

SET NAMES utf8mb4;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `workout` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `workout`;

DROP TABLE IF EXISTS `authassignment`;
CREATE TABLE `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` int(11) unsigned NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `userid` (`userid`),
  CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `authassignment_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `wo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin',	1,	NULL,	'N;');

DROP TABLE IF EXISTS `authitem`;
CREATE TABLE `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL COMMENT '0: operation, 1: task, 2: role',
  `description` text,
  `bizrule` text COMMENT 'holds an executable PHP code which returns false or true and ends with a semicolon',
  `data` text COMMENT 'string contains some serialized value(s)',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin',	2,	'',	NULL,	'N;'),
('adminPanel',	1,	'Доступ в админ панель',	NULL,	'N;'),
('guest',	2,	'',	NULL,	'N;'),
('RBAC',	1,	'Role-Based Access Control',	NULL,	'N;'),
('readUser',	0,	'read user',	NULL,	'N;');

DROP TABLE IF EXISTS `authitemchild`;
CREATE TABLE `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `authitemchild` (`parent`, `child`) VALUES
('admin',	'adminPanel'),
('adminPanel',	'RBAC');

DROP TABLE IF EXISTS `wo_comment`;
CREATE TABLE `wo_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `to` int(11) NOT NULL,
  `m` varchar(30) NOT NULL,
  `m_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `created` int(10) NOT NULL,
  `lang` varchar(6) NOT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `wo_comment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `wo_user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `wo_comment` (`id`, `user_id`, `to`, `m`, `m_id`, `text`, `created`, `lang`, `active`) VALUES
(1,	1,	0,	'page',	3,	'Молодцы!',	1424002520,	'ru',	0),
(2,	4,	1,	'page',	3,	'Сам ты молодец!',	1425001220,	'uk',	0),
(3,	4,	1,	'page',	3,	'Да ты тоже ничего так. В настройках шаблона есть шоткод: [edit_link] - УРЛ на редактирование ссылки. Используйте его, вместо него появится ссылка.',	1425000220,	'uk',	0),
(4,	4,	0,	'page',	3,	'Самопомощь рулёз',	1423992520,	'uk',	0),
(5,	1,	0,	'page',	3,	'Читатели же, видя это, чаще склоняются к старому доброму',	1424002600,	'ru',	0),
(6,	1,	0,	'page',	3,	'В начале поймём суть того, что же называют «поведением».\r\n\r\nВнутреннюю работу с поведениями обеспечивает класс CComponent, от которого так или иначе наследуются все классы в Yii. Поэтому не важно, с чем мы будем экспериментировать. Для начала стоит сказать, что в Yii имеется базовый класс поведения CBehavior, его наследник CModelBehavior и наследник второго уровня CActiveRecordBehavior, который дополнен обработчиками событий модели CActiveRecord. Но в первом примере это пока не важно.\r\n\r\nИтак, представим, что в нашем проекте есть несколько десятков моделей, и у пары из них нам нужно организовать поддержку отображения изображений. Не будем записывать здесь все стандартные методы модели.',	1424002700,	'ru',	0),
(7,	1,	2,	'page',	3,	'Популярная телеведущая и модель Ольга Фреймут отправилась на свой день рождения на роскошный курорт в Швейцарию.  Звезда выбрала для отпуска невероятно красивое место с видом на Женевское озеро и заснеженные Альпы. Именинница прекрасно отдохнула, и порадовала поклонников стильными образами и отличным настроением!',	1425231127,	'uk',	0),
(8,	1,	7,	'page',	3,	'Где армия зависит от народа, там рано или поздно оказывается, что правительство зависит от армии',	1425231391,	'uk',	0),
(9,	1,	3,	'page',	3,	'Известная модель Алена Шишкова похвасталась перед фанатами шикарным снимком, на котором звезда продемонстрировала своию шикарную фигуру в купальнике, во время фотосесси в море. Выглядит красотка просто неотразимо, за что и получила от восторженных поклонником просто море комплиментов.',	1425827882,	'uk',	0),
(10,	1,	0,	'page',	3,	'rdzg',	1425231842,	'uk',	0),
(11,	1,	10,	'page',	3,	'efsaeasfdsae',	1425231961,	'ru',	0),
(12,	1,	11,	'page',	3,	's',	1425232110,	'ru',	0),
(13,	8,	11,	'page',	3,	'efsefesz fzsefzs efsze fzsfdrx gxdrgxdr gxdrgdxr gxd gdrxgd rrdgxxd',	1425314128,	'uk',	0),
(16,	8,	6,	'page',	3,	'esfzsefzsefzse fzse fzse f',	1425321815,	'uk',	-1);

DROP TABLE IF EXISTS `wo_file`;
CREATE TABLE `wo_file` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `m` varchar(30) NOT NULL,
  `m_id` int(11) unsigned NOT NULL,
  `src_id` int(11) unsigned NOT NULL,
  `name` varchar(127) NOT NULL,
  `description` varchar(255) NOT NULL,
  `active` tinyint(3) unsigned NOT NULL,
  `order` int(11) NOT NULL,
  `time` int(10) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `view` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `src` (`src_id`),
  KEY `user` (`user_id`),
  CONSTRAINT `wo_file_ibfk_5` FOREIGN KEY (`src_id`) REFERENCES `wo_file_body` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wo_file_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `wo_user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `wo_file` (`id`, `m`, `m_id`, `src_id`, `name`, `description`, `active`, `order`, `time`, `user_id`, `view`) VALUES
(3,	'pageIco',	4,	3,	'0a2bfc73.fit886xNone.2ba06a.grassland_10_of_17_5YyndCl',	'',	0,	0,	1425493439,	1,	0),
(4,	'pageIco',	4,	4,	'_5eQWiFKckI',	'',	0,	0,	1425493463,	1,	0),
(5,	'pageIco',	4,	5,	'4YJ25dKkZjU',	'',	0,	0,	1425493601,	1,	0),
(6,	'pageIco',	4,	6,	'1Efds5lfyw0',	'',	1,	0,	1425494049,	1,	0),
(7,	'pageIco',	11,	7,	'1tX6wza5We0',	'',	1,	0,	1425494076,	1,	0),
(8,	'pageIco',	3,	8,	'4kAyfN3YOX8',	'',	1,	0,	1425494080,	1,	0),
(9,	'page',	4,	6,	'1Efds5lfyw0',	'какой то текст',	0,	3,	1425494127,	1,	0),
(10,	'page',	4,	7,	'1tX6wza5We0',	'просто так текст',	0,	2,	1425494180,	1,	0),
(11,	'page',	4,	8,	'4kAyfN3YOX8',	'альтернатива',	0,	1,	1425494185,	1,	0),
(12,	'page',	4,	5,	'112223333 44444 555555',	'какая то текстовая строка',	1,	5,	1425494188,	1,	0),
(13,	'page',	4,	9,	'8_VAMNTTVNE',	'витамины',	0,	0,	1425494526,	1,	0),
(14,	'page',	4,	6,	'1Efds5lfyw0',	'где то',	1,	3,	1425494693,	1,	0),
(15,	'page',	4,	7,	'1tX6wza5We0',	'русским',	1,	1,	1425494696,	1,	0),
(16,	'page',	4,	10,	'git',	'ысысыусыусяыс',	0,	0,	1425495197,	1,	0),
(17,	'page',	4,	11,	'Idiorm',	'',	0,	0,	1425495795,	1,	0),
(18,	'page',	4,	11,	'Idiorm',	'',	0,	0,	1425496237,	1,	0),
(19,	'page',	4,	11,	'Idiorm',	'',	0,	0,	1425496489,	1,	0),
(20,	'page',	4,	11,	'Idiorm',	'крутто',	0,	1,	1425496588,	1,	0),
(21,	'page',	4,	12,	'github-git-cheat-sheet',	'шпаргалка по гиту',	1,	0,	1425497342,	1,	0),
(22,	'page',	4,	11,	'Idiorm',	'пик',	1,	4,	1425497483,	1,	0),
(23,	'page',	4,	13,	'jQuerySelectors',	'селекторы джейквери',	1,	2,	1425547236,	1,	0),
(24,	'page',	3,	14,	'_MG_4539',	'',	0,	0,	1425836065,	1,	0),
(25,	'page',	3,	15,	'_MG_4542',	'',	0,	0,	1425836087,	1,	0),
(26,	'page',	3,	16,	'_MG_4540',	'',	0,	0,	1425836096,	1,	0),
(27,	'page',	3,	17,	'_MG_4543',	'',	0,	0,	1425836105,	1,	0),
(28,	'page',	3,	18,	'_MG_4544',	'',	0,	0,	1425836115,	1,	0),
(29,	'page',	3,	19,	'_MG_4545',	'выступление на 8-е марта',	1,	0,	1425836123,	1,	0);

DROP TABLE IF EXISTS `wo_file_body`;
CREATE TABLE `wo_file_body` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `src` varchar(40) NOT NULL,
  `size` int(11) NOT NULL,
  `ext` varchar(8) NOT NULL,
  `is_image` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `wo_file_body` (`id`, `src`, `size`, `ext`, `is_image`) VALUES
(3,	'cc/22/48a69c027a37de3f0b2e3e7d2cd6',	229050,	'jpg',	1),
(4,	'7b/c4/016129061b811408800a90c11380',	397533,	'jpg',	1),
(5,	'fb/9f/24105322550e1f7bb485dc66451c',	222913,	'jpg',	1),
(6,	'd9/6b/3aaf722cb63813e9ac855f1ed39d',	361592,	'jpg',	1),
(7,	'2e/f8/4bd83b1ee038d32c4496fa5250a6',	345868,	'jpg',	1),
(8,	'5f/6c/2a253d0da273d67c506e0eb9a8d7',	241709,	'jpg',	1),
(9,	'3d/c2/9838c66a3eb86892dddeb1caeb5b',	419833,	'jpg',	1),
(10,	'93/f8/cbad7175812006cc2b15c07b5c69',	2165,	'txt',	0),
(11,	'66/86/f72b53339de8aa69bec87d45286a',	178484,	'pdf',	0),
(12,	'f1/f8/a2cf00c9b9765f30ca904281290e',	377767,	'pdf',	0),
(13,	'8b/f0/8623e3cca1bfbb69b7dade614454',	12277,	'xlsx',	0),
(14,	'38/84/3e95b0015e3d770c127e14bbf90a',	984426,	'jpg',	1),
(15,	'79/55/44bcd7ba6ff42efe70f0e78175c3',	916786,	'jpg',	1),
(16,	'2f/0a/5cef5691a5dadf6a54cf7673281e',	982091,	'jpg',	1),
(17,	'6a/e8/423d97df30a06a2f5af7ac2a93b0',	997311,	'jpg',	1),
(18,	'2d/65/fe10504f458c38bbec0db25bd4e5',	924986,	'jpg',	1),
(19,	'1c/de/cae2e433c4d6608c001dfd4a94a2',	996993,	'jpg',	1);

DROP TABLE IF EXISTS `wo_file_lang`;
CREATE TABLE `wo_file_lang` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` int(11) unsigned NOT NULL,
  `lang_id` varchar(6) NOT NULL,
  `l_description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `file_id` (`file_id`),
  CONSTRAINT `wo_file_lang_ibfk_2` FOREIGN KEY (`file_id`) REFERENCES `wo_file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `wo_file_lang` (`id`, `file_id`, `lang_id`, `l_description`) VALUES
(1,	21,	'uk',	'шпаргалки по гіту тощо'),
(2,	15,	'uk',	'українською'),
(3,	14,	'uk',	'подекуди'),
(4,	22,	'uk',	'пік'),
(5,	12,	'uk',	'якась текстова стрічка'),
(6,	23,	'uk',	'селектори джейквері'),
(7,	24,	'uk',	''),
(8,	25,	'uk',	''),
(9,	26,	'uk',	''),
(10,	27,	'uk',	''),
(11,	28,	'uk',	''),
(12,	29,	'uk',	'виступ на восьме березня');

DROP TABLE IF EXISTS `wo_file_more`;
CREATE TABLE `wo_file_more` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `src_id` int(11) unsigned NOT NULL,
  `lat` decimal(12,8) NOT NULL,
  `lon` decimal(12,8) NOT NULL,
  `make` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `created` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  KEY `id` (`id`),
  KEY `src` (`src_id`),
  CONSTRAINT `wo_file_more_ibfk_1` FOREIGN KEY (`src_id`) REFERENCES `wo_file_body` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `wo_file_more` (`id`, `src_id`, `lat`, `lon`, `make`, `model`, `created`, `height`, `width`) VALUES
(1,	14,	0.00000000,	0.00000000,	'Canon',	'Canon EOS DIGITAL REBEL XS',	1425568051,	1536,	2304),
(2,	15,	0.00000000,	0.00000000,	'Canon',	'Canon EOS DIGITAL REBEL XS',	1425568203,	1536,	2304),
(3,	16,	0.00000000,	0.00000000,	'Canon',	'Canon EOS DIGITAL REBEL XS',	1425568053,	1536,	2304),
(4,	17,	0.00000000,	0.00000000,	'Canon',	'Canon EOS DIGITAL REBEL XS',	1425568209,	1536,	2304),
(5,	18,	0.00000000,	0.00000000,	'Canon',	'Canon EOS DIGITAL REBEL XS',	1425568258,	1536,	2304),
(6,	19,	0.00000000,	0.00000000,	'Canon',	'Canon EOS DIGITAL REBEL XS',	1425568260,	1536,	2304);

DROP TABLE IF EXISTS `wo_page`;
CREATE TABLE `wo_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL,
  `user` int(11) unsigned NOT NULL,
  `alias` varchar(127) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(30) NOT NULL,
  `anons` varchar(1023) NOT NULL,
  `content` text NOT NULL,
  `note` varchar(1023) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `show_child` tinyint(1) NOT NULL DEFAULT '1',
  `menu` tinyint(1) NOT NULL DEFAULT '0',
  `sitemap` tinyint(1) NOT NULL DEFAULT '1',
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page_alias` (`alias`),
  KEY `user` (`user`),
  CONSTRAINT `wo_page_ibfk_1` FOREIGN KEY (`user`) REFERENCES `wo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `wo_page` (`id`, `parent`, `user`, `alias`, `name`, `short_name`, `anons`, `content`, `note`, `created`, `modified`, `active`, `order`, `path`, `show_child`, `menu`, `sitemap`, `title`, `description`, `keywords`) VALUES
(3,	0,	1,	'blog',	'Блог',	'Блог',	'',	'etdsgvb',	'',	'2015-01-23 21:22:51',	'2015-03-08 17:36:03',	1,	0,	'',	1,	1,	1,	'',	'',	''),
(4,	0,	1,	'test-article',	'Тестовая статья',	'Тестовая статья',	'',	'',	'',	'2015-01-25 19:02:09',	'2015-03-05 12:15:24',	1,	1,	'',	1,	1,	1,	'',	'',	''),
(11,	0,	1,	'pogolovkene',	'Поголовкене',	'Поголовкене',	'',	'<h1>HTML Ipsum Presents</h1>\r\n	       \r\n<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href=\"#\">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>\r\n\r\n<h2>Header Level 2</h2>\r\n	       \r\n<ol>\r\n   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>\r\n   <li>Aliquam tincidunt mauris eu risus.</li>\r\n</ol>\r\n\r\n<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>\r\n\r\n<h3>Header Level 3</h3>\r\n\r\n<ul>\r\n   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>\r\n   <li>Aliquam tincidunt mauris eu risus.</li>\r\n</ul>\r\n\r\n<pre><code>\r\n#header h1 a { \r\n	display: block; \r\n	width: 300px; \r\n	height: 80px; \r\n}\r\n</code></pre>',	'',	'2015-02-23 19:31:52',	'2015-03-04 18:34:44',	1,	2,	'',	1,	1,	1,	'',	'',	''),
(12,	3,	1,	'testovaya-statya-777',	'Тестовая статья',	'Тестовая статья (копия)',	'',	'',	'',	'2015-02-23 19:59:19',	'2015-02-24 07:17:03',	1,	0,	'blog',	1,	0,	1,	'',	'',	''),
(13,	3,	1,	'second',	'Вторая из статей',	'Вторая из статей',	'',	'',	'',	'2015-02-25 21:29:00',	'2015-02-25 19:31:23',	1,	0,	'blog',	1,	0,	1,	'',	'',	'');

DROP TABLE IF EXISTS `wo_page_lang`;
CREATE TABLE `wo_page_lang` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `lang_id` varchar(6) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `l_short_name` varchar(30) NOT NULL,
  `l_anons` varchar(1023) NOT NULL,
  `l_content` text NOT NULL,
  `l_title` varchar(255) NOT NULL,
  `l_description` varchar(255) NOT NULL,
  `l_keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `page_id` (`page_id`),
  CONSTRAINT `wo_page_lang_ibfk_2` FOREIGN KEY (`page_id`) REFERENCES `wo_page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `wo_page_lang` (`id`, `page_id`, `lang_id`, `l_name`, `l_short_name`, `l_anons`, `l_content`, `l_title`, `l_description`, `l_keywords`) VALUES
(1,	11,	'uk',	'Назва',	'',	'',	'<h1>HTML Ipsum Presents (Українська)</h1>\r\n	       \r\n<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href=\"#\">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>\r\n\r\n<h2>Header Level 2</h2>\r\n	       \r\n<ol>\r\n   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>\r\n   <li>Aliquam tincidunt mauris eu risus.</li>\r\n</ol>\r\n\r\n<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>\r\n\r\n<h3>Header Level 3</h3>\r\n\r\n<ul>\r\n   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>\r\n   <li>Aliquam tincidunt mauris eu risus.</li>\r\n</ul>\r\n\r\n<pre><code>\r\n#header h1 a { \r\n	display: block; \r\n	width: 300px; \r\n	height: 80px; \r\n}\r\n</code></pre>',	'Pellentesque',	'dolor',	'fermentum'),
(2,	3,	'uk',	'Блог',	'',	'Тарифи на електрику зростуть на 5% - 50%',	'<p class=\"story-body__introduction\">Від 1 квітня тарифи для різних категорій споживачів зростуть на 5%-50% - таким є рішення Національної комісії, що здійснює регулювання у сфері електроенергетики та комунальних послуг. Підвищення тарифів триватимуть і у 2016-2017 роках.</p><p>Середнє квітневе підвищення тарифів становитиме 19%.</p><p>До 2017 року тарифи на світло зростуть у 3,5 рази.</p><p>Своє рішення НКРЕКП обґрунтовує тим, що нині \"населення оплачує лише 21,3 % фактичних витрат на виробництво, передачу та постачання електричної енергії\", тоді як промисловість платить більше. Через це, на думку регулятора, ціни на товари та послуги постійно зростають. А відтак, населення оплачує ринкові тарифи опосередковано через ціну товарів.</p><p>У комісії також розраховують, що вже перший етап підвищення тарифів має принести додаткові 5,4 млрд грн.</p><p>Підвищення тарифів буде різним для різних категорій споживачів, залежно від обсягів споживання електроенергії.</p><p>Ті, хто за місяць споживає менше 100 кВт/год., платитимуть на 19% більше - 37 копійок за 1кВт/год.</p><p>Друга група споживачів, яка споживає від 101 до 600 кВт/год., з 1 квітня платитиме на 50% більше - 63 копійки за 1кВт/год.</p><p>Ті, хто споживає понад 600 кВт/год., сплачуватимуть на 5% більше, ніж нині - 1,4 грн за 1 кВт/год.</p><p>У НКРЕКП підрахували, що загалом, після підвищення тарифів 1 квітня, у грошовому вимірі споживачі платитимуть:</p><p>на 3 грн більше, якщо споживають до 50 кВт/год.</p><p>на 6 грн більше, якщо споживають до 100 кВт/год.</p><p>на 22 грн більше, якщо споживають до 150 кВт/год.</p>',	'',	'',	''),
(3,	4,	'uk',	'Тестова статья',	'',	'',	'',	'',	'',	'');

DROP TABLE IF EXISTS `wo_param`;
CREATE TABLE `wo_param` (
  `par` varchar(16) NOT NULL,
  `name` varchar(127) NOT NULL,
  `type` varchar(16) DEFAULT 'input',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `value` text,
  `label` varchar(64) DEFAULT NULL,
  `description` varchar(511) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `wo_param` (`par`, `name`, `type`, `hidden`, `value`, `label`, `description`) VALUES
('main',	'email_contact',	'input',	0,	'',	'E-mail на который отправлять письма с сайта',	'E-mail адрес (или адреса через запятую) для отправки писем с формы обратной связи (страница Контакты или пр.)'),
('main',	'email_public',	'input',	0,	'',	'E-mail, который виден на сайте',	'Адрес, который будет виден на сайте, указываться в письмах.'),
('main',	'phone_fax',	'input',	0,	'',	'Номер факса',	'Отображается на сайте и в письмах.'),
('main',	'index_page_description',	'input',	0,	'',	'Мета тег description для главной страницы',	NULL),
('main',	'index_page_keywords',	'input',	0,	'',	'Мета тег keywords для главной страницы',	NULL),
('main',	'index_page_title',	'input',	0,	'',	'Мета тег title для главной страницы',	NULL),
('main',	'phone_public',	'input',	0,	'',	'Номер телефона',	'Отображается на сайте и в письмах.'),
('main',	'site_description',	'area',	0,	'',	'Описание сайта',	'Несколько предложений о миссии сайта. Будет показано на главной, под названием сайта'),
('main',	'site_name',	'input',	0,	'',	'Название сайта',	NULL),
('main',	'skype',	'input',	0,	'',	'Skype',	NULL),
('main',	'vk',	'input',	0,	'',	'Группа Вконтакте',	'полный адрес страницы с http://vk.com/');

DROP TABLE IF EXISTS `wo_user`;
CREATE TABLE `wo_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `activation_key` varchar(255) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `last_visit_on` datetime NOT NULL,
  `password_set_on` datetime NOT NULL,
  `email_verified` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `one_time_password_secret` varchar(255) DEFAULT NULL,
  `one_time_password_code` varchar(255) DEFAULT NULL,
  `one_time_password_counter` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username_idx` (`username`),
  UNIQUE KEY `user_email_idx` (`email`),
  KEY `user_email_verified_idx` (`email_verified`),
  KEY `user_is_active_idx` (`is_active`),
  KEY `user_is_disabled_idx` (`is_disabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `wo_user` (`id`, `username`, `password`, `email`, `firstname`, `lastname`, `phone`, `activation_key`, `created_on`, `updated_on`, `last_visit_on`, `password_set_on`, `email_verified`, `is_active`, `is_disabled`, `one_time_password_secret`, `one_time_password_code`, `one_time_password_counter`) VALUES
(1,	'admin',	'$2y$10$juVIsqOUMISHTLP7wjTYM.bbGEgOFQYl/7q5DJkIi1KE2NLnVsmzi',	'2bemate@gmail.com',	'Андрей',	'Яременко',	'0669060050',	'01fa20d2144ae9b3914514911985fb94',	'2015-01-10 14:17:50',	'2015-01-11 19:29:02',	'2015-03-08 23:32:45',	'2015-01-10 14:17:50',	1,	1,	0,	NULL,	NULL,	0),
(4,	'bemate',	'$2y$10$z2Kf8qagbZXPKHug0yxyk.CtOjkPTE3kpiwSuV02573ry0NX4uUgK',	'1osk@mail.ru',	'Иван',	'Иванов',	'',	'2f337197c756fa8f0cdefefbcf66f811',	'2015-01-15 22:53:55',	'0000-00-00 00:00:00',	'2015-01-23 15:04:12',	'2015-01-15 22:58:27',	1,	1,	0,	NULL,	NULL,	0),
(5,	'id1425313034',	'$2y$10$hSfowzY7H8ZMbzXEXiHmUurw8su7eupJpvWpddsZlo4j9DoGLn.Gu',	'asfsadf@szfe.ri',	'adw',	'awer',	'',	'a8badd06a5561d723e971499c9f2755f',	'2015-03-02 18:17:14',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	0,	0,	NULL,	NULL,	0),
(6,	'id1425313614',	'$2y$10$XwKCTLJf5vcyEATCNVkboOI0VUhn.wgRozftsscFCai85/0HPFGVO',	'zsef@feszsef.eu',	'szef',	'szef',	'',	'f2285be11b82df43497101c1cd61eac4',	'2015-03-02 18:26:54',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	0,	0,	NULL,	NULL,	0),
(7,	'id1425313714',	'$2y$10$ZWOpgmGDX/GJTLQo7/Wjxuv.ZBl2OKFgwOkRg3OKFNbF3WA0GyN/q',	'zsef@szfezse.ri',	'szef',	'szef',	'',	'76a29f7759e87748a4058de3f9cb5c8d',	'2015-03-02 18:28:34',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	0,	0,	NULL,	NULL,	0),
(8,	'id1425314126',	'$2y$10$0SQQbG2Cj2ttzeOo7V5sBOuibpt8N/fYmPoNCPPgJmJS5.R8eTv7W',	'zsef@szgeszg.ru',	'sezf',	'szef',	'',	'76fd4b3515606c5890e40bb927212d9a',	'2015-03-02 18:35:26',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	1,	0,	1,	NULL,	NULL,	0);

DROP TABLE IF EXISTS `wo_user_agent`;
CREATE TABLE `wo_user_agent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_agent` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `wo_user_agent` (`id`, `user_agent`) VALUES
(1,	'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Iron/30.0.1650.0  Chrome/30.0.1650.0 Safari/537.36'),
(2,	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36'),
(3,	'Mozilla/5.0 (Windows NT 6.1; rv:37.0) Gecko/20100101 Firefox/37.0'),
(4,	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Iron/35.0.1900.0 Chrome/35.0.1900.0 Safari/537.36'),
(5,	'Mozilla/5.0 (Windows NT 6.1; rv:35.0) Gecko/20100101 Firefox/35.0'),
(6,	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36 OPR/27.0.1689.69'),
(7,	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Iron/30.0.1650.0  Chrome/30.0.1650.0 Safari/537.36');

DROP TABLE IF EXISTS `wo_user_login_attempt`;
CREATE TABLE `wo_user_login_attempt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `username` varchar(255) NOT NULL,
  `performed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_successful` tinyint(1) NOT NULL DEFAULT '0',
  `session_id` varchar(255) DEFAULT NULL,
  `ipv4` int(11) DEFAULT NULL,
  `user_agent` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wo_user_login_attempts_user_id_idx` (`user_id`),
  KEY `user_agent` (`user_agent`),
  CONSTRAINT `wo_user_login_attempt_ibfk_2` FOREIGN KEY (`user_agent`) REFERENCES `wo_user_agent` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `wo_user_login_attempt` (`id`, `user_id`, `username`, `performed_on`, `is_successful`, `session_id`, `ipv4`, `user_agent`) VALUES
(3,	0,	'admin',	'2015-01-10 10:16:33',	0,	'5q710j468qjdo3qe27ema2r8b5',	2130706433,	1),
(4,	0,	'фвьшт',	'2015-01-10 10:18:13',	0,	'5q710j468qjdo3qe27ema2r8b5',	2130706433,	1),
(5,	1,	'admin',	'2015-01-10 10:18:27',	1,	'5q710j468qjdo3qe27ema2r8b5',	2130706433,	1),
(6,	1,	'admin',	'2015-01-10 10:19:45',	0,	'5q710j468qjdo3qe27ema2r8b5',	2130706433,	1),
(7,	1,	'admin',	'2015-01-10 10:20:14',	1,	'5q710j468qjdo3qe27ema2r8b5',	2130706433,	1),
(8,	1,	'admin',	'2015-01-10 10:51:22',	1,	'c9e9o4a9n82n2n915qgdlkeo67',	2130706433,	1),
(9,	1,	'admin',	'2015-01-10 13:13:09',	1,	'vd3laf7c5590j07k64l5st71c6',	2130706433,	1),
(10,	1,	'admin',	'2015-01-11 12:15:29',	1,	'8r1s2dsp153a2o6iafjo4158i7',	2130706433,	1),
(11,	1,	'admin',	'2015-01-11 13:40:36',	1,	'oftqvc950l356gtj25beeh6aj1',	2130706433,	1),
(12,	0,	'066 906 0050',	'2015-01-11 14:36:23',	0,	'tvu384i60v5rkjo5ao1tqhepa1',	2130706433,	1),
(13,	0,	'066 sac906 0050sd',	'2015-01-11 14:36:38',	0,	'tvu384i60v5rkjo5ao1tqhepa1',	2130706433,	1),
(14,	0,	'066 sac906 0050sd',	'2015-01-11 14:36:52',	0,	'tvu384i60v5rkjo5ao1tqhepa1',	2130706433,	1),
(15,	0,	'066 sac906 0050sd',	'2015-01-11 14:38:05',	0,	'tvu384i60v5rkjo5ao1tqhepa1',	2130706433,	1),
(16,	0,	'sac906 0050sd',	'2015-01-11 14:46:35',	0,	'tvu384i60v5rkjo5ao1tqhepa1',	2130706433,	1),
(17,	0,	'0669060050',	'2015-01-11 14:47:01',	0,	'tvu384i60v5rkjo5ao1tqhepa1',	2130706433,	1),
(18,	0,	'0669060050',	'2015-01-11 14:48:03',	0,	'tvu384i60v5rkjo5ao1tqhepa1',	2130706433,	1),
(19,	1,	'admin',	'2015-01-11 14:48:15',	1,	'tvu384i60v5rkjo5ao1tqhepa1',	2130706433,	1),
(20,	0,	'66 90 600 50',	'2015-01-11 15:12:18',	0,	'9g5pj7genbr2dt4g2h0ire12t7',	2130706433,	1),
(21,	1,	'(066) 90-600-50',	'2015-01-11 15:12:35',	1,	'9g5pj7genbr2dt4g2h0ire12t7',	2130706433,	1),
(22,	1,	'0669060050',	'2015-01-11 15:19:26',	1,	'c6lje768cqf2qjlvcbim7n8np2',	2130706433,	1),
(23,	1,	'2bemate@gmail.com',	'2015-01-11 15:38:10',	1,	'597glq1ivf458mnjle0p2n0bg0',	2130706433,	1),
(24,	1,	'admin',	'2015-01-11 16:14:12',	1,	'71f4coiu4kg4rotpomh84ueil6',	2130706433,	1),
(25,	1,	'admin',	'2015-01-13 12:40:50',	1,	'8k4uavemc1kmgps0n15ka95630',	2130706433,	1),
(26,	1,	'admin',	'2015-01-13 12:50:30',	0,	'94a8dl117uickec05b8pq88qj6',	2130706433,	1),
(27,	1,	'admin',	'2015-01-13 12:50:37',	1,	'94a8dl117uickec05b8pq88qj6',	2130706433,	1),
(28,	1,	'admin',	'2015-01-14 06:35:37',	1,	'c1em91mobuc8ijlap5nmfhun27',	2130706433,	1),
(29,	1,	'admin',	'2015-01-14 10:47:04',	1,	'fitjiuga7boh3d0fvs24ta8ok0',	2130706433,	1),
(30,	1,	'admin',	'2015-01-14 12:44:40',	1,	'2bugjf2h3j864augmd29e8fo74',	2130706433,	1),
(31,	1,	'admin',	'2015-01-14 14:31:26',	1,	'ul61usp63ca81kjn381nla4d37',	2130706433,	1),
(32,	1,	'admin',	'2015-01-14 15:57:20',	1,	'8elrbpif0r558ltdimaes7ur25',	2130706433,	1),
(33,	1,	'admin',	'2015-01-15 04:33:24',	1,	'cop2ggipkql4kjj9dqss5c0mo7',	2130706433,	1),
(34,	1,	'admin',	'2015-01-15 10:03:53',	1,	'tsdk73678op87ag6ip5l0vkfk3',	2130706433,	1),
(35,	1,	'admin',	'2015-01-15 10:08:13',	1,	'anleffuossqueu39albt4su1n2',	2130706433,	1),
(36,	1,	'admin',	'2015-01-15 10:11:08',	1,	'mo9bmhbjf0vhgi6qn3ggbj06f5',	2130706433,	1),
(37,	1,	'admin',	'2015-01-15 10:15:59',	1,	'7j42j9cimv5c9utdr2eqn0r1s3',	2130706433,	1),
(38,	1,	'admin',	'2015-01-15 10:50:17',	1,	'7j42j9cimv5c9utdr2eqn0r1s3',	2130706433,	1),
(39,	1,	'admin',	'2015-01-15 10:50:41',	1,	'7j42j9cimv5c9utdr2eqn0r1s3',	2130706433,	1),
(40,	1,	'admin',	'2015-01-15 11:45:23',	1,	'0nmvrlqdhnpaa4e4vnhospu2e4',	2130706433,	1),
(41,	1,	'admin',	'2015-01-15 15:43:11',	1,	'4rehb217ouiddpnb8e86tpjki4',	2130706433,	1),
(42,	4,	'bemate',	'2015-01-15 18:56:03',	0,	'dfdln4l76fiujrf5k3teu3f3q2',	2130706433,	1),
(43,	4,	'bemate',	'2015-01-15 18:58:27',	1,	'dfdln4l76fiujrf5k3teu3f3q2',	2130706433,	1),
(44,	1,	'admin',	'2015-01-16 11:07:00',	1,	'1ru8905suev1ilbna9smeva3n0',	2130706433,	1),
(45,	1,	'admin',	'2015-01-19 15:11:15',	1,	'pvsjabvjp2salfu46lfv26r647',	2130706433,	1),
(46,	1,	'admin',	'2015-01-23 10:33:08',	1,	't11rpggv8gcoasufrshautnbo1',	2130706433,	1),
(47,	4,	'1osk@mail.ru',	'2015-01-23 10:53:45',	0,	'spga649mpe2hfe61gpvl3r6m82',	2130706433,	1),
(48,	4,	'1osk@mail.ru',	'2015-01-23 10:53:53',	1,	'spga649mpe2hfe61gpvl3r6m82',	2130706433,	1),
(49,	1,	'admin',	'2015-01-23 11:04:32',	1,	'0rojsa35ihpbrh3a8ilrlk1255',	2130706433,	1),
(50,	1,	'admin',	'2015-01-25 08:08:27',	1,	'3vou62g12n0koapm6vsch25811',	2130706433,	1),
(51,	1,	'admin',	'2015-01-25 13:18:29',	1,	'7fraffhnearhovvn72t83k54j2',	2130706433,	1),
(52,	1,	'admin',	'2015-01-26 04:51:12',	1,	'81f645nhh4uuj59k8hnm09b8l0',	2130706433,	1),
(53,	1,	'admin',	'2015-02-01 14:16:33',	1,	'84qbm56r84e7i9gtu83mk1h8k3',	2130706433,	2),
(54,	1,	'admin',	'2015-02-02 10:41:11',	1,	'ocsblafq75m41uslnq7e0q4nh4',	2130706433,	2),
(55,	1,	'admin',	'2015-02-02 14:28:40',	1,	'b8ucuuu4m4c461vk65v8covok6',	2130706433,	2),
(56,	1,	'admin',	'2015-02-03 05:59:21',	1,	'4bsscsvphbjiii9teo4l75kj07',	2130706433,	2),
(57,	1,	'admin',	'2015-02-03 07:03:20',	1,	'2hlrui29ppch476un3ngb2k5a7',	2130706433,	3),
(58,	1,	'admin',	'2015-02-03 15:56:28',	1,	'kb94tcc8499neuoh7q5dcradc6',	2130706433,	4),
(59,	1,	'admin',	'2015-02-03 17:09:01',	1,	'n68pfa1gke0qqud3r8655k0r07',	2130706433,	5),
(60,	1,	'admin',	'2015-02-04 05:03:35',	1,	'o4mb4c4es0m5jmr7unk4r2a7f7',	2130706433,	4),
(61,	1,	'admin',	'2015-02-04 07:04:03',	1,	'2smt95o1f99hqd8rsvp6vbo820',	2130706433,	5),
(62,	1,	'admin',	'2015-02-04 11:21:20',	1,	'orn35cbmjc5257to213pnhere5',	2130706433,	5),
(63,	1,	'admin',	'2015-02-04 14:25:24',	1,	'vd686q8ktm1lqbglt7m4p8pca1',	2130706433,	5),
(64,	1,	'admin',	'2015-02-04 19:56:15',	1,	'ptt6efsp4eig86llqopq2ki185',	2130706433,	4),
(65,	1,	'admin',	'2015-02-09 04:04:52',	1,	'5a0knjaoq53ea295vt4jplt166',	2130706433,	4),
(66,	1,	'admin',	'2015-02-11 08:24:41',	1,	'54s07hbue7co6o9i8kujjbgjc2',	2130706433,	5),
(67,	1,	'admin',	'2015-02-11 13:38:33',	1,	'tin4ga5645k4gduf89f8shgqm1',	2130706433,	5),
(68,	1,	'admin',	'2015-02-11 17:06:14',	1,	'3s07s047hua7mh07235j64v636',	2130706433,	5),
(69,	0,	'+38 066 906-00-50',	'2015-02-11 18:25:04',	0,	'dps6k17ki317733pmq2ubd5lm1',	2130706433,	5),
(70,	1,	'066 906-00-50',	'2015-02-11 18:25:27',	1,	'dps6k17ki317733pmq2ubd5lm1',	2130706433,	5),
(71,	0,	'+380669060050',	'2015-02-12 06:01:52',	0,	'h28h6d4lv14kjobc6i9efcj864',	2130706433,	5),
(72,	0,	'+380669060050',	'2015-02-12 06:11:44',	0,	'h28h6d4lv14kjobc6i9efcj864',	2130706433,	5),
(73,	0,	'+380669060050',	'2015-02-12 06:13:08',	0,	'h28h6d4lv14kjobc6i9efcj864',	2130706433,	5),
(74,	1,	'+380669060050',	'2015-02-12 06:14:16',	1,	'h28h6d4lv14kjobc6i9efcj864',	2130706433,	5),
(75,	1,	'+38 066 906-00-50',	'2015-02-12 10:42:35',	1,	'uulnqlb8meshigr3bps56vqv04',	2130706433,	5),
(76,	1,	'admin',	'2015-02-12 17:07:48',	1,	'ngtt8i2f2b2a4s8f28ds1hqbi7',	2130706433,	5),
(77,	1,	'admin',	'2015-02-12 18:30:07',	1,	'm6lpotk3dc4badep9si7vo3163',	2130706433,	5),
(78,	1,	'admin',	'2015-02-12 19:02:52',	0,	'a4qt9aekcmdb6731457jilnh94',	2130706433,	6),
(79,	1,	'admin',	'2015-02-12 19:02:58',	1,	'a4qt9aekcmdb6731457jilnh94',	2130706433,	6),
(80,	1,	'admin',	'2015-02-12 19:23:28',	1,	'rec4qmvdfl99ct0jv8o1r2o1s5',	2130706433,	7),
(81,	1,	'admin',	'2015-02-23 14:11:16',	1,	'g2pjd13l8j7ndncpanprsc2h94',	2130706433,	7),
(82,	1,	'admin',	'2015-03-02 06:40:04',	1,	'uudg5ocsluu9t0t9k4rim1vs96',	2130706433,	7),
(83,	1,	'admin',	'2015-03-02 16:56:08',	1,	'cljqcasi3gg0m2r2efft4kbli5',	2130706433,	7),
(84,	1,	'admin',	'2015-03-02 16:57:02',	1,	'q274aem597h4oe258t3vghbtr1',	2130706433,	7),
(85,	1,	'admin',	'2015-03-02 16:57:57',	1,	'q274aem597h4oe258t3vghbtr1',	2130706433,	7),
(86,	1,	'admin',	'2015-03-03 07:31:05',	0,	'38t90vejeckgigke1ii4oe1sn7',	2130706433,	7),
(87,	1,	'admin',	'2015-03-03 07:31:17',	1,	'38t90vejeckgigke1ii4oe1sn7',	2130706433,	7);

DROP TABLE IF EXISTS `wo_user_remote_identity`;
CREATE TABLE `wo_user_remote_identity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `provider` varchar(100) NOT NULL,
  `identifier` varchar(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_used_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `wo_user_remote_identities_provider_identifier_idx` (`provider`,`identifier`),
  KEY `wo_user_remote_identities_user_id_idx` (`user_id`),
  CONSTRAINT `wo_user_remote_identity_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `wo_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `wo_user_used_password`;
CREATE TABLE `wo_user_used_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `password` varchar(255) NOT NULL,
  `set_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `wo_user_used_passwords_user_id_idx` (`user_id`),
  CONSTRAINT `wo_user_used_password_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `wo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `wo_user_used_password` (`id`, `user_id`, `password`, `set_on`) VALUES
(1,	1,	'$2y$10$juVIsqOUMISHTLP7wjTYM.bbGEgOFQYl/7q5DJkIi1KE2NLnVsmzi',	'2015-01-10 10:17:50'),
(4,	4,	'$2y$10$r3pP.lKNT7gGWc9.kA1zbeEhyZC8CMpUN/ie1HSioNEl56L44miK6',	'2015-01-15 18:53:56'),
(5,	4,	'$2y$10$z2Kf8qagbZXPKHug0yxyk.CtOjkPTE3kpiwSuV02573ry0NX4uUgK',	'2015-01-15 18:58:27');

-- 2015-03-08 21:39:14
