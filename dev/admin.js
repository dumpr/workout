fd.jQuery();

var allowedExts = ['jpg','jpeg','png','gif','pdf','zip','rar','xls','xlsx','doc','docx','txt'];

$('#adm').on('click','.ico_holder i',function(){
    $.post('admin/default/fileDelete',{id:$(this).parent().prev().val()});
    $(this).parent().prev().val('');
    $(this).parent().find("img,i").remove();
}).on('click','#result-message .fa-times',function(){
    $(this).parent().remove();
});

function pageFilesAttache(m,id,url){
    var file_ext;
    if(url==null) url = 'upload?m='+m+'&m_id='+id+'&preset=pageAttached';
var parent = $("#"+m+"Files_"+id);
var holder = $(parent).prev().find(".attache_holder");
$('<div class="attache_loader"></div>')
    .appendTo(parent)
    .filedrop({multiple: true})
    .on('fdsend', function (e, files) {
        $.each(files, function (i, file) {
            file_ext = file.name.split(".").slice(-1).toString().toLowerCase();
            if($.inArray(file_ext,allowedExts) == -1){
                $('<div class="mess-thumb"><img src="img/file.png"><b>'+file.name+'</b> Загрузка файлов <b>.'+file_ext+'</b> не поддерживается.<br /><a href="" onClick="$(this).parent().remove();return false">Отменить</a></div>').prependTo(holder);
            } else {
                $('<div class="mess-thumb" data-up-file-name="'+file.name+'"><i class="fa fa-spinner fa-spin"></i><b>'+file.name+'</b> загружается <a href="" onClick="$(this).parent().remove();return false">Отменить</a></div>').prependTo(holder);
                file.sendTo('admin/'+m+'/'+url);
            }
        });
    })
    .on('filedone', function (e, file, xhr) {
        var response = jQuery.parseJSON(xhr.responseText);
        $(holder).find('[data-up-file-name="'+response.basename+'"]').remove();
        $(response.html).prependTo(holder).on('click','.fa-times',function(){
            $(this).parent().remove();
        });
    });
}

function icoLoader(m,id,url){
    if(url==null) url = 'upload?m='+m+'Ico&m_id='+id+'&preset=pageIco';
    var image_holder = $("td[id="+m+"Ico_"+id+"] .ico_holder");
    $(image_holder).filedrop({multiple: false})
    .on('fdsend', function (e, files) {
        $.each(files, function (i, file) {
            var file_ext = file.name.split(".").slice(-1).toString().toLowerCase();
            if($.inArray(file_ext,allowedImgExts) == -1){
                resMessage('Это должна быть картинка.');
            } else {
                $(image_holder).find("img,i").remove();
                $('<i class="fa fa-spinner fa-spin"></i>').appendTo(image_holder);
                file.sendTo('admin/'+m+'/'+url);
            }
        });
    })
    .on('filedone', function (e, file, xhr) {
        $(image_holder).find("img,i").remove();
        var response = jQuery.parseJSON(xhr.response);

        if(xhr.statusText=="OK"){
            $('<i class="fa fa-times"></i><img src="'+response.src+'" />').appendTo(image_holder);
            $("td[id="+m+"Ico_"+id+"] > input:hidden").val(response.id);
        } else resMessage('При загрузке произощла ошибка.');
    });
}

function paramSubmit(){
    //var fields = serializeEls($('#paramsForm tbody'));
    var fields = $('#paramForm').serializeArray();
    $.ajax("admin/param/update", {
        method: 'POST',
        data: fields,
        success: function(response) {
            $.fn.yiiGridView.update("paramGrid");
            resMessage('Изменения сохранены',1); // +response
        }
    });
    return false;
}

function gridShowChildren(id){
    $.fn.yiiGridView.update("pageGrid",{ data: { parent:id }});
}

function delePreviewIco(id){
    $('.podcategory_icon_preview_'+id).html('');
    $('.page_ico_'+id).val('');
}

// http://pastie.org/4411132
function serializeEls(wr){
    var toReturn = [];
	var els = $(wr).find(':input').get();
    var serialized = $(els).serializeArray();
    $.map(serialized, function(n, i) {
        toReturn[n['name']] = encodeURIComponent(n['value']);
    });
    return toReturn;
}

function gridedit(sb){
    $(sb).attr('disabled','disabled');
    var m = getm(sb);
    var fields = $(sb).closest('form')
                        .find(" input[name='selected[]']:checked" )
                        .map(function(){return this.value}).get();
    $.each(fields, function( i, id ) {
            fillTabs(id,m);
    });
    $(sb).removeAttr('disabled');
}

function fillTabs(id,m){
    //$('#'+m+'Tab .tabs li:last-child a').html('<i class="fa fa-spinner fa-spin"></i>');
    $.post('admin/'+m+'/update?id='+id).done(function(data, textStatus, jqXHR){
        var title = $(data).find('span.title').text();
        newtab4edit(m+'Tab',id,title,data);
        //resMessage('Изменения сохранены - '+textStatus,1);
        return data;
    }).fail(function(jqXHR, textStatus, errorThrown){
        resMessage('Произошла ошибка - '+errorThrown);
    });
}

function copyTab(m,oldId){
    $.post( 'admin/default/gridupdate', { model:m, 'ids[]':[''], 'selected[]':[oldId], docopy:true }, function( data ) {
    $.each(data, function( i, id ) {
            fillTabs(id,m);
        });
    }, "json");
    $.fn.yiiGridView.update(m+"Grid");
}

function gridcopy(sb){
    var g = preFormData(sb);
    $.post( g[1], g[0], function( data ) {
        $.each(data, function( i, id ) {
            console.log(id,g[2]);
            fillTabs(id,g[2]);
        });
        $.fn.yiiGridView.update(g[2]+"Grid");
    }, "json");
}

function getm(sb){
    var formId = $(sb).closest('.yiiTab').attr('id');
    return formId.substring(0,formId.length-3);
}

function preFormData(sb){
    $(sb).attr('disabled','disabled');
    var form = $(sb).closest('form');
    //var url = $(form).attr('action');
    var formData = $(form).serializeArray();
    var m = getm(sb);

    var wdo = $(sb).attr('name');
    formData.push({ name: 'model', value: m });
    formData.push({ name: $(sb).attr('name'), value: true });

    return [formData,'admin/default/gridupdate',m];
}

function gridelete(sb){
	if (confirm('Вы действительно хотите удалить выделенные объекты?')){
	   gridupdate(sb);
	}
}
function singledelete(sb){
    if (confirm('Вы действительно хотите удалить данный объект?')){
        var m = getm(sb);
        mu = m[0].toUpperCase() + m.slice(1);
        var id = $(sb).closest('form').find('#'+mu+'_id').val();
        $.post('admin/default/deleteone',{'m':mu,'id':id}).done(function(data, textStatus, jqXHR){
            $.fn.yiiGridView.update(m+"Grid");
            $(sb).removeAttr('disabled');
            resMessage(data,1);

            var id = $(sb).closest('div.form').parent().attr('id');
            var newid = $('#'+id).prev().attr('id');
            $('#'+id).remove();
            $('a[href=#'+id+']').parent().remove();
            showPrevTab(newid);
            // return data;
        }).fail(function(jqXHR, textStatus, errorThrown){
            resMessage(errorThrown);
        });
    }
}

function gridupdate(sb){
    var g = preFormData(sb);
    $.post(g[1],g[0]).done(function(data, textStatus, jqXHR){
        $.fn.yiiGridView.update(g[2]+"Grid");
        $(sb).removeAttr('disabled');
        resMessage(data,1);

        var time_mem = $("#"+g[2]+"listTab").find('span.time_mem').text();
        $('#time_mem').text(time_mem);

    }).fail(function(jqXHR, textStatus, errorThrown){
        resMessage(errorThrown);
    });
}

function selectParent(parent,curr){
    jQuery.ajax({
        'url':'admin/Page/tree?parent='+parent+'&curr='+curr,
        'cache':false,
        'success':function(html){
            jQuery("#pages_tree_"+curr).replaceWith(html);
        }
    });
    return false;
}

function selectAllCh(i){
    var checkboxes = $(i).closest('form').find('input[name^=selected]').prop('checked', $(i).prop('checked'));
}

function showPrevTab(n){
    $('#'+n).show();
    $('a[href=#'+n+']').addClass('active');
}

function noclone(cont,id){
    var undefinedTab = $('#'+cont+'Tab_'+id).html();
    if(typeof undefinedTab !== 'undefined'){
        $('#'+cont+'Tab li a').removeAttr('class');
        $('#'+cont+'Tab li a[href=#'+cont+'Tab_'+id+']').addClass('active');
        $('#'+cont+'Tab .view').hide();
        $('#'+cont+'Tab_'+id).show();
        //$('.tabs a[href=#'+cont+'Empty]').html();
        return;
    }
    //$('#'+cont+'Tab .tabs li:last-child a').html('<i class="fa fa-spinner fa-spin"></i>');
}

function closeTab(m){
    var id = $(m).closest('div.form').parent().attr('id');
    var newid = $('#'+id).prev().attr('id');
    $('#'+id).remove();
    $('a[href=#'+id+']').parent().remove();
    showPrevTab(newid);
}

function closeTabTitle(b){
    var toclose = $(b).next();
    var id = $(toclose).attr('href');
    var newid = $(id).prev().attr('id');
    $(id).remove();
    $('a[href='+id+']').parent().remove();
    if($(toclose).hasClass('active'))
        showPrevTab(newid);
}

// http://stackoverflow.com/questions/7657816/ckeditor-ajax-issue
/*function UpdateCKEditors() {
    if(typeof CKEDITOR !== 'undefined'){
        for (var i in CKEDITOR.instances) {
            CKEDITOR.instances[i].updateElement();
        }
    }
}*/

function saveTab(m,id){
    //UpdateCKEditors();
    var data = $('#'+m+'Form_'+id).serialize();
    $.ajax({
        type: 'POST',
        url: 'admin/'+m+'/update?id='+id,
        data: data,
        success: function(data){
            var ident = $(data).find('form').attr('id');
            ident = ident.replace("Form", "Tab"); // replaceWith
            $('#'+m+'Tab_'+id).attr("id",ident);

            $('#'+ident).html(data);
            var time_mem = $('#'+ident).find('span.time_mem').text();
            $('#time_mem').text(time_mem);

            var title = $(data).find('input[name$="[name]"]').val();
            $('a[href="#'+m+'Tab_'+id+'"]').attr('href','#'+ident).text(title);
            resMessage('Изменения сохранены.',1);
            $.fn.yiiGridView.update(m+"Grid");
            //$(window).scrollTop();
        },
        error: function(data) {
            resMessage(data.responseText);
        },
        dataType:'html'
    });
}

function newtab4new(controller,data){
    var i = $(data).find('form').attr('id');
    var id = i.substr(i.length - 5);
    newtab4edit(controller,id,'new',data);
}

function newtab4edit(controller,id,name,data){
    var undefinedTab = $('.tabs a[href=#'+controller+'_'+id+']').html();
    if(typeof undefinedTab !== 'undefined'){
        $('.tabs a[href=#'+controller+'Empty]').html();
        return;
    }
    $('#'+controller+'>div.view').hide();
    $('#'+controller+'Empty').before('<div class="view" id="'+controller+'_'+id+'">'+data+'</div>');
    $('ul.tabs').find('a').removeAttr('class');
    $('#'+controller+' > ul.tabs li:last-child')
        .before('<li><i class="close" onclick="closeTabTitle(this);">×</i><a href="#'+controller+'_'+id+'" class="active">'+name+'</a></li>');
    var time_mem = $('#'+controller+'_'+id).find('span.time_mem').text();
    $('#time_mem').text(time_mem);
}
function modalClose(){
    $('#modal,#overlay').html('').hide();
}
function resMessage(message,ok){
    $('#result-message').remove();
    var st = "background-color:#4A7A4D;color:#663333";
    if(ok) st = "background-color:#ECFBED;color:#4D764E";
    //<i class="fa fa-times"></i>
    var messBlock = '<div id="result-message" style="'+st+'">'+message+'</div>';
    $(messBlock).appendTo('#adm').fadeIn(200).delay(3000).fadeOut(200);
}