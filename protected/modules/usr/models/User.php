<?php

class User extends CActiveRecord
{
	public $fullname;
	/**
	 * @inheritdoc
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		// password is unsafe on purpose, assign it manually after hashing only if not empty
		return array(
			array('username, email, phone, firstname, lastname, fullname, is_active, is_disabled, last_visit_on', 'filter', 'filter' => 'trim'),
			array('username', 'length', 'min'=>4),
			array('activation_key, created_on, updated_on, last_visit_on, password_set_on, email_verified', 'filter', 'filter' => 'trim', 'on' => 'search'),
			// username, email, firstname, lastname,
			array('is_active, is_disabled, email_verified', 'default', 'setOnEmpty' => true, 'value' => null),
			array('activation_key, created_on, updated_on, last_visit_on, password_set_on, email_verified', 'default', 'setOnEmpty' => true, 'value' => null, 'on' => 'search'),
			array('username, email, is_active, is_disabled, email_verified', 'required', 'except' => 'search'),
			array('created_on, updated_on, last_visit_on, password_set_on', 'date', 'format' => array('yyyy-MM-dd', 'yyyy-MM-dd HH:mm', 'yyyy-MM-dd HH:mm:ss'), 'on' => 'search'),
			array('activation_key', 'length', 'max'=>128, 'on' => 'search'),
			array('is_active, is_disabled, email_verified', 'boolean'),
			array('username, email', 'unique', 'except' => 'search'),
		);
	}

	/**
	 * @inheritdoc
	 */
	public function relations()
	{
		return array(
			'userLoginAttempts' => array(self::HAS_MANY, 'UserLoginAttempt', 'user_id', 'order'=>'performed_on DESC'),
			//'userProfilePictures' => array(self::HAS_MANY, 'UserProfilePicture', 'user_id'),
			'userRemoteIdentities' => array(self::HAS_MANY, 'UserRemoteIdentity', 'user_id'),
			'userUsedPasswords' => array(self::HAS_MANY, 'UserUsedPassword', 'user_id', 'order'=>'set_on DESC'),
		);
	}

	public function scopes()
	{
		//
		return array(
			'safe'=>array(
				'select' => '`t`.id,
						`t`.username,
						`t`.firstname,
						`t`.lastname,
						`t`.phone,
						`t`.email',
			),
		);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('models', 'ID'),
			'username' => Yii::t('models', 'Username'),
			'password' => Yii::t('models', 'Password'),
			'email' => Yii::t('models', 'Email'),
			'phone' => Yii::t('models', 'Phone'),
			'firstname' => Yii::t('models', 'Firstname'),
			'lastname' => Yii::t('models', 'Lastname'),
			'fullname' => Yii::t('models', 'Fullname'),
			'phone' => Yii::t('models', 'Phone'),
			'activation_key' => Yii::t('models', 'Activation Key'),
			'created_on' => Yii::t('models', 'Created On'),
			'updated_on' => Yii::t('models', 'Updated On'),
			'last_visit_on' => Yii::t('models', 'Last Visit On'),
			'password_set_on' => Yii::t('models', 'Password Set On'),
			'email_verified' => Yii::t('models', 'Email Verified'),
			'is_active' => Yii::t('models', 'Is Active'),
			'is_disabled' => Yii::t('models', 'Is Disabled'),
			'one_time_password_secret' => Yii::t('models', 'One Time Password Secret'),
			'one_time_password_code' => Yii::t('models', 'One Time Password Code'),
			'one_time_password_counter' => Yii::t('models', 'One Time Password Counter'),
		);
	}


	public function fullname()
	{
		return $this->lastname.' '.$this->firstname;
	}
	/**
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
                if($this->fullname){
                    $criteria->addCondition('firstname LIKE "%'.$this->fullname.'%"','OR');
                    $criteria->addCondition('lastname LIKE "%'.$this->fullname.'%"','OR');
                }
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('updated_on',$this->updated_on,true);
		$criteria->compare('last_visit_on',$this->last_visit_on,true);
		$criteria->compare('password_set_on',$this->password_set_on,true);
		$criteria->compare('email_verified',$this->email_verified);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('is_disabled',$this->is_disabled);

        return new CActiveDataProvider(get_class($this),array(
            'criteria'=>$criteria,
		    'sort'=>array(
				'attributes'=>array(
					'fullname'=>array(
						'asc'=>'lastname',
						'desc'=>'lastname DESC',
					),
					'*'
				),
		    ),
            'pagination' => array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
	}

	/**
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}

	/**
	 * @inheritdoc
	 */
	protected function beforeSave()
	{
            $field = $this->isNewRecord ? 'created_on' : 'updated_on';
            return parent::beforeSave() && $this->$field = date('Y-m-d H:i:s');
	}

	public static function hashPassword($password)
	{
            require(Yii::getPathOfAlias('usr.extensions').DIRECTORY_SEPARATOR.'password.php');
            return password_hash($password, PASSWORD_DEFAULT);
	}

	public function verifyPassword($password)
	{
            require(Yii::getPathOfAlias('usr.extensions').DIRECTORY_SEPARATOR.'password.php');
            return $this->password !== null && password_verify($password, $this->password);
	}
}
