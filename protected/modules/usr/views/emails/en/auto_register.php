<p>Мы рекомендуем вам подтвердить этот адрес перейдя по ссылке <?php echo CHtml::link($actionUrl, $actionUrl); ?> и в дальнейшем использовать для входа этот пароль <?php echo $password; ?> (его можно будет сменить в личном кабинете и мы рекомендуем сделать это при первом же посещении нашего сайта)</p>

<p>
	This message contains instructions to verify this email address. It was requested on the <?php echo CHtml::link(Yii::app()->name, $siteUrl); ?> website.
    If you did not perform this request, please ignore this email or contact our administrator.
</p>

<p>To verify this email address, open the following link:</p>
<p>
<?php echo CHtml::link($actionUrl, $actionUrl); ?>
</p>
<p>
If the link does not open correctly, try copying it and pasting in the browser's address bar.
</p>

