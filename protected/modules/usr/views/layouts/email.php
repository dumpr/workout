<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta property="og:title" content="<?= isset($title) ? $title : Yii::t('form','From site').' '.$_SERVER['HTTP_HOST'] ?>" />
        <title><?= isset($title) ? $title : Yii::t('form','From site').' '.$_SERVER['HTTP_HOST'] ?></title>
		<style type="text/css">
			#outlook a{padding:0}
			body{margin:0;padding:0;width:100% !important;-webkit-text-size-adjust:none;font-family:Georgia !important;color:#525252;font-size:14px}
			img{border:0;height:auto;line-height:100%;outline:none;text-decoration:none}
            hr{border:none;height:1px;background:#ddd}</style>
	</head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            	<tr>
                	<td align="center" valign="top">

                    	<table border="0" cellpadding="0" cellspacing="0" width="720">
                        	<tr>
                            	<td align="center" valign="top">

                                	<table border="0" cellpadding="0" cellspacing="0" width="720">
                                        <tr>
                                        	<td width="40px"></td>
                                        	<td width="90px" valign="middle" height="110px">
                                            	<img src="http://gbu/images/logo_m.png" />
                                        	</td>
                                            <td style="text-align:center">
                                            	<div style="color:#084148;font-size:18px"><?php Yii::app()->name ?></div>
	                                            <hr />
	                                            <?php /* ?><span style="color:#75969F;font-size:11px;margin:0 5px">Тел: <?= Yii::app()->controller->phoneFormat(Yii::app()->params['main']['phone_public']) ?></span>
	                                            <span style="color:#75969F;font-size:11px;margin:0 5px">Факс: <?= Yii::app()->controller->phoneFormat(Yii::app()->params['main']['fax_public']) ?></span>
	                                            <span style="color:#75969F;font-size:11px;margin:0 5px">Email: <?= Yii::app()->params['main']['email_public'] ?></span><?php */ ?>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">

<?php echo $content; ?>

                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                	<table border="0" cellpadding="10" cellspacing="0" width="720" id="templateFooter">
                                    	<tr>
                                        	<td valign="top">

                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td>
                                                        <hr />
                                                        <div style="text-align:center;color:#B7B7B7;margin:5px auto">Все права защищены &copy; <?= date('Y') ?></div>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
