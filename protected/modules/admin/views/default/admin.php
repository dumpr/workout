<form id="<?= $this->id ?>Form" action="/site/gridupdate" method="POST" onsubmit="return false;">
<?php

$this->widget('application.components.myGridView', array(
    'filter'=>$model,
    'dataProvider' => $model->search(),
    'template' => "{items}\n{pager}{pagesize}",
	'columns'=>array(
        array(
            'name'=>'day',
            'value'=> function($data,$row,$column){
                $d = '20'.$data->date;
                return Yii::app()->controller->rus_date('j F H:i',strtotime($d)); // 1411201400
            },
            'type'=>'raw',
            'filter'=> $model::days()//$model->appointmentTimes,
        ),
        /*array(
            'name'=>'day',
            'type'=>'raw',
            'value' => function($data,$row,$column){
                $cont = $column->grid->controller->id;
                $d = '20'.$data->date;
                return CHtml::link(
                    //$this->rus_date('j F Y',strtotime($data->created)).' г.',
                    Yii::app()->controller->rus_date('j F H:i',strtotime($d)),
                    "",
                    array(
                        "onClick"=>CHtml::ajax(
                            array(
                                "url"=>Yii::app()->createUrl("admin/".$cont."/update",array("id"=>$data->id)),
                                "type"=>"POST",
                                "data"=>false,
                                "beforeSend"=>"js:function() {noclone('".$column->grid->controller->id."',".$data->id.");}",
                                "success"=>"js:function(data){newtab4edit('".$cont."Tab',".$data->id.",'".$data->id."',data);}",
                            )
                        ),
                        "style"=>"cursor:pointer"
                    )
                ).CHtml::hiddenField("ids[]",$data->id,array("id"=>false));
            },
            'headerHtmlOptions'=>array('style'=>'width:160px'),
            'filter'=>$model::days(),
        ),*/
        /*array(
            'name'=>'time',
            'value'=> function($data,$row,$column){
                return Yii::app()->controller->rus_date('H:i',strtotime('20'.$data->date)); // 1411201400
            },
            'type'=>'raw',
            'filter'=> $model::times(),
        ),*/
        array(
            'name'=>'request',
            'header'=>'',
            'value'=> function($data,$row,$column){ //qw($data->request0,'e');
                return $data->request0['lastname'].' '.mb_substr($data->request0['firstname'], 0, 1).'. '.mb_substr($data->request0['patronymic'], 0, 1).'.<br />'.
                $data->request0['street'].', дом '.$data->request0['house'].', '.($data->request0['block'] ? 'корпус '.$data->request0['block'] : '').', кв. '.$data->request0['apartment'];
            },
            'type'=>'raw',
            'filter'=> '',
        ),
        'request0.text',
        array(
            'name' => 'request0.status',
            'type' => 'raw',
            'value' => function($data,$row,$column){
                return $column->filter[$data->request0->status];
            },
            'filter'=> Request::getStatuses(),
        ),
        /*array(
            'name'=>'',
            'header'=>CHtml::checkBox("selectAll",null,array("onChange"=>"selectAllCh(this)","id"=>false)),
            'value'=>function($data){
                return CHtml::label(CHtml::checkBox("selected[]",null,array("id"=>false,"value"=>$data->id)), false);
            },
            'type'=>'raw',
            'filter'=>'',
        ),
        array(
            'name'=>'created',
            'type'=>'raw',
            'value' => function($data,$row,$column){
                $cont = $column->grid->controller->id;
                return CHtml::link(
                    //$this->rus_date('j F Y',strtotime($data->created)).' г.',
                    Yii::app()->format->timeago($data->created),
                    "",
                    array(
                        "onClick"=>CHtml::ajax(
                            array(
                                "url"=>Yii::app()->createUrl("admin/".$cont."/update",array("id"=>$data->id)),
                                "type"=>"POST",
                                "data"=>false,
                                "beforeSend"=>"js:function() {noclone('".$column->grid->controller->id."',".$data->id.");}",
                                "success"=>"js:function(data){newtab4edit('".$cont."Tab',".$data->id.",'".$data->id."',data);}",
                            )
                        ),
                        "style"=>"cursor:pointer"
                    )
                ).CHtml::hiddenField("ids[]",$data->id,array("id"=>false));
            },
            'headerHtmlOptions'=>array('style'=>'width:160px'),
            'filter'=>'',
        ),
        array(
            'name'=>'target',
            'value'=>'$data->target',
            'type'=>'raw',
        ),*/
        /*array(
            'name'=>'name',
            'value'=>'$data->name',
            'type'=>'raw',
        ),
        array(
            'name'=>'user',
            'value'=> function($data,$row,$column){
                return $data->lastname.' '.mb_substr($data->firstname, 0, 1).'. '.mb_substr($data->patronymic, 0, 1).'.<br />'.
                $data->street.', дом '.$data->house.', '.($data->block ? 'корпус '.$data->block : '').', кв. '.$data->apartment;
            },
            'type'=>'raw',
            'filter'=> '',
        ),
        'text',
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => function($data,$row,$column){
                return $column->filter[$data->status];
            },
            'filter'=> $model->statuses,
        ),
        array(
            'name'=>'note',
            'value'=>'$data->note',
            'type'=>'raw',
        ),*/
    ),
));

?>

</form>