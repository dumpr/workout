<div class="form">

    <h3>Запрос №<span class="title"><?= $model->id ?></span></h3>
<?php
/*

          'id' => string '1' (length=1)
          'type' => string '0' (length=1)
          'user' => string '1' (length=1)
          'status' => string '0' (length=1)
          'firstname' => string 'Андрей' (length=12)
          'lastname' => string 'Яременко' (length=16)
          'patronymic' => string 'Иванович' (length=16)
          'street' => string 'Никулинская' (length=22)
          'house' => string 'fd' (length=2)
          'block' => string '' (length=0)
          'apartment' => string 'dr' (length=2)
          'phone' => string '+1234567892' (length=11)
          'email' => string '2bemate@gmail.com' (length=17)
          'text' => string 'xdrgdgdr gxdgxdr gxdrg xdr gd' (length=29)
          'note' => string '' (length=0)
          'created' => string '2014-11-14 22:44:17' (length=19)
          'modified' => string '2014-11-14 22:57:44' (length=19)
      private '_related' (CActiveRecord) =>
        array (size=1)
          'user0' =>
                array (size=14)
                  'id' => string '1' (length=1)
                  'username' => string 'admin' (length=5)
                  'firstname' => string 'Андрей' (length=12)
                  'lastname' => string 'Яременко' (length=16)
                  'patronymic' => string 'Иванович' (length=16)
                  'mobile' => string '+1234561023' (length=11)
                  'phone' => string '+1234567892' (length=11)
                  'email' => string '2bemate@gmail.com' (length=17)
                  'street' => string 'Никулинская' (length=22)
                  'house' => string '15' (length=2)
                  'block' => string '2' (length=1)
                  'apartment' => string '118' (length=3)
                  'is_admin' => string '1' (length=1)
                  'flat' => string '2' (length=1)
              private '_related' (CActiveRecord) =>
                array (size=1)
                  'address' =>
                        array (size=10)
                          'id' => string '2' (length=1)
                          'house' => string '2' (length=1)
                          'enter' => string '0' (length=1)
                          'floor' => string '0' (length=1)
                          'n' => string '118' (length=3)
                          'hvs' => string '-155.00' (length=7)
                          'gvs' => string '4200.00' (length=7)
                          'vdo' => string '120.00' (length=6)
                          'oto' => string '8750.00' (length=7)
                          'sod' => string '-60.00' (length=6)
                      'house0' =>
                            array (size=14)
                              'id' => string '2' (length=1)
                              'city' => string '0' (length=1)
                              'street' => string '11' (length=2)
                              'number' => string '15' (length=2)
                              'seria' => string 'serzf e' (length=7)
                              'floors' => string '16' (length=2)
                              'enters' => string '4' (length=1)
                              'sq_all' => string '0' (length=1)
                              'sq_live' => string '0' (length=1)
                              'elev_pass' => string '0' (length=1)
                              'elev_track' => string '0' (length=1)
                              'year' => string '1990' (length=4)
                              'status' => string '' (length=0)
                              'photo' => string '/data/ce/87/f6c138942b6922f314aba493fd07.jpg' (length=44)
                          private '_related' (CActiveRecord) =>
                            array (size=1)
                              'street_name' =>
                                    array (size=4)
                                      'id' => string '11' (length=2)
                                      'name' => string 'Никулинская улица' (length=33)
                                      'alias' => string 'nikulinskaya-ulicza' (length=19)
                                      'active' => string '1' (length=1)
*/
// qw($model->user0->address,'e');
$form = $this->beginWidget('CActiveForm', array(
	'id'=>$this->id.'Form_'.$model->id,
    //'enableAjaxValidation'=>true,
    'htmlOptions'=>array(
        'onsubmit'=>"return false;",
    ),
));

    echo $form->hiddenField($model,'id'); ?>

	<?php echo $form->errorSummary($model); ?>
<table class="table">
    <tr>
        <td><?php echo $form->labelEx($model,'created'); ?></td>
        <td>
            <?= $this->rus_date('j F Y H:i',strtotime($model->created)) ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td><?php echo $form->labelEx($model,'user'); ?></td>
        <td>
            <?= $model->lastname.' '.$model->firstname.'. '.$model->patronymic.'.<br />'.
                $model->street.', дом '.$model->house.', '.($model->block ? 'корпус '.$model->block : '').', кв. '.$model->apartment.'.<br />'.
                $model->phone ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>Текст заявки</td>
        <td>
            <?= $model->text ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>Комментарий<br /><i>будет виден только администратору</i></td>
        <td>
            <?= $form->textArea($model,'note',array('style'=>'height:160px')); ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>
            <?php echo $form->labelEx($model,'status'); ?>
        </td>
        <td>
            <?php echo $form->dropDownList($model,'status',$model->statuses,array('style'=>'width:auto')); ?>
        </td>
        <td></td>
    </tr>
</table>

<div class="gridButtons" >
    <?php
    echo CHtml::submitButton(
        'Сохранить',
        array('name'=>'save', 'class'=>'btn btn-primary btn-sm okbutton right-btn', 'onClick'=>'saveTab("'.$this->id.'","'.$model->id.'")'));
    ?>
</div>

<?php $this->endWidget(); ?>

</div>