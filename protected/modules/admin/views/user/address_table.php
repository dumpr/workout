            <?php
            $adr_string = "";
            $cert = "";
            $area = "";
            $scan = "";
            $confirm ="";
            $verify = "";
                if($adr->flat){
                    $adr_string = $adr->flat0->house0->street_name->name.', дом. '.$adr->flat0->house0->number.(empty($adr->flat0->house0->block) ? '' : ', '.$adr->flat0->house0->block).', кв.'.$adr->flat0->n;
                    $cert = $adr->certificate;
                    $area = $adr->area;
                    $path = explode('.',$adr->scan);
                    $ext = array_pop($path);
                    $path = implode('',$path)."_original.".$ext;
                    if($adr->scan && file_exists($_SERVER['DOCUMENT_ROOT'].$path))
                        $scan = "<a target='_blank' href='".$path."'>скан</a> (откроется в новом окне)";
                    $verify = "<a href='#' class='unchain_address'>Удалить этот адрес из списка адресов пользователя</a>";
                } elseif(!empty($adr->street) & !empty($adr->house) & !empty($adr->apartment) & !empty($adr->certificate) & !empty($adr->area) & !empty($adr->scan)){
                    $adr_string = "<b style='color:red'>".$adr->street.", дом ".$adr->house.(empty($adr->block) ? '' : ', '.$adr->block).", кв. ".$adr->apartment."</b>";
                    $cert = $adr->certificate;
                    $area = $adr->area;
                    $path = explode('.',$adr->scan);
                    $ext = array_pop($path);
                    $path = implode('',$path)."_original.".$ext;
                    if($adr->scan && file_exists($_SERVER['DOCUMENT_ROOT'].$path))
                        $scan = "<a target='_blank' href='".$path."'>скан</a> (откроется в новом окне)";
                    $confirm = "<input type='text' name='note' class='short_input' placeholder='Указать ошибку в заполнении' value='".$adr->note."' /><input class='btn btn-primary btn-sm okbutton addressNote' type='submit' value='Отправить''>";
                    $verify = "<a href='#' class='confirm_address'>Присвоить пользователю адрес</a><br />";
                } else {
                    $adr_string = $adr->street.", дом ".$adr->house.(empty($adr->block) ? '' : ', '.$adr->block).", кв. ".$adr->apartment;
                } ?>
                <tr>
                    <td>№ свидетельства</td>
                    <td><?= $cert ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Площадь</td>
                    <td><?= $area ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Скан</td>
                    <td><?= $scan ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Адрес<br />----</td>
                    <td colspan="2">
                        <div class="address_string"><?= $adr_string ?></div>
                        <div class="confirmForm" data-address-id="<?= $adr->id ?>">
                            <?= $confirm ?>
                        </div>
                        <?= $verify ?>
                        <div class="confirm_info"></div>
                        <br />
                    </td>
                </tr>