<form id="<?= $this->id ?>Form" action="/site/gridupdate" method="POST" onsubmit="return false;">
<?php

$this->widget('myGridView', array(
    'filter'=>$model,
    'dataProvider' => $model->search(),
    'template' => "{items}\n{deleteButton}\n{pager}{pagesize}",
	'columns'=>array(
        array(
            'name'=>'',
            'header'=>CHtml::checkBox("selectAll",null,array("onChange"=>"selectAllCh(this)","id"=>"checkall")).CHtml::label('',"checkall"),
            'headerHtmlOptions'=>array('style'=>'width:30px',"class"=>"checkbox"),
            'value'=>function($data){
                return CHtml::checkBox("selected[]",null,array("id"=>"selected".$data->id,"value"=>$data->id)).CHtml::label('',"selected".$data->id).CHtml::hiddenField("ids[]",$data->id,array("id"=>false));
            },
            'htmlOptions'=>array("class"=>"checkbox"),
            'type'=>'raw',
            'filter'=>'',
        ),
        array(
            'name'=>'username',
            'type'=>'raw',
            'value' => function($data,$row,$column){
                $cont = $column->grid->controller->id;
                return CHtml::link(
                    $data->username,
                    "",
                    array(
                        "onClick"=>CHtml::ajax(
                            array(
                                "url"=>Yii::app()->createUrl("admin/".$cont."/update",array("id"=>$data->id)),
                                "type"=>"POST",
                                "data"=>false,
                                "beforeSend"=>"js:function() {noclone('".$column->grid->controller->id."',".$data->id.");}",
                                "success"=>"js:function(data){newtab4edit('".$cont."Tab',".$data->id.",'".$data->username."',data);}",
                            )
                        )
                    )
                ).CHtml::hiddenField("ids[]",$data->id,array("id"=>false));
            },
        ),
        array(
            'name'=>'fullname',
            'value'=>'$data->fullname()',
        ),
        array(
            'name'=>'email',
            'type'=>'raw',
            'value' => '$data->email',
        ),
        array(
            'name'=>'phone',
            'value'=>'$data->phone',
        ),
        array(
            'name'=>'last_visit_on',
            'type' => empty($data->last_visit_on) ? 'raw' : 'timeago',
            'value'=>function($data){
                return $data->last_visit_on{0}==0 ? '-' : $data->last_visit_on;
            },
        ),
        array(
            'name'=>'is_disabled',
            'value'=>function($data){
                return $data->is_disabled ? 'бан' : '';
            },
            'type'=>'raw',
            'filter'=>array('0'=>'ОК','1'=>'Забанен'),
            'headerHtmlOptions'=>array('style'=>'width:100px'),
        ),
    ),
));

?>
</form>