<div class="form">

    <h3>Пользователь <span class="title"><?= substr($model->email,0,strpos($model->email,'@')) ?></span></h3>
<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>$this->id.'Form_'.$model->id,
    //'enableAjaxValidation'=>true,
    'htmlOptions'=>array(
        'onsubmit'=>"return false;",
    ),
));

    echo $form->hiddenField($model,'id'); ?>

	<?php echo $form->errorSummary($model); ?>
<table class="table">
    <tr>
        <td style="width:350px">ID</td>
        <td>
            <?= $model->id ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>Последнее посещение личного кабинета</td>
        <td>
            <?= $model->last_visit_on ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>Полное имя</td>
        <td>
            <?= $model->fullname() ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>Контактная информация</td>
        <td>
            <?= $model->email ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>Адрес</td>
        <td colspan="2"></td>
    </tr>
</table>

<div class="gridButtons" >
    <?php
    echo CHtml::submitButton('Закрыть',array('name'=>'close', 'class'=>'btn btn-default', 'onClick'=>'closeTab(this);'));

    echo CHtml::submitButton(
        'Сохранить',
        array('name'=>'save', 'class'=>'btn btn-primary btn-sm okbutton right-btn', 'onClick'=>'saveTab("'.$this->id.'","'.$model->id.'")'));
    ?>
</div>

<?php $this->endWidget(); ?>

</div>