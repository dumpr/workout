<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <base href='<?= Yii::app()->getBaseUrl(true); ?>' />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic%7CPT+Sans+Narrow:400,700%7CPT+Sans+Caption:400,700&amp;subset=latin,cyrillic">
    <title><?= CHtml::encode($this->pageTitle) ?></title>
</head>

<body>

    <div class="row top_menu">
        <div class="container">
            <a class="logout" href="logout">Выйти</a>
            <ul class="inner_menu">
                <li><a href="admin" title="Панель управления. Главная"><i class="fa fa-home"></i></a></li>
                <li><a href="admin/param" title="Основные настройки"><i class="fa fa-gear"></i></a></li>
                <li><a href="rbac" title="Управление доступами"><i class="fa fa-user-secret"></i></a></li>
                <li><a href="admin/page" title="Страницы"><i class="fa fa-file"></i><span>Страницы</span></a></li>
                <li><a href="admin/comment" title="Комментарии"><i class="fa fa-comment"></i><span>Комментарии</span></a></li>
                <li><a href="admin/user" title="Зарегистрированные пользователи"><i class="fa fa-user"></i><span>Пользователи</span></a></li>
            </ul>
        </div>
        <div class="container adm_title">
            <h1><?= CHtml::encode($this->pageTitle) ?></h1>
            <a class="logout" href="http://<?= $_SERVER['HTTP_HOST'] ?>">Перейти на сайт</a>
        </div>
    </div>

    <div class="container" id="adm">
        <div class="row">
            <?php echo $content; ?>
        </div>
    </div>

	<div class="clear"></div>

	<div id="footer">
		<?= Yii::t('site','Copyright &copy; {n} by {sitename}.',array('{n}'=>date('Y'),'{sitename}'=>'<a href="http://dump.com.ua/adminPanel">DUMP admin panel</a>')) ?><br/>
		GNU General Public License<br/>
		<?= Yii::powered(); ?><br/>
		<span id="time_mem"><?= dbginfo() ?></span>
	</div>

    <div id="modal">dbxxdrfgbxdr</div>
    <div id="overlay" onclick="javascript:modalClose()"></div>

    <?php
        $aix = implode('","',Yii::app()->filestore->allowedImgExts);
        $adx = implode('","',Yii::app()->filestore->allowedDocExts);
        $js = <<<EOT
    var allowedImgExts = ["{$aix}"];
    var allowedDocExts = ["{$adx}"];
    var allowedExts = allowedImgExts.concat(allowedDocExts);
EOT;
        Yii::app()->clientScript->registerScript("allowedExts", $js);
        $bp = Yii::app()->assetManager->getPublishedUrl(Yii::getPathOfAlias('application.modules.admin.assets'));
    ?>
<?php /* ?><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script><?php */ ?>
    <script src="dev/jquery.min.js"></script>
    <script type="text/javascript">var packageBasePath = "<?= $bp ?>";</script>
    <?php

    $adminStaticPackage = array(
        'basePath'    => 'application.modules.admin.assets',
        'css'         => array('css/bootstrap.min.css','font-awesome-4.3.0/css/font-awesome.min.css','markitup/skins/markitup/style.css','markitup/sets/html/style.css','css/awesome-bootstrap-checkbox.css'),
        'js'          => array('js/bootstrap.min.js','js/filedrop-min.js','js/jquery.dragsort-0.5.2.min.js','js/admin_libs.js','markitup/jquery.markitup.js','markitup/sets/html/set.js'),
        //'depends'     => array('jquery'),
    );
    Yii::app()->clientScript->addPackage('adminStaticPackage',$adminStaticPackage)->registerPackage('adminStaticPackage');

/*    Yii::app()->assetManager->publish('dev/admin.css', false, -1, YII_DEBUG);
    Yii::app()->assetManager->publish('dev/admin.js', false, -1, YII_DEBUG);*/
    Yii::app()->clientScript->registerCssFile('dev/admin.css'); // ,'js/admin.css'
    Yii::app()->clientScript->registerScriptFile('dev/admin.js'); // ,'js/admin.js'

    Yii::app()->clientScript->registerScript('adm_init',"$('.inner_menu a[href=\"admin/{$this->id}\"]').addClass('current');");

    ?>
</body>
</html>
