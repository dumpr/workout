<form id="<?= $this->id ?>Form" action="/site/gridupdate" method="POST" onsubmit="return false;">
<?php
// http://www.yiiframework.com/forum/index.php/topic/20941-filter-date-range-on-cgridview-toolbar/
// this is the date picker
$dateisOn = $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'Comment[date_first]',
            'language' => 'ru',
            'value' => $model->date_first,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => 'true',
                'changeYear' => 'true',
                'constrainInput' => 'false',
            ),
            'htmlOptions' => array(
                'style' => 'height:30px;width:90px;',
                'title' => 'Начиная с ..',
            ),), true) . $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'Comment[date_last]',
            'language' => 'ru',
            'value' => $model->date_last,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => 'true',
                'changeYear' => 'true',
                'constrainInput' => 'false',
            ),
            'htmlOptions' => array(
                'style' => 'height:30px;width:90px',
                'title' => 'по .. включительно',
            ),), true);

$this->widget('myGridView', array(
    'filter'=>$model,
    'dataProvider' => $model->search(),
    'afterAjaxUpdate' => "function() {
        jQuery('#Comment_date_first').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['ru'], {'dateFormat':'yy-mm-dd','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));
        jQuery('#Comment_date_last').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['ru'], {'dateFormat':'yy-mm-dd','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));
    }",
    'template' => "{commentButtons}\n{items}\n{pager}{pagesize}",
	'columns'=>array(
            array(
                'name'=>'',
                'header'=>CHtml::checkBox("selectAll",null,array("onChange"=>"selectAllCh(this)","id"=>"checkall")).CHtml::label('',"checkall"),
                'headerHtmlOptions'=>array('style'=>'width:30px',"class"=>"checkbox"),
                'value'=>function($data){
                    return CHtml::checkBox("selected[]",null,array("id"=>"selected".$data->id,"value"=>$data->id)).CHtml::label('',"selected".$data->id).CHtml::hiddenField("ids[]",$data->id,array("id"=>false));
                },
                'htmlOptions'=>array("class"=>"checkbox"),
                'type'=>'raw',
                'filter'=>'',
            ),
            array(
                'name'=>'m',
                'value'=>'$data->objName()',
                'filter'=>'',
            ),
            /*array(
                'name'=>'m',
                'value'=>function($data){
                    return $data->objName();
                },
                //'filter'=>'',
            ),*/
            array(
                'name'=>'user_id',
                'value'=>function($data){
                    return UsrModule::user($data->user_id)['fullname'];
                    // return CHtml::link($data->user->fullname(),'profile/'.$data->user->username);
                },
                'type'=>'raw',
                'filter'=>'',
            ),
            array(
                'name'=>'created',
                'value' => function($data,$row,$column){
                    $cont = $column->grid->controller->id;
                    return CHtml::link(
                        Helper::rusDate("l, j F H:i",$data->created),
                        "",
                        array(
                            "onClick"=>CHtml::ajax(
                                array(
                                    "url"=>Yii::app()->createUrl("admin/".$cont."/update",array("id"=>$data->id)),
                                    "type"=>"POST",
                                    "data"=>false,
                                    "beforeSend"=>"js:function() {noclone('".$column->grid->controller->id."',".$data->id.");}",
                                    "success"=>"js:function(data){newtab4edit('".$cont."Tab',".$data->id.",'".$data->id."',data);}",
                                )
                            )
                        )
                    );
                },
                'filter'=>$dateisOn,
                //'type' => 'timeago',
                'type'=>'raw',
            ),
            array(
                'name'=>'text',
                'value'=>'nl2br(CHtml::encode($data->text))',
                'type'=>'raw',
            ),
            array(
                'name'=>'active',
                'value'=>function($data){
                    return $data->statuses[$data->active];
                },
                'type'=>'raw',
                'filter'=>$model->statuses,
                'headerHtmlOptions'=>array('style'=>'width:100px'),
            ),
/*      array(
            'name'=>'',
            'header'=>CHtml::checkBox("selectAll",null,array("onChange"=>"selectAllCh(this)","id"=>false)),
            'value'=>function($data){
                return CHtml::label(CHtml::checkBox("selected[]",null,array("id"=>false,"value"=>$data->id)), false);
            },
            'type'=>'raw',
            'filter'=>'',
        ),
        array(
            'name'=>'username',
            'type'=>'raw',
            'value' => function($data,$row,$column){
                $cont = $column->grid->controller->id;
                return CHtml::link(
                    $data->username,
                    "",
                    array(
                        "onClick"=>CHtml::ajax(
                            array(
                                "url"=>Yii::app()->createUrl("admin/".$cont."/update",array("id"=>$data->id)),
                                "type"=>"POST",
                                "data"=>false,
                                "beforeSend"=>"js:function() {noclone('".$column->grid->controller->id."',".$data->id.");}",
                                "success"=>"js:function(data){newtab4edit('".$cont."Tab',".$data->id.",'".$data->username."',data);}",
                            )
                        )
                    )
                ).CHtml::hiddenField("ids[]",$data->id,array("id"=>false));
            },
        ),
        array(
            'name'=>'fullname',
            'value'=>'$data->fullname()',
        ),
        array(
            'name'=>'email',
            'type'=>'raw',
            'value' => '$data->email',
        ),
        array(
            'name'=>'phone',
            'value'=>'$data->phone',
        ),
        array(
            'name'=>'last_visit_on',
            'type' => 'timeago',
        ),*/
    ),
));

?>
</form>