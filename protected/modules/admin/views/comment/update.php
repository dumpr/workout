<div class="form">

<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>$this->id.'Form_'.$model->id,
    //'enableAjaxValidation'=>true,
    'htmlOptions'=>array(
        'onsubmit'=>"return false;",
    ),
));

    echo $form->hiddenField($model,'id'); ?>

    <?php echo $form->errorSummary($model); ?>

<div class="gridButtons" >
    <?php
    echo CHtml::submitButton('Закрыть',array('name'=>'close', 'class'=>'btn btn-info', 'onClick'=>'closeTab(this);'));

    echo CHtml::submitButton(
        'Сохранить',
        array('name'=>'save', 'class'=>'btn btn-primary btn-sm okbutton right-btn', 'onClick'=>'saveTab("'.$this->id.'","'.$model->id.'")'));
    ?>
</div>

<table class="table">
    <tr>
        <td style="width:350px">ID</td>
        <td>
            <?= $model->id ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>Последнее посещение личного кабинета</td>
        <td>
            <?= $model->user->last_visit_on ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>Полное имя</td>
        <td>
            <?= $model->user->fullname() ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>Контактная информация</td>
        <td>
            <?= $model->user->email ?><br />
            <?= $model->user->phone ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>Текст</td>
        <td>
            <?= $form->textArea($model,'text',array('style'=>'height:90px')) ?></td>
        <td></td>
    </tr>
    <tr>
        <td>Статус</td>
        <td class="radio">
            <?= $form->radioButtonList($model,'active',$model->statuses); ?>
        </td>
        <td></td>
    </tr>
</table>


<?php $this->endWidget(); // $form->dropDownList($model,'active',$model->statuses);  ?>

</div>