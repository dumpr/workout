<p>Будьте осторожны при редактировании идентификаторов настроек. Возможно они необходимы для работы движка.</p>
<form method="POST" action="params/index?admin" id="params_form">
<table id="params_panel">
<?php
        foreach($params as $p){
            $this->renderPartial('param_admin_field',array('p'=>$p));
        }
?>
    <tr сlass="clear_param">
        <td style="width: 240px">
            <input type="text" value="" name="param[new_param][name]" placeholder="идентификатор" class="identifier" />
            <input type="text" value="" name="param[new_param][label]" placeholder="Название" /><br>
            <textarea name="param[new_param][description]" placeholder="Описание"></textarea>
        </td>
        <td>
        <?php /* ?>
        <select name="field_type" onchange="add_params(this.value)">
            <option value="">Сделайте выбор</option>
<?php foreach($types as $t){ ?>
            <option value="<?= $t ?>"><?= $t ?></option>
<?php } ?>
        </select>
        <?php */ ?>
        <input type="hidden" value="" name="param[new_param][type]" class="hidden_identifier" />
<div class="hidden_fields">
    <input type="text" value="" name="param[new_param][value]" placeholder="Значение" class="param_value hidden input" />
    <?php
/*
    <textarea name="param[new_param][value]" placeholder="Текст" class="hidden textarea editor"></textarea>
    <textarea name="param[new_param][value]" placeholder="Каждое значение на новой строке" class="hidden select checkbox radio array"></textarea>
    <input type="file" value="" name="param[new_param][value]" class="param_value hidden file" />
    <input type="text" value="" name="param[new_param][value]" placeholder="yyyy-mm-dd hh:mm" class="param_value hidden datetime" />
 */
    ?>
</div>
        </td>
    </tr>
    <tr>
        <td><a href="params/index">Ввод данных</a></td>
        <td><input name="save_params" type="submit" value="Сохранить изменения" /></td>
    </tr>
</table>
</form>