
<form method="POST" action="" id="params_form" onsubmit="return false;">
<table class="table">
    <tr>
        <td></td>
        <td style="width: 500px"></td>
        <td></td>
    </tr>
<?php foreach($params as $p){
    if($p['name'] == 'watermark')
        $this->renderPartial('watermark',array('p'=>$p,),false,true);
    else { ?>

    <tr>
        <td><?= $p['name'] ?><br /></td>
        <td><?= empty($p['label']) ? $p['name'] : '<b>'.$p['label'].'</b>' ?><br />
        <small><?= $p['description'] ?></small></td>
        <td>
        <?php if($p['type'] == 'area'){ ?>
            <textarea name="<?= $p['name'] ?>" style="height: 150px; width: 100%"><?= $p['value'] ?></textarea>
        <?php } elseif($p['type'] == 'input'){ ?>
            <input type="text" value="<?= $p['value'] ?>" name="<?= $p['name'] ?>" placeholder="Значение" class="param_value" />
        <?php } ?>
        </td>
    </tr>
<?php } } ?>
</table>

    <input name="save_params" type="submit" onclick="javascript:paramSubmit(this);" class="btn btn-primary pull-right" value="Сохранить изменения" />
</form>