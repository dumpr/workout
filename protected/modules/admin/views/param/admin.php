<form id="<?= $this->id ?>Form" action="/site/gridupdate" method="POST" onsubmit="return false;">

<?php $this->widget('myGridView', array(
    'filter'=>$model,
    'dataProvider' => $model->search(),
    'template' => "{items}\n{paramsave}\n{pager}{pagesize}",
	'columns'=>array(
        array(
            'name'=>'par',
            'value'=>'CHtml::encode($data->par)',
            'type'=>'raw',
            'filter'=>Yii::app()->controller->pars, // array('select'=>'path','distinct'=>true,)
        ),
        array(
            'name'=>'name',
            'value'=>'CHtml::encode($data->name)',
            'type'=>'raw',
        ),
        array(
            'name'=>'label',
            'value'=>function($data){
                return '<b>'.CHtml::encode($data->label).'</b><br /><small>'.CHtml::encode($data->description).'</small>';
            },
            'type'=>'raw',
        ),
        array(
            'name'=>'value',
            'headerHtmlOptions'=>array('style'=>'min-width:500px'),
            'value'=>function($data){
                $value = '';
                if($data->type == 'input')
                    $value = '<input type="text" value="'.$data->value.'" name="'.$data->name.'" placeholder="'.$data->label.'" />';
                elseif($data->type == 'area')
                    $value = '<textarea name="'.$data->name.'" style="height: 150px; width: 100%" placeholder="'.$data->label.'">'.$data->value.'</textarea>';
                elseif($data->type == 'watermark') $value = Yii::app()->controller->renderPartial('watermark',array('p'=>$data,),true,true);
                return $value;
            },
            'type'=>'raw',
        ),
    ),
));

?>

</form>