
            <div class="watermark_preview"><?= empty($p['value']) ? '' : '<img src='.$p['value'].' />' ?></div>
        <?php
	$this->widget('ext.EAjaxUpload.EAjaxUpload',
                array(
                    'id'=>'watermarkParams',
                    'config'=>array(
                        'action'=>'admin/params/uploader',
                        'template'=>'
<div class="qq-uploader">
    <div class="qq-upload-drop-area"><span>Перетяните сюда файл для загрузки</span></div>
    <div class="qq-upload-button small-window">Загрузить картинку</div>
    <ul class="qq-upload-list"></ul>
</div>',
                        'debug'=>false,
                        'allowedExtensions'=>array('png'),
                        'sizeLimit'=>50*1024,
                        // 'minSizeLimit'=>1024,
                        'onSubmit'=>"js:function(){ $('.qq-upload-list').text(''); }",
                        'onComplete'=>"js:function(id, fileName, responseJSON){
                            $('input[name=watermark_field]').attr('value',responseJSON.filename);
                            if(responseJSON.error == undefined)
                                $('.watermark_preview').html('<img src=\"'+responseJSON.filename+'\" />');
                        }"),));
?>

            <input type="hidden" name="watermark_field" value="<?= $p['value'] ?>" />
