<?php
/* @var $this StaticPagesController */
/* @var $model StaticPages */

$this->breadcrumbs=array(
	'Static Pages'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List StaticPages', 'url'=>array('index')),
	array('label'=>'Create StaticPages', 'url'=>array('create')),
	array('label'=>'Update StaticPages', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete StaticPages', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage StaticPages', 'url'=>array('admin')),
);
?>

<h1>View StaticPages #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'page_alies',
		'page_index',
		'page_short_title',
		'title',
		'description',
		'keywords',
		'ico',
		'page_title',
		'anons',
		'content',
		'page_ps',
		'page_note',
		'page_user',
		'created',
		'modified',
		'active',
		'page_menu',
		'order',
		'page_comm',
		'parent',
		'tags',
		'cat_id',
		'path',
		'assets',
		'show_child',
		'sitemap',
	),
)); ?>
