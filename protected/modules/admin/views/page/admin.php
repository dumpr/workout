<form id="<?= $this->id ?>Form" action="/site/gridupdate" method="POST" onsubmit="return false;">

<?php

        $prnt = 0;
        if(isset($_GET['parent'])){
            $prnt = (int)$_GET['parent'];
            $sql = 'SELECT parent FROM '.Page::model()->tableSchema->rawName.' WHERE id='.$prnt;
            $upLevel = Yii :: app()->db->createCommand($sql)->queryScalar();
        }
        if(isset($_GET['parent']) && $_GET['parent'])
            $upLevel = '<span class="countChildren" onClick="gridShowChildren(\'page\','.$upLevel.')">&uarr;</span>';
        else $upLevel = '';

$this->widget('myGridView', array(
    'filter'=>$model,
    'dataProvider' => $model->search(),
    'template'=> "{buttons}\n{items}\n{pager}{pagesize}",
	'columns'=>array(
        array(
            'name'=>'',
            'header'=>CHtml::checkBox("selectAll",null,array("onChange"=>"selectAllCh(this)","id"=>"checkall")).CHtml::label('',"checkall"),
            'headerHtmlOptions'=>array('style'=>'width:30px',"class"=>"checkbox"),
            'value'=>function($data){
                return CHtml::checkBox("selected[]",null,array("id"=>"selected".$data->id,"value"=>$data->id)).CHtml::label('',"selected".$data->id).CHtml::hiddenField("ids[]",$data->id,array("id"=>false));
            },
            'htmlOptions'=>array("class"=>"checkbox"),
            'type'=>'raw',
            'filter'=>'',
        ),
        array(
            'name' => 'name',
            'type' => 'raw',
            'value' => function($data,$row,$column){
                $cont = $column->grid->controller->id;
                return CHtml::link(
                    CHtml::encode($data->name),
                    "",
                    array(
                        "onClick"=>CHtml::ajax(
                            array(
                                "url"=>Yii::app()->createUrl("admin/".$cont."/update",array("id"=>$data->id)),
                                "type"=>"POST",
                                "data"=>false,
                                "beforeSend"=>"js:function() {noclone('".$cont."',".$data->id.");}",
                                "success"=>"js:function(data){newtab4edit('".$cont."Tab',".$data->id.",'".$data->name."',data);}",
                            )
                        )
                    )
                ).' <span class="item_id">'.$data->id.'</span>';
            },
        ),
        array(
            'name'=>'',
            'header'=>$upLevel,
            'value'=> function($data){
                if($data->countChildren)
                    return '<span class="countChildren" onClick="gridShowChildren('.$data->id.')">'.$data->countChildren.'</span>';
            },
            'type'=>'raw',
            'filter'=>'',
        ),
        /*array(
            'name'=>'path',
            'value'=>'CHtml::encode($data->path)',
            'type'=>'raw',
            'filter'=>Yii::app()->controller->paths,
        ),
        array(
            'name'=>'alias',
            'value'=>'CHtml::encode($data->alias)',
            'type'=>'raw',
        ),*/
        // 2 - BLOG
        array(
            'name' => 'order',
            'type' => 'raw',
            'value' => 'CHtml::numberField("orders[]",$data->order,array("id"=>false, "class"=>"arrows-input"))',
            'filter'=>'',
            'headerHtmlOptions'=>array('style'=>'width:100px'),
        ),
        array(
            'name'=>'menu',
            'htmlOptions'=>array("class"=>"checkbox"),
            'value'=>function($data){
                return CHtml::checkBox("menu[]",$data->menu,array("id"=>"menu".$data->id, "value"=>$data->id, "class"=>"active-item-input")).CHtml::label('',"menu".$data->id);
            },
            'type'=>'raw',
            'filter'=>array(0=>"Нет",1=>"Да"),
            'headerHtmlOptions'=>array('style'=>'width:100px'),
        ),
        array(
            'name'=>'active',
            'htmlOptions'=>array("class"=>"checkbox"),
            'value'=>function($data){
                return CHtml::checkBox("active[]",$data->active,array("id"=>"active".$data->id, "value"=>$data->id, "class"=>"active-item-input")).CHtml::label('',"active".$data->id);
            },
            'type'=>'raw',
            'filter'=>array(0=>"Нет",1=>"Да"),
            'headerHtmlOptions'=>array('style'=>'width:100px'),
        ),
    ),
));

?>
</form>
<span class="time_mem"><?= dbginfo() ?></span>