<div data-file-id="<?= $data['id'] ?>">
    <i class="fa fa-times"></i>
    <div class="ext <?= $data['ext'] ?>">
        <?php if($data['is_image']){ ?>
        <img width="160" src="<?= $data['src'] ?>" alt="<?= CHtml::encode($data['description']) ?>" />
        <?php } else { ?>
        <a href="<?= $data['src'] ?>" title="<?= CHtml::encode($data['description']) ?>"><?= CHtml::encode($data['name']) ?></a>
        <?php } ?>
    </div>
    <div class="attached_file_info">
        <label>
            <b><?= $data['is_image'] ? 'Альтернативный' : 'Сопроводительный' ?> текст</b>
        <?php foreach(Yii::app()->params['translatedLanguages'] as $l=>$lang){
            $field = $l === Yii::app()->params['defaultLanguage'] ? 'description' : 'description_' . $l; ?>
            <span class="flag <?= $l ?>" title="<?= $lang ?>"></span>
            <input class="ml" type="text" name="Attache[<?= $data['id'] ?>][<?= $field ?>]" value="<?= isset($data[$field]) ? CHtml::encode($data[$field]) : '' ?>" />
        </label>
        <?php } ?>
        <label>
            <b>Имя файла</b>
            <input type="text" name="Attache[<?= $data['id'] ?>][name]" value="<?= CHtml::encode($data['name']) ?>" />
        </label>
        <?php /* ?><select style="width:auto" name="Attache[<?= $data['src'] ?>][year]">
                <?php
                $sel = empty($data['year']) ? date('Y') : $data['year'];
                foreach(WorkPlan::model()->years as $year){?>
                <option value="<?= $year ?>"<?= $sel==$year ? ' selected="selected"' : '' ?>><?= $year ?></option>
                <?php } ?>
        </select><?php */ ?>
    </div>
</div>