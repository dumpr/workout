<table class="table">
    <tr>
        <td><?= $form->labelEx($model,'name'); ?></td>
        <td colspan="2"><?= $form->textField($model,'name'.$suffix,array('maxlength'=>255)); ?></td>
    </tr>
    <tr>
        <td><?= $form->labelEx($model,'anons'); ?></td>
        <td colspan="2"><?= $form->textArea($model,'anons'.$suffix,array('style'=>'height:90px')); ?></td>
    </tr>
    <tr>
        <td style="min-height:420px">
            <?= $form->labelEx($model,'content'); ?>
        </td>
        <td colspan="2">
            <?= $form->textArea($model,'content'.$suffix,array('id'=>false,'class'=>'page_content')); ?>
        </td>
    </tr>
    <tr>
        <td>
            <?= $form->labelEx($model,'title'); ?>
        </td>
        <td colspan="2">
            <?= $form->textField($model,'title'.$suffix,array('size'=>60,'maxlength'=>255)); ?>
        </td>
    </tr>
    <tr>
        <td>
            <?= $form->labelEx($model,'keywords'); ?>
        </td>
        <td colspan="2">
            <?= $form->textField($model,'keywords'.$suffix,array('size'=>60,'maxlength'=>255)); ?>
        </td>
    </tr>
    <tr>
        <td>
            <?= $form->labelEx($model,'description'); ?>
        </td>
        <td colspan="2">
            <?= $form->textField($model,'description'.$suffix,array('size'=>60,'maxlength'=>511)); ?>
        </td>
    </tr>
</table>