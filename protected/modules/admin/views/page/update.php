<div class="form">

<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>$this->id.'Form_'.$model->id,
//    'enableAjaxValidation'=>true,
    'htmlOptions'=>array(
        'onsubmit'=>"return false;",
    ),
));
    echo $form->hiddenField($model,'id'); ?>

	<?= $form->errorSummary($model); ?>


<div class="gridButtons" >
    <?php
    echo CHtml::submitButton(
        $model->isNewRecord ? 'Создать' : 'Сохранить',array(
                    'name'=>$model->isNewRecord ? 'create' : 'save',
                    'class'=>'btn btn-primary btn-sm okbutton right-btn',
                    'onClick'=>'saveTab("'.$this->id.'","'.$model->id.'")'));

    echo CHtml::submitButton('Закрыть',array(
                    'name'=>'close',
                    'class'=>'btn btn-info',
                    'onClick'=>'closeTab(this);')); // javascript:closeTabTitle(this);
    if(empty($model->isNewRecord)){
        echo CHtml::submitButton('Копировать',array(
                        'name'=>'copy',
                        'class'=>'btn btn-info',
                        'onClick'=>'copyTab("'.$this->id.'","'.$model->id.'");'));
        echo CHtml::submitButton('Удалить', array(
                        "name"=>"dodelete",
                        "class"=>"btn btn-danger",
                        "onClick"=>"singledelete(this);"));
    } ?>
    <span class="item_id"><?= $model->isNewRecord ? 'новая' : 'id: '.$model->id ?></span>
</div>

<table class="table">
    <tr>
        <td style="width:250px"><?= $form->labelEx($model,'parent'); ?></td>
        <td><?= $form->dropDownList($model,'parent',$model->catList); ?></td>
        <?php if($model->isNewRecord){ ?>
        <td rowspan="3" class="create_first">Вы сможете прикрепить документы и изображения после того как создадите страницу.</td>
        <?php } else { ?>
        <td rowspan="3" id="pageIco_<?= $model->id ?>" style="width:250px">
            <?=  CHtml::hiddenField('pageIco',empty($model->ico) ? '' : $model->ico['id']) ?>
            <div class="ico_holder">
            <?php if(!empty($model->ico)){ ?>
                <i class="fa fa-times"></i>
                <img src="<?= Yii::app()->image->createUrl('pageIco',$model->ico['id']) ?>" />
            <?php } ?>
            </div>
        </td>
        <?php } ?>
    </tr>
    <tr>
        <td>
            <?= $form->labelEx($model,'alias'); ?>
        </td>
        <td class="aliastd">
            <span><?= $model->path ?>/</span>
            <?= $form->textField($model,'alias',array('size'=>60,'maxlength'=>127)); ?>
        </td>
    </tr>
    <tr>
        <td>
            <?= $form->labelEx($model,'created'); ?>
        </td>
        <td>
        <?php if($model->isNewRecord){ ?>
            <div class="form-group">
            <div class='input-group date' id='datetimepicker_<?= $model->id ?>'>
                <?= $form->textField($model,'created',array('class'=>'datetime_field')); ?>
                <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span> </span>
            </div>
            </div>
            <script type="text/javascript">
            $(function () {
                $('#datetimepicker_<?= $model->id ?>').datetimepicker({
                    locale: 'ru',
                    format: 'YYYY-MM-DD HH:mm:ss',
                    defaultDate: '<?= $model->created ?>'
                });
            });
        </script>
        <?php } else { ?>
                <p><?= '<small>страница создана '.Helper::rusDate('d F Y H:i',$model->created) ?><?= $model->modified{0} == '0' ? '' : ', последний раз редактировалась '.Helper::rusDate('d F Y H:i',$model->modified).'</small>' ?></p>
        <?php } ?>
        </td>
    </tr>
</table>


<?php
$markItUp = '';
foreach(Yii::app()->params['translatedLanguages'] as $l=>$lang){
    $suffix = $l === Yii::app()->params['defaultLanguage'] ? '' : '_' . $l;
    $markItUp .= '$("#'.$l.'Tab'.$model->id.' .page_content").markItUp(markitupHtmlSettings);';
    $tabs[$l.'Tab'.$model->id] = array(
                'title'=>$lang,
                'view'=>'_form',
                'data'=>array(
                    'form'=>$form,
                    'model'=>$model,
                    'suffix'=>$suffix,
                )
            );
}
$this->widget('TabView',array(
        'cssFile'=>false,
        'id'=>$this->id.'Tab_'.$model->id,
        'tabs'=>$tabs,
    )
); ?>


<table class="table">
    <tr>
        <td>
            <label for="Page_alias">Вложения</label>
        </td>
        <td>
            <div class="attache_holder">
                <?php
                foreach($model->file as $file) {
                    foreach ($file->multilangFile as $l=>$translate){
                        foreach ($file->ml->localizedAttributes as $attr){
                            $translates[$attr . '_' . $l] = $translate['l_' . $attr];
                        }
                    }

                    $data = array_merge($file->src0->attributes,$file->attributes,$translates);
                    if($data['is_image']){
                        $data['src'] = Yii::getPathOfAlias('application.data') . '/' . $data['src'];
                        $data['src'] = Yii::app()->image->createUrl('pageAttached', $data);
                    } else $data['src'] = Yii::app()->filestore->downloadLink($data['src']);
                    $this->renderPartial('attached', array('data' => $data));
                }
                ?>
            </div>
        </td>
        <td id="pageFiles_<?= $model['id'] ?>" style="width:250px"></td>
    </tr>
</table>

<?php $this->endWidget(); ?>
</div>
<script>
<?php if(!$model->isNewRecord){ ?>icoLoader('<?= $this->id ?>','<?= $model->id ?>');<?php } ?>
<?= $markItUp ?>
$('#<?= $this->id ?>Form_<?= $model->id ?>').on('click','.attache_holder .fa-times',function(){if(confirm('Удалить прикреплённый файл?')){
        var parDiv = $(this).parent();
        $.post('admin/default/fileDelete',{id:$(parDiv).data('fileId')});
        $(parDiv).remove();
    }});
pageFilesAttache('<?= $this->id ?>','<?= $model->id ?>');
$('#<?= $this->id ?>Form_<?= $model->id ?> .attache_holder').dragsort({ dragSelector: "div.ext" });
</script>
<span class="time_mem"><?= dbginfo() ?></span>