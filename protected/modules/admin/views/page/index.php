<?php

        $prnt = 0;
        if(isset($_GET['parent']))
            $prnt = (int)$_GET['parent'];

$this->widget('TabView',array(
        'cssFile'=>false,
        'id'=>$this->id . 'Tab',
        'tabs'=>array(
            $this->id . 'listTab' => array(
                'title'=>'Список',
                'view'=>$prnt==2 ? 'blog' : 'admin', // 2 - id blog root in table
                'data'=>array('model'=>$model),
            ),
            $this->id.'TabEmpty' => array(
                'title'=>'+',
            ),
        ),
    )
); ?>