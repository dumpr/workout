<form id="<?= $this->id ?>Form" action="/site/gridupdate" method="POST" onsubmit="return false;">

<?php

        $prnt = 0;
        if(isset($_GET['parent'])){
            $prnt = (int)$_GET['parent'];
            $sql = 'SELECT parent FROM '.Page::model()->tableSchema->rawName.' WHERE id='.$prnt;
            $upLevel = Yii :: app()->db->createCommand($sql)->queryScalar();
        }
        if(isset($_GET['parent']) && $_GET['parent'])
            $upLevel = '<span class="countChildren" onClick="gridShowChildren(\'page\','.$upLevel.')">&uarr;</span>';
        else $upLevel = '';

$this->widget('myGridView', array(
    'filter'=>$model,
    'dataProvider' => $model->search(),
    'template'=> "{items}\n{buttons}\n{pager}{pagesize}",
	'columns'=>array(
        array(
            'name'=>'',
            'header'=>CHtml::checkBox("selectAll",null,array("onChange"=>"selectAllCh(this)","id"=>false)),
            'value'=>function($data){
                return CHtml::label(CHtml::checkBox("selected[]",null,array("id"=>false,"value"=>$data->id)).CHtml::hiddenField("ids[]",$data->id,array("id"=>false)), false);
            },
            'type'=>'raw',
            'headerHtmlOptions'=>array('style'=>'width:30px'),
            'filter'=>'',
        ),
        array(
            'name' => 'name',
            'type' => 'raw',
            'value' => function($data,$row,$column){
                $cont = $column->grid->controller->id;
                return CHtml::link(
                    CHtml::encode($data->name),
                    "",
                    array(
                        "onClick"=>CHtml::ajax(
                            array(
                                "url"=>Yii::app()->createUrl("admin/".$cont."/update",array("id"=>$data->id)),
                                "type"=>"POST",
                                "data"=>false,
                                "beforeSend"=>"js:function() {noclone('".$cont."',".$data->id.");}",
                                "success"=>"js:function(data){newtab4edit('".$cont."Tab',".$data->id.",'".$data->name."',data);}",
                            )
                        )
                    )
                );
            },
        ),
        array(
            'name'=>'',
            'header'=>$upLevel,
            'value'=> function($data){
                if($data->countChildren)
                    return '<span class="countChildren" onClick="gridShowChildren('.$data->id.')">'.$data->countChildren.'</span>';
            },
            'type'=>'raw',
            'filter'=>'',
        ),
        /*array(
            'name'=>'path',
            'value'=>'CHtml::encode($data->path)',
            'type'=>'raw',
            'filter'=>Yii::app()->controller->paths,
        ),
        array(
            'name'=>'alias',
            'value'=>'CHtml::encode($data->alias)',
            'type'=>'raw',
        ),*/
        // 2 - BLOG
        array(
            'name'=>'created',
            'value'=>function($data){
                return Helper::rusDate('j F, H:i',$data->created);
            },
            'type'=>'raw',
        ),
        array(
            'name'=>'active',
            'value'=>function($data){
                return CHtml::label(
                    CHtml::checkBox("active[]",$data->active,array("id"=>false, "value"=>$data->id, "class"=>"active-item-input")), false);
            },
            'type'=>'raw',
            'filter'=>array(0=>"Нет",1=>"Да"),
            'headerHtmlOptions'=>array('style'=>'width:100px'),
        ),
    ),
));

?>

</form>