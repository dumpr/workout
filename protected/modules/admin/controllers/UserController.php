<?php

class UserController extends Controller
{

    public $layout = 'main';

	public function filters()
	{
		return array(
			'accessControl',
			'postOnly + delete',
		);
	}

    /**
     *
     */
    public function accessRules()
    {
        /*if(!Yii::app()->user->checkAccess('admin'))
                throw new CHttpException(404,'The requested page does not exist.');*/

        return array(
            array('allow',
                    'actions'=>array('index','update'),
        'roles'=>array('root'),
            ),
            array('deny',
                    'roles'=>array('*'),
            ),
        );
    }


    /**
     *
     */
    public function actionUpdate($id)
    {
    $model = $this->loadModel($id);
        //$this->performAjaxValidation($model,$profile);

        if(isset($_POST['User'])){
                if($model->save(false)){
                        $this->redirect(array('update','id'=>$model->id));
                }
        }

        $this->renderPartial('update',array('model'=>$model),false,true);
    }


    /**
     *
     */
    public function actionIndex()
    {

        if(isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $model = new User;
        //$count = $model->count('created_on > "'.  date('Y-m-d H:i:s',strtotime('-7 days')) .'"');
        $new = Yii::app()->db->createCommand('SELECT id FROM {{user}} WHERE created_on > "'.  date('Y-m-d H:i:s',strtotime('-7 days')) .'"')->queryColumn();
        $model->unsetAttributes();

        if(isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $cs = Yii::app()->clientScript->registerScript("usrs","$('a[href=#".$this->id."TabEmpty]').on('click',function(){var ids = [".implode(',',$new)."];$.each(ids, function(i,id) {fillTabs(id,'".$this->id."');});$(this).removeAttr('href');return false;})");
        $this->pageTitle = "Зарегистрированные пользователи";
        $this->render('index',array('model'=>$model,'users'=>count($new)));
    }

    /**
     * @param $id
     * @return mixed
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        // ->with('address.flat0.house0.street_name')
        $model = User::model()->safe()->findByPk($id);// ->with('profile','auth')
        if($model === null)
                throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}
