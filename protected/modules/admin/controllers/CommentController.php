<?php

class CommentController extends Controller
{

    public $layout = 'main';

	public function filters()
	{
		return array(
			'accessControl',
			'postOnly + delete',
		);
	}

	/**
	 *
	 */
	public function accessRules()
	{
		/*if(!Yii::app()->user->checkAccess('admin'))
			throw new CHttpException(404,'The requested page does not exist.');*/

		return array(
			array('allow',
				'actions'=>array('index','update'),
                'roles'=>array('root'),
			),
			array('deny',
				'roles'=>array('*'),
			),
		);
	}


	/**
	 *
	 */
	public function actionUpdate($id)
	{
            $model = $this->loadModel($id);
            //$this->performAjaxValidation($model,$profile);

            if(isset($_POST['Comment'])){
                $model->text = $_POST['Comment']['text'];
                $model->active = $_POST['Comment']['active'];
                if($model->save(false)){
                    $this->redirect(array('update','id'=>$model->id));
                }
            }

            $this->renderPartial('update',array('model'=>$model),false,true);
	}


	/**
	 *
	 */
	public function actionIndex()
	{

            if(isset($_GET['pageSize'])) {
                Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
                unset($_GET['pageSize']);
            }

            $model = new Comment;
                $model->unsetAttributes();

            if(isset($_GET['Comment'])){
                $model->attributes = $_GET['Comment'];
                $model->date_first = isset($_GET['Comment']['date_first']) ? $_GET['Comment']['date_first'] : '';
                $model->date_last = isset($_GET['Comment']['date_last']) ? $_GET['Comment']['date_last'] : '';
            }
            if(!isset($_GET['Comment']['active']) || $_GET['Comment']['active']===false)
                $model->active = 0;

            Yii::app()->clientScript->registerScript("newUser","");
            $this->pageTitle = "Комментарии пользователей";

            if (Yii::app()->request->isAjaxRequest)
                $this->renderPartial('index', array('model' => $model,),false,true);
            else
                $this->render('index', array('model' => $model));
	}

	/**
	 * @param $id
	 * @return mixed
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		// ->with('address.flat0.house0.street_name')
		$model = Comment::model()->with('user')->findByPk($id);// ->with('profile','auth')
		if($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
