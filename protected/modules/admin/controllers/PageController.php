<?php

/**
 *
 */
class PageController extends Controller {

    public $layout = 'main';
    public $new_alias = false;
    public $paths = array();

    /**
     *
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     *
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('update', 'index', 'create', 'upload'),
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     *
     */
    public function actionAttacheDelete()
    {
        if(isset($_POST['id'])){
            Yii::app()->filestore->deleteFile($_POST['id']);
            //File::model()->find('m_id='.(int)$_POST['id'].' AND m="page" AND src="'.$src.'"')->delete();
            return true;
        }
    }

    /**
     * Updates a particular model.
     * @param $id
     * @throws CException
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        if(!empty($id) && $id{0} !== 'n'){
            $model = $this->loadModel($id);
            //$this->performAjaxValidation($model);
        } else {
            $id = $_POST['Page']['id'];
            unset($_POST['Page']['id']);
            $model = new Page;
        }

        if (isset($_POST['Page'])) {
            $this->attacheSave($model->id);
            if ($model->alias !== $_POST['Page']['alias']) {
                $model->new_alias = true;
            }
            $model->attributes = $_POST['Page'];

            if ($model->save()) {
                $this->icoUpdate($_POST['pageIco'],$model);
                $this->redirect(array('update', 'id' => $model->id));
            } // else qw($model->errors,'e');
            else
                throw new CHttpException(403, CHtml::errorSummary($model, ''));
        }

        $this->renderPartial('update', array('model' => $model,), false, true);
    }


    /**
     *
     */
    public function attacheSave($id = false)
    {
        if(empty($id))
            return false;

        if(isset($_POST['Attache'])){
            $i = 0;
            foreach ($_POST['Attache'] as $f => $values){
                $model = File::model()->multilang()->findByPk($f);
                $model->attributes = $values;
//                $model->description = $values['description'];
//                $model->name = $values['name'];
                $model->order = $i;
                $model->active = 1;
                $model->save();
                $i++;
            }
        }
    }

    /**
     * @param $file_id int идент файла из таблицы file переданного формой
     * @param $m File model редактируемой статьи
     */
    private function icoUpdate($file_id, $m)
    {
        // если иконка не менялась
        if($file_id == $m->ico['id'])
            return true;

        Yii::app()->filestore->hideFiles('pageIco',$m->id);
        Yii::app()->filestore->activeFile($file_id);
    }

    /**
     * Загружает файлы по аяксу
     * @return json
     */
    public function actionUpload()
    {
        if (!isset($_GET['m']) || empty($_GET['m']) || !isset($_GET['m_id']) || empty($_GET['m_id']))
            Yii::app()->end();

        if (isset($_SERVER['HTTP_X_FILE_NAME'])) {
            $m_id = $_GET['m_id'];
            $m = $_GET['m'];

            if (!Yii::app()->filestore->store($m, $m_id))
                Yii::app()->end();

            header('Content-Type: application/json');
            echo CJSON::encode(Yii::app()->image->getResult($_GET['preset']));
        }
        Yii::app()->end();
    }


    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $model = new Page;

        $model->unsetAttributes();
        if (isset($_GET['Page']))
            $model->attributes = $_GET['Page'];

        $asset = Yii::app()->assetManager->getPublishedUrl(Yii::getPathOfAlias('application.modules.admin.assets'));
        Yii::app()->clientScript->registerScriptFile($asset . '/datetimepicker/js/moment.min.js');
        Yii::app()->clientScript->registerScriptFile($asset . '/datetimepicker/js/bootstrap-datetimepicker.min.js');
        Yii::app()->clientScript->registerScriptFile($asset . '/datetimepicker/locales/bootstrap-datetimepicker.ru.js');
        Yii::app()->clientScript->registerCssFile($asset . '/datetimepicker/css/bootstrap-datetimepicker.min.css');

        Yii::app()->clientScript->registerScript("newItem", "$('a[href=#" . $this->id . "TabEmpty]').on('click',function(){jQuery.ajax({'url':'/admin/" . $this->id . "/create','type':'POST','data':false,'success':function(data){newtab4new('" . $this->id . "Tab',data);},'cache':false});return false;})");
        $this->pageTitle = "Информационные страницы";
        if (Yii::app()->request->isAjaxRequest)
            $this->renderPartial('index', array('model' => $model,));
        else
            $this->render('index', array('model' => $model));
    }

    /**
     * Creates a new model.
     */
    public function actionCreate()
    {
        $model = new Page;
        $model->id = 'n' . rand(1000, 9999);
        $model->created = date('Y-m-d H:i:s');
        $this->renderPartial('update', array('model' => $model, false, true));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Page the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Page::model()->multilang()->with('file:multilang.src0','ico')->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Page $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'page-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
