<?php

class DefaultController extends Controller
{

    public $layout = 'main';

//    public $defaultAction = 'index';

	/**
	 *
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	 *
	 */
	public function accessRules()
	{
        /*if(!Yii::app()->user->checkAccess('admin'))
			throw new CHttpException(404,'The requested page does not exist.');*/

		return array(
			array('allow',
				'actions'=>array('index','gridUpdate'),
				'roles'=>array('admin'),
			),
			array('deny',
				'roles'=>array('*'),
			),
		);
	}


    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->render('index');
    }


    /**
     *
     */
    public function actionFileDelete()
    {
        if (isset($_POST['id']) && is_numeric($_POST['id'])){
            // Yii::app()->filestore->deleteFile($_POST['src']);
            Yii::app()->filestore->hideFile($_POST['id']);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDeleteOne()
    {
        $m = ucfirst($_POST['m']);
        $id = (int)$_POST['id'];
        $model = $m::model()->findByPk($id);
        if($model){
            $model->delete();
            echo "Удаление прошло успешно";
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        // if(!isset($_GET['ajax']))
        //     $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }


    /**
     *
     */
    public function actionGridUpdate()
    {
        if (empty($_POST['model']) || !is_array($_POST['ids']))
            return false;

        $mn = ucfirst($_POST['model']);
        $model = new $mn;

        if(isset($_POST['dosave']))
            $this->dosave($model->tableSchema->name);

        if(isset($_POST['selected']) && is_array($_POST['selected']) && !empty($_POST['selected']))
        {
            if (isset($_POST['dodeactivate']))
                $this->dodeactivate($model->tableSchema->name);
            if (isset($_POST['doactivate']))
                $this->doactivate($model->tableSchema->name);
            if (isset($_POST['dohidedelete']))
                $this->dohidedelete($model->tableSchema->name);
            if (isset($_POST['dohidenban'])){
                $this->dohidenban($model->tableSchema->name);
            }
            if (isset($_POST['docopy']))
                $this->docopy($model);

            if (isset($_POST['dodelete'])){
                foreach($_POST['selected'] as $k=>$id){
                    $m = $model->findByPk($id);
                    if($m && !$m->delete())
                        $error[] = $m->name;
                }

                if(isset($error)){
                    if(count($error)>1)
                        echo '"'.implode('", "',$error).'" не могут быть удалены, так как имеют вложенные статьи.';
                    else echo '"'.$error[0].'" не может быть удалена, так как имеет вложенные статьи.';

                } else echo "Удаление прошло успешно";
            }
        }
        Yii::app()->end();
    }

    /**
     *
     * @param type $model
     */
    private function dodeactivate($tb)
    {
        $sql = "UPDATE ".$tb." t
SET t.active = 0
WHERE t.id IN (".implode(',', $_POST['selected']).")";
        Yii::app()->db->createCommand($sql)->execute();
    }

    /**
     *
     * @param type $tb
     */
    private function doactivate($tb)
    {
        $sql = "UPDATE ".$tb." t
SET t.active = 1
WHERE t.id IN (".implode(',', $_POST['selected']).")";
        Yii::app()->db->createCommand($sql)->execute();
    }

    /**
     *
     * @param type $model
     */
    private function dohidedelete($tb)
    {
        $sql = "UPDATE ".$tb." t
SET t.active = '-1'
WHERE t.id IN (".implode(',', $_POST['selected']).")";
        Yii::app()->db->createCommand($sql)->execute();
    }

    /**
     *
     * @param type $model
     */
    private function dohidenban($tb)
    {
        $sql = "UPDATE {{user}} SET is_active = 0, is_disabled = 1 WHERE id IN ("
                . "SELECT DISTINCT user_id FROM ".$tb." "
                . "WHERE id IN (".implode(',', $_POST['selected'])."))";
        $ids = Yii::app()->db->createCommand($sql)->execute();
        $this->dohidedelete($tb);
        $sql = "UPDATE ".$tb." t
SET t.active = '-1'
WHERE t.id IN (".implode(',', $_POST['selected']).")";
        Yii::app()->db->createCommand($sql)->execute();
    }

        /**
     *
     */
    private function docopy($model)
    {
        $tb = $model->tableSchema->name;
        $ids = array();
        $criteria = new CDbCriteria();
        $criteria->addInCondition('t.id',$_POST['selected']);
        foreach($model->findAll($criteria) as $row){
            $row['active'] = 0;
            if(isset($row['alias']))
                $row['alias'] = $row['alias'].'-'.Helper::passGen(4);
            $row['name'] = $row['name'] . ' (копия)';

            $mn = ucfirst($_POST['model']);
            $new = new $mn;
            $new->attributes = $row->attributes;
            if(isset($new->created))
                $new->created = date('Y-m-d H:i:s');
            unset($new->modified,$new->id);
            if($new->save())
                $ids[] = $new->id;
            else qw($new->errors,'l');

            /*$sql = "INSERT INTO ".$tb." (".implode(',',array_keys($attr)).") VALUES(:".implode(', :',array_keys($attr)).")";
qw($sql,'e');
            $command = Yii::app()->db->createCommand($sql);
            foreach($attr as $key=>&$value)
                $command->bindParam(":".$key,$value,PDO::PARAM_STR);
            $ok = $command->execute();
            $new_id = Yii::app()->db->getLastInsertID();*/
        }
        header('Content-type: application/json');
        echo json_encode($ids);
    }


    /**
     *
     */
    private function dosave($tb)
    {

        if(isset($_POST['orders']) && sizeof($_POST['ids']) == sizeof($_POST['orders'])){
            $sql = "UPDATE ".$tb." t SET t.active = 0, t.order = CASE t.id ";
            $i = 0;
            foreach ($_POST['ids'] as $id) {
                $sql .= sprintf("
WHEN %d THEN %d ", $id, $_POST['orders'][$i]);
                $i++;
            }
            $sql .= "END
WHERE t.id IN (".implode(',', $_POST['ids']).")";
            Yii::app()->db->createCommand($sql)->execute();
        }

        // статус
        if(isset($_POST['menu'])){
            $sql = "UPDATE ".$tb." t
SET t.menu = 0
WHERE t.id IN (".implode(',', $_POST['ids']).")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "UPDATE ".$tb." t
SET t.menu = 1
WHERE t.id IN (".implode(',', $_POST['menu']).")";
            Yii::app()->db->createCommand($sql)->execute();
        }

        // статус
        if(isset($_POST['active'])){
            $sql = "UPDATE ".$tb." t
SET t.active = 1
WHERE t.id IN (".implode(',', $_POST['active']).")";
            Yii::app()->db->createCommand($sql)->execute();
        }
        echo "Изменения успешно сохранены";
    }

}