<?php

class ParamController extends Controller
{

    public $layout = 'main';

    public $defaultAction = 'index';

    public $pars = array();

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	 *
	 */
	public function accessRules()
	{
	    /*if(!Yii::app()->user->role)
			throw new CHttpException(404,'The requested page does not exist.');*/
		return array(
			array('allow',
				'actions'=>array('index','update','uploader'),
				'roles'=>array('admin'),
			),
			array('deny',
				'roles'=>array('*'),
			),
		);
	}

    /**
     *
     */
    public function actionIndex()
    {

        if(isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }

		$model = new Param();
		$model->unsetAttributes();
		if(isset($_GET['Param']))
			$model->attributes = $_GET['Param'];
        else $model->par = 'main';

        $this->pageTitle = 'Основные настройки сайта';
        $this->pars = array_filter(CHtml::listData(Param::model()->pars()->findAll(),"par","par"));
		$this->render('admin',array('model'=>$model));
    }

    /**
     *
     */
    /*public function actionUploader()
    {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $ext = array('png');
        $maxsize = 50*1024;
        $uploader = new qqFileUploader($ext, $maxsize);
        $result = $uploader->handleUpload($_SERVER['DOCUMENT_ROOT'].'/uploads/watermark/');
        $result['filename'] = 'uploads/watermark/'.$result['filename'];

        if(isset($result)){
            $sql = "UPDATE {{params}} SET value = :value WHERE name = 'watermark'";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(':value', $result['filename'],PDO::PARAM_STR);
            $command->execute();
            Yii::app()->cache->delete('params');
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        Yii::app()->end();
    }*/

    /**
     *
     */
    public function actionUpdate()
    {
        if(isset($_POST['Param']['par'])) {
            $par = $_POST['Param']['par'];
            unset($_POST['Param']);
            $sql = "UPDATE {{param}} SET value = :value WHERE name = :name";
            if($par) $sql .= " AND par = '".$par."'";
            $command = Yii::app()->db->createCommand($sql);
            foreach($_POST as $n=>$v){
                //if(strpos($p['name'],'ccess_')) $p['value'] = str_replace(' ','',$p['value']);
                $command->bindParam(':value', $v,PDO::PARAM_STR);
                $command->bindParam(':name', $n,PDO::PARAM_STR);
                $command->execute();
            }
        }
		$this->redirect(array('index','Param[par]'=>$par));

        // Yii::app()->cache->delete('params');
        // $this->redirect('index'); forAjax
    }

}
