<?php

class AdminModule extends CWebModule
{
	public function init()
	{
		if(!Yii::app()->user->checkAccess('adminPanel'))
			Yii::app()->request->redirect(Yii::app()->createUrl('usr/default/login'));
	    /*if(!isset(Yii::app()->user->role) || Yii::app()->user->role == "guest")
            Yii::app()->request->redirect('/login');*/
			// throw new CHttpException(404,'The requested page does not exist.');

		$this->setModules(array('rbacui'));
		// import the module-level models and components
		$this->setImport(array(
			'admin.models.*',
			'admin.widgets.*',
        	'admin.components.*',
		));
	}

    /*
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
    */
}
