<?php

Yii::import('zii.widgets.grid.CGridView');

class myGridView extends CGridView
{
    public $cssFile = false;

    public $itemsCssClass = 'table table-striped table-bordered'; //  bootstrap-datatable

    public $template = "{items}\n{pager}{pagesize}";

    public function renderPagesize()
    {
        $pageSize = Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']);
        echo '<div class="pagesize pull-right"><p>Показывать по:';
        foreach (array('10','20','50','100') as $s){
            if($pageSize == $s)
                echo '<span style="cursor:text"> '.$s.' </span>';
            else
                echo '<span onClick="$.fn.yiiGridView.update(\''.$this->controller->id.'Grid\',{ data: { pageSize:'.$s.' }})"> '.$s.' </span>';
        }
        echo '</p></div>';
    }

    public function renderCommentButtons() {
        echo "<div class='gridButtons'>";
        echo CHtml::submitButton('Удалить', array(
                        "name"=>"dohidedelete",
                        "class"=>"btn btn-danger",
                        "onClick"=>"gridelete(this);"));
        echo CHtml::submitButton('.. и забанить автора', array(
                        "name"=>"dohidenban",
                        "class"=>"btn btn-danger",
                        "onClick"=>"gridelete(this);"));
        echo CHtml::submitButton('Временно не показывать', array(
                        "name"=>"dodeactivate",
                        "class"=>"btn btn-info",
                        "onClick"=>"gridupdate(this);"));
        echo CHtml::submitButton('Активировать', array(
                        "name"=>"doactivate",
                        "class"=>"btn btn-primary",
                        "onClick"=>"gridupdate(this);"));
        echo "</div>";
    }

    public function renderDeleteButton()
    {
        echo "<div class='gridButtons'>";
        echo CHtml::submitButton('Удалить', array(
                        "name"=>"dodelete",
                        "class"=>"btn btn-default",
                        "onClick"=>"gridelete(this);"));
        echo "</div>";
    }

    public function renderSaveButton()
    {
        echo "<div class='gridButtons'>";
        echo CHtml::submitButton('Сохранить?', array(
                        "name"=>"dosave",
                        "class"=>"btn btn-primary btn-sm okbutton right-btn",
                        "onClick"=>"gridupdate(this);"));
        echo "</div>";
    }

    public function renderReviewsButtons()
    {
        echo "<div class='gridButtons'>";
        echo CHtml::submitButton('Удалить', array(
                        "name"=>"dodelete",
                        "class"=>"btn btn-default",
                        "onClick"=>"gridelete(this);"));
        echo CHtml::submitButton('Сохранить?', array(
                        "name"=>"dosave",
                        "class"=>"btn btn-primary btn-sm okbutton right-btn",
                        "onClick"=>"gridupdate(this);"));
        echo "</div>";
    }

    public function renderParamsave()
    {
        echo "<div class='gridButtons'>";
        echo CHtml::submitButton('Сохранить?', array(
                        "name"=>"dosave",
                        "class"=>"btn btn-primary btn-sm right-btn",
                        "onClick"=>"paramSubmit(this);"));
        echo "</div>";
    }

    public function renderButtons()
    {
        echo "<div class='gridButtons'>";
        echo CHtml::submitButton('Копировать', array(
                        "name"=>"docopy",
                        "class"=>"btn btn-info",
                        "onClick"=>"gridcopy(this);"));
        echo CHtml::submitButton('Редактировать', array(
                        "name"=>"doedit",
                        "class"=>"btn btn-info",
                        "onClick"=>"gridedit(this);"));
        echo CHtml::submitButton('Удалить', array(
                        "name"=>"dodelete",
                        "class"=>"btn btn-danger",
                        "onClick"=>"gridelete(this);"));
        echo CHtml::submitButton('Добавить', array(
                        "name"=>"add",
                        "class"=>"addnewbutton",
                        "class"=>"btn btn-success",
                        "onClick"=>CHtml::ajax(
                            array(
                                "url"=>Yii::app()->createUrl("admin/".$this->controller->id."/create"),
                                "type"=>"POST",
                                "data"=>false,
                                "success"=>"js:function(data){newtab4new('".$this->controller->id."Tab',data);}",
                            )
                        )));
        echo CHtml::submitButton('Сохранить?', array(
                        "name"=>"dosave",
                        "class"=>"btn btn-primary btn-sm okbutton right-btn",
                        "onClick"=>"gridupdate(this);"));
        echo "</div>";
    }

	public function init()
	{
        $this->id = $this->controller->id . 'Grid';
        $this->ajaxUpdate = $this->controller->id . 'Grid';
        $this->pager = array(
            'cssFile' => false,
            'header' => '<span class="pager-header">Страница: </span>',
            'maxButtonCount' => 5,
            'prevPageLabel' => '<',
            'nextPageLabel' => '>',
            'firstPageLabel' => '<<',
            'lastPageLabel' => '>>',
        );
		parent::init();
    }
}
/*
        -> OLD
"onClick"=>CHtml::ajax(
                            array(
                                "url"=>Yii::app()->createUrl($k."/create"),
                                "type"=>"POST",
                                "data"=>false,
                                "success"=>"js:function(data){newtab4new('".$k."Tab',data);}",
                            )
                        )
CHtml::dropDownList(
            'pageSize',
            Yii::app()->user->getState('pageSize',$pageSize),
            array(20=>20,50=>50,100=>100),
            array('onchange'=>'$.fn.yiiGridView.update("'.$this->cont.'Grid",{ data:{pageSize: $(this).val() }})')
        )
public $pager = array(
        'class' => 'CListPager',
        'htmlOptions' => array(
            'onchange'=>'$.fn.yiiGridView.update("'.$cont.'Grid",{ data:{'.ucfirst($cont).'_page: $("#'.$cont.'Grid .pageSelector option:selected").text() }})',
            'class'=>'pageSelector')
    );
*/
?>