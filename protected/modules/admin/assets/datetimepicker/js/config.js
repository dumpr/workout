$.fn.datetimepicker.defaults = {
    pickDate: true,                 //en/disables the date picker
    pickTime: true,                 //en/disables the time picker
    useMinutes: true,               //en/disables the minutes picker
    useSeconds: true,               //en/disables the seconds picker
    minuteStepping:1,               //set the minute stepping
    startDate:1/1/1970,             //set a minimum date
    endDate: (today +50 years),     //set a maximum date
    language:'en',                  //sets language locale
    defaultDate:"",                 //sets a default date, accepts js dates, strings and moment objects
    disabledDates:[],               //an array of dates that cannot be selected
    //enabledDates:1/1/1970,         //an array of dates that can be selected
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
    },
    useStrict: false,             //use "strict" when validating dates
};