/**
 * jQuery Yii plugin file.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright 2008-2010 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
;(function($) {

	$.extend($.fn, {
		yiitab: function() {

			function activate(id) {
				var pos = id.indexOf("#");
				if (pos>=0) {
					id = id.substring(pos);
				}
				var $tab=$(id);
				var $container=$tab.parent();
				$container.find('>ul a').removeClass('active');
				$container.find('>ul a[href="'+id+'"]').addClass('active');
				$container.children('div').hide();
				$tab.show();
			}

			$('.tabs').on('click','li > a', function(event) { // $.fn.yiiGridView.update
                event.preventDefault();
				var href=$(this).attr('href');
				var pos=href.indexOf('#');
				activate(href);
				if(pos==0 || (pos>0 && (window.location.pathname==''
                    || window.location.pathname==href.substring(0,pos))))
					return false;
			});
			// activate a tab based on the current anchor
			var url = decodeURI(window.location);
			var pos = url.indexOf("#");
			if (pos >= 0) {
				var id = url.substring(pos);
				if (this.find('>ul a[href="'+id+'"]').length > 0) {
					activate(id);
					return;
				}
			}
		}
	});

})(jQuery);

/**
 * jQuery Yii GridView plugin file.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright 2008-2010 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
(function(e){var d,b,c={},a=[];d=function(f){var g=a[f],h=e("#"+f).find("."+g.tableClass);h.children("tbody").find("input.select-on-check").filter(":checked").each(function(){e(this).closest("tr").addClass("selected")});h.children("thead").find("th input").filter('[type="checkbox"]').each(function(){var i=this.name.substring(0,this.name.length-4)+"[]",j=e("input[name='"+i+"']",h);this.checked=j.length>0&&j.length===j.filter(":checked").length});return this};b={init:function(f){var g=e.extend({ajaxUpdate:[],ajaxVar:"ajax",ajaxType:"GET",pagerClass:"pager",loadingClass:"loading",filterClass:"filters",tableClass:"items",selectableRows:1},f||{});g.tableClass=g.tableClass.replace(/\s+/g,".");return this.each(function(){var j,i=e(this),m=i.attr("id"),k="#"+m+" ."+g.pagerClass.replace(/\s+/g,".")+" a",h="#"+m+" ."+g.tableClass+" thead th a.sort-link",l="#"+m+" ."+g.filterClass+" input, #"+m+" ."+g.filterClass+" select";g.updateSelector=g.updateSelector.replace("{page}",k).replace("{sort}",h);g.filterSelector=g.filterSelector.replace("{filter}",l);a[m]=g;if(g.ajaxUpdate.length>0){e(document).on("click.yiiGridView",g.updateSelector,function(){if(g.enableHistory&&window.History.enabled){var n=e(this).attr("href").split("?"),o=e.deparam.querystring("?"+(n[1]||""));delete o[g.ajaxVar];window.History.pushState(null,document.title,decodeURIComponent(e.param.querystring(n[0],o)))}else{e("#"+m).yiiGridView("update",{url:e(this).attr("href")})}return false})}e(document).on("change.yiiGridView keydown.yiiGridView",g.filterSelector,function(o){if(o.type==="keydown"){if(o.keyCode!==13){return}else{j="keydown"}}else{if(j==="keydown"){j="";return}}var p=e(g.filterSelector).serialize();if(g.pageVar!==undefined){p+="&"+g.pageVar+"=1"}if(g.enableHistory&&g.ajaxUpdate!==false&&window.History.enabled){var n=e("#"+m).yiiGridView("getUrl"),q=e.deparam.querystring(e.param.querystring(n,p));delete q[g.ajaxVar];window.History.pushState(null,document.title,decodeURIComponent(e.param.querystring(n.substr(0,n.indexOf("?")),q)))}else{e("#"+m).yiiGridView("update",{data:p})}return false});if(g.enableHistory&&g.ajaxUpdate!==false&&window.History.enabled){e(window).bind("statechange",function(){var n=window.History.getState();e("#"+m).yiiGridView("update",{url:n.url})})}if(g.selectableRows>0){d(this.id);e(document).on("click.yiiGridView","#"+m+" ."+g.tableClass+" > tbody > tr",function(s){var r,o,q,p,n=e(s.target);if(n.closest("td").is(".empty,.button-column")||(s.target.type==="checkbox"&&!n.hasClass("select-on-check"))){return}o=e(this);r=e("#"+m);p=e("input.select-on-check",r);q=o.toggleClass("selected").hasClass("selected");if(g.selectableRows===1){o.siblings().removeClass("selected");p.prop("checked",false)}e("input.select-on-check",o).prop("checked",q);e("input.select-on-check-all",r).prop("checked",p.length===p.filter(":checked").length);if(g.selectionChanged!==undefined){g.selectionChanged(m)}});if(g.selectableRows>1){e(document).on("click.yiiGridView","#"+m+" .select-on-check-all",function(){var q=e("#"+m),p=e("input.select-on-check",q),n=e("input.select-on-check-all",q),o=q.find("."+g.tableClass).children("tbody").children();if(this.checked){o.addClass("selected");p.prop("checked",true);n.prop("checked",true)}else{o.removeClass("selected");p.prop("checked",false);n.prop("checked",false)}if(g.selectionChanged!==undefined){g.selectionChanged(m)}})}}else{e(document).on("click.yiiGridView","#"+m+" .select-on-check",false)}})},getKey:function(f){return this.children(".keys").children("span").eq(f).text()},getUrl:function(){var f=a[this.attr("id")].url;return f||this.children(".keys").attr("title")},getRow:function(f){var g=a[this.attr("id")].tableClass;return this.find("."+g).children("tbody").children("tr").eq(f).children()},getColumn:function(f){var g=a[this.attr("id")].tableClass;return this.find("."+g).children("tbody").children("tr").children("td:nth-child("+(f+1)+")")},update:function(g){var f;if(g&&g.error!==undefined){f=g.error;delete g.error}return this.each(function(){var i,h=e(this),k=h.attr("id"),j=a[k];g=e.extend({type:j.ajaxType,url:h.yiiGridView("getUrl"),success:function(m){var l=e("<div>"+m+"</div>");e.each(j.ajaxUpdate,function(n,p){var o="#"+p;e(o).replaceWith(e(o,l))});if(j.afterAjaxUpdate!==undefined){j.afterAjaxUpdate(k,m)}if(j.selectableRows>0){d(k)}},complete:function(){c[k]=null;h.removeClass(j.loadingClass)},error:function(n,p,o){var l,m;if(n.readyState===0||n.status===0){return}if(f!==undefined){l=f(n);if(l!==undefined&&!l){return}}switch(p){case"timeout":m="The request timed out!";break;case"parsererror":m="Parser error!";break;case"error":if(n.status&&!/^\s*$/.test(n.status)){m="Error "+n.status}else{m="Error"}if(n.responseText&&!/^\s*$/.test(n.responseText)){m=m+": "+n.responseText}break}if(j.ajaxUpdateError!==undefined){j.ajaxUpdateError(n,p,o,m)}else{if(m){alert(m)}}}},g||{});if(g.type==="GET"){if(g.data!==undefined){g.url=e.param.querystring(g.url,g.data);g.data={}}}else{if(g.data===undefined){g.data=e(j.filterSelector).serialize()}}if(c[k]!=null){c[k].abort()}h.addClass(j.loadingClass);if(j.ajaxUpdate!==false){if(j.ajaxVar){g.url=e.param.querystring(g.url,j.ajaxVar+"="+k)}if(j.beforeAjaxUpdate!==undefined){j.beforeAjaxUpdate(k,g)}c[k]=e.ajax(g)}else{if(g.type==="GET"){window.location.href=g.url}else{i=e('<form action="'+g.url+'" method="post"></form>').appendTo("body");if(g.data===undefined){g.data={}}if(g.data.returnUrl===undefined){g.data.returnUrl=window.location.href}e.each(g.data,function(l,m){i.append(e('<input type="hidden" name="t" value="" />').attr("name",l).val(m))});i.submit()}}})},getSelection:function(){var g=a[this.attr("id")],h=this.find(".keys span"),f=[];this.find("."+g.tableClass).children("tbody").children().each(function(j){if(e(this).hasClass("selected")){f.push(h.eq(j).text())}});return f},getChecked:function(f){var g=a[this.attr("id")],i=this.find(".keys span"),h=[];if(f.substring(f.length-2)!=="[]"){f=f+"[]"}this.find("."+g.tableClass).children("tbody").children("tr").children("td").children('input[name="'+f+'"]').each(function(j){if(this.checked){h.push(i.eq(j).text())}});return h}};e.fn.yiiGridView=function(f){if(b[f]){return b[f].apply(this,Array.prototype.slice.call(arguments,1))}else{if(typeof f==="object"||!f){return b.init.apply(this,arguments)}else{e.error("Method "+f+" does not exist on jQuery.yiiGridView");return false}}};e.fn.yiiGridView.settings=a;e.fn.yiiGridView.getKey=function(g,f){return e("#"+g).yiiGridView("getKey",f)};e.fn.yiiGridView.getUrl=function(f){return e("#"+f).yiiGridView("getUrl")};e.fn.yiiGridView.getRow=function(g,f){return e("#"+g).yiiGridView("getRow",f)};e.fn.yiiGridView.getColumn=function(g,f){return e("#"+g).yiiGridView("getColumn",f)};e.fn.yiiGridView.update=function(g,f){e("#"+g).yiiGridView("update",f)};e.fn.yiiGridView.getSelection=function(f){return e("#"+f).yiiGridView("getSelection")};e.fn.yiiGridView.getChecked=function(g,f){return e("#"+g).yiiGridView("getChecked",f)}})(jQuery);



/*
 * jQuery BBQ: Back Button & Query Library - v1.3pre - 8/26/2010
 * http://benalman.com/projects/jquery-bbq-plugin/
 *
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($,r){var h,n=Array.prototype.slice,t=decodeURIComponent,a=$.param,j,c,m,y,b=$.bbq=$.bbq||{},s,x,k,e=$.event.special,d="hashchange",B="querystring",F="fragment",z="elemUrlAttr",l="href",w="src",p=/^.*\?|#.*$/g,u,H,g,i,C,E={};function G(I){return typeof I==="string"}function D(J){var I=n.call(arguments,1);return function(){return J.apply(this,I.concat(n.call(arguments)))}}function o(I){return I.replace(H,"$2")}function q(I){return I.replace(/(?:^[^?#]*\?([^#]*).*$)?.*/,"$1")}function f(K,P,I,L,J){var R,O,N,Q,M;if(L!==h){N=I.match(K?H:/^([^#?]*)\??([^#]*)(#?.*)/);M=N[3]||"";if(J===2&&G(L)){O=L.replace(K?u:p,"")}else{Q=m(N[2]);L=G(L)?m[K?F:B](L):L;O=J===2?L:J===1?$.extend({},L,Q):$.extend({},Q,L);O=j(O);if(K){O=O.replace(g,t)}}R=N[1]+(K?C:O||!N[1]?"?":"")+O+M}else{R=P(I!==h?I:location.href)}return R}a[B]=D(f,0,q);a[F]=c=D(f,1,o);a.sorted=j=function(J,K){var I=[],L={};$.each(a(J,K).split("&"),function(P,M){var O=M.replace(/(?:%5B|=).*$/,""),N=L[O];if(!N){N=L[O]=[];I.push(O)}N.push(M)});return $.map(I.sort(),function(M){return L[M]}).join("&")};c.noEscape=function(J){J=J||"";var I=$.map(J.split(""),encodeURIComponent);g=new RegExp(I.join("|"),"g")};c.noEscape(",/");c.ajaxCrawlable=function(I){if(I!==h){if(I){u=/^.*(?:#!|#)/;H=/^([^#]*)(?:#!|#)?(.*)$/;C="#!"}else{u=/^.*#/;H=/^([^#]*)#?(.*)$/;C="#"}i=!!I}return i};c.ajaxCrawlable(0);$.deparam=m=function(L,I){var K={},J={"true":!0,"false":!1,"null":null};$.each(L.replace(/\+/g," ").split("&"),function(O,T){var N=T.split("="),S=t(N[0]),M,R=K,P=0,U=S.split("]["),Q=U.length-1;if(/\[/.test(U[0])&&/\]$/.test(U[Q])){U[Q]=U[Q].replace(/\]$/,"");U=U.shift().split("[").concat(U);Q=U.length-1}else{Q=0}if(N.length===2){M=t(N[1]);if(I){M=M&&!isNaN(M)?+M:M==="undefined"?h:J[M]!==h?J[M]:M}if(Q){for(;P<=Q;P++){S=U[P]===""?R.length:U[P];R=R[S]=P<Q?R[S]||(U[P+1]&&isNaN(U[P+1])?{}:[]):M}}else{if($.isArray(K[S])){K[S].push(M)}else{if(K[S]!==h){K[S]=[K[S],M]}else{K[S]=M}}}}else{if(S){K[S]=I?h:""}}});return K};function A(K,I,J){if(I===h||typeof I==="boolean"){J=I;I=a[K?F:B]()}else{I=G(I)?I.replace(K?u:p,""):I}return m(I,J)}m[B]=D(A,0);m[F]=y=D(A,1);$[z]||($[z]=function(I){return $.extend(E,I)})({a:l,base:l,iframe:w,img:w,input:w,form:"action",link:l,script:w});k=$[z];function v(L,J,K,I){if(!G(K)&&typeof K!=="object"){I=K;K=J;J=h}return this.each(function(){var O=$(this),M=J||k()[(this.nodeName||"").toLowerCase()]||"",N=M&&O.attr(M)||"";O.attr(M,a[L](N,K,I))})}$.fn[B]=D(v,B);$.fn[F]=D(v,F);b.pushState=s=function(L,I){if(G(L)&&/^#/.test(L)&&I===h){I=2}var K=L!==h,J=c(location.href,K?L:{},K?I:2);location.href=J};b.getState=x=function(I,J){return I===h||typeof I==="boolean"?y(I):y(J)[I]};b.removeState=function(I){var J={};if(I!==h){J=x();$.each($.isArray(I)?I:arguments,function(L,K){delete J[K]})}s(J,2)};e[d]=$.extend(e[d],{add:function(I){var K;function J(M){var L=M[F]=c();M.getState=function(N,O){return N===h||typeof N==="boolean"?m(L,N):m(L,O)[N]};K.apply(this,arguments)}if($.isFunction(I)){K=I;return J}else{K=I.handler;I.handler=J}}})})(jQuery,this);



/*
 * jQuery hashchange event - v1.3 - 7/21/2010
 * http://benalman.com/projects/jquery-hashchange-plugin/
 *
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
//(function($,e,b){var c="hashchange",h=document,f,g=$.event.special,i=h.documentMode,d="on"+c in e&&(i===b||i>7);function a(j){j=j||location.href;return"#"+j.replace(/^[^#]*#?(.*)$/,"$1")}$.fn[c]=function(j){return j?this.bind(c,j):this.trigger(c)};$.fn[c].delay=50;g[c]=$.extend(g[c],{setup:function(){if(d){return false}$(f.start)},teardown:function(){if(d){return false}$(f.stop)}});f=(function(){var j={},p,m=a(),k=function(q){return q},l=k,o=k;j.start=function(){p||n()};j.stop=function(){p&&clearTimeout(p);p=b};function n(){var r=a(),q=o(m);if(r!==m){l(m=r,q);$(e).trigger(c)}else{if(q!==m){location.href=location.href.replace(/#.*/,"")+q}}p=setTimeout(n,$.fn[c].delay)}!d&&(function(){var q,r;j.start=function(){if(!q){r=$.fn[c].src;r=r&&r+a();q=$('<iframe tabindex="-1" title="empty"/>').hide().one("load",function(){r||l(a());n()}).attr("src",r||"javascript:0").insertAfter("body")[0].contentWindow;h.onpropertychange=function(){try{if(event.propertyName==="title"){q.document.title=h.title}}catch(s){}}}};j.stop=k;o=function(){return a(q.location.href)};l=function(v,s){var u=q.document,t=$.fn[c].domain;if(v!==s){u.title=h.title;u.open();t&&u.write('<script>document.domain="'+t+'"<\/script>');u.close();q.location.hash=v}}})();return j})()})(jQuery,this);
