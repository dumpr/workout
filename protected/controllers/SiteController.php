<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 *
	 */
	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 *
	 */
	public function actionView($route)
	{
		$array['model'] = $this->loadModel($route);
		$this->render('view', $array);
	}

	/**
	 * комментарий будет виден сразу, если отправлен зарегистрированным и активным пользователем
	 * у не зарегистрированных только после проверки админом или подтверждения пользователем своей почты
	 */
	public function actionAddComment()
	{
		if(!isset($_POST['CommentForm']))
			exit;

		$form = new CommentForm;
		$form->attributes = $_POST['CommentForm'];
		if($form->validate()){
			// комментарий прошёл валидацию, но ещё не сохранён
			$comment = new Comment;
			$comment->attributes = $form->attributes;
			if(Yii::app()->user->isGuest){
				$comment->active = 0;
				$user_id = Yii::app()->getModule('usr')->autoRegister($form);
				$comment->user_id = $user_id;
			} else $comment->user_id = Yii::app()->user->id;

			if(!$comment->save())
				qw($comment->errors,'e');
		} else qw($form->errors,'e');

		$model = $this->loadModel($comment->m_id);
		$this->renderPartial('comment/all_comments',array('comments'=>$model->comment,'commform'=>new CommentForm));
	}

	/**
	 * @param $src
	 */
	public function actionGetfile($src)
	{

	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$mail = Yii::app()->getModule('usr')->mailer;
				$mail->AddAddress(Yii::app()->params['adminEmail'], Yii::t('form','To Admin'));
				$mail->Subject = $_SERVER['HTTP_HOST'] . ' ' . Yii::t('form','Contact Form');
				$body = $this->renderPartial($mail->getPathViews().'.contact_form', array('model'=>$model->attributes,'user'=>UsrModule::user()), true);
				$full = $this->renderPartial($mail->getPathLayouts().'.email', array('content'=>$body), true);
				$mail->MsgHTML($full);
				if (!$mail->Send()) {
					Yii::log($mail->ErrorInfo, 'error');
					return false;
				}
				/*$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);*/
				Yii::app()->user->setFlash('contact',Yii::t('Thank you for contacting us. We will respond to you as soon as possible.'));
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}


	/**
	 *
	 */
	public function loadModel($route)
	{
		if(is_numeric($route))
			$cond = 't.id='.$route;
		else {
			$path = explode('/',trim($route,'/'));
			$alias = array_pop($path);
			$cond = 't.alias = "'.$alias.'" AND t.path = "'.implode('/',$path).'" AND t.active="1" AND t.created < NOW()';
		}
		$model = Page::model()->multilang()->with('comment.reply')->find($cond); // ->with('files') .user
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$this->metaGen($model);
		$this->breadcrumbs = array_reverse($this->makeBreadcrumbs($model->attributes),1);
		$this->breadcrumbs = array_filter($this->breadcrumbs);
		array_push($this->breadcrumbs,$model['name']);
		return $model;
	}


	/**
	 *
	 */
	public function metaGen($model)
	{
		$cs = Yii::app()->clientScript;
		if(!empty($model['description'.$this->l]))
			$cs->registerMetaTag($model['description'.$this->l], 'description');
		if(!empty($model['keywords'.$this->l]))
			$cs->registerMetaTag($model['keywords'.$this->l], 'keywords');
		if(!empty($model['title'.$this->l]))
			$this->pageTitle = $model['title'.$this->l];
		else $this->pageTitle = $model['name'];
	}


	/**
	* @param $id int идентификатор непосредственного родителя страницы,
	* для которой нужно вывести хлебные крошки
	* @return array возвращает массив из пар title=>$url
	*/
	public function makeBreadcrumbs($inside, $path = array())
	{
		$cur = Yii::app()->db->createCommand("SELECT t.id,t.parent,t.name,CONCAT_WS('/', t.path, t.alias) as path FROM {{page}} t WHERE t.id = " . $inside['parent'])->queryRow();
		$path[$cur['name']] = ltrim($cur['path'], "/");
		if ($cur['parent'] > 0)
			$path = $this->makeBreadcrumbs($cur, $path);

		return $path;
	}
}