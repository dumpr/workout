<?php

// http://www.yiiframework.com/doc/guide/1.1/ru/topics.i18n
return array(
    'My Web Application' => 'Моё интернет приложение',
	'Home' => 'Главная',
    'About' => 'О нас',
	'Office' => 'Офис',
	'Contacts' => 'Контакты',
	'Contact' => 'Контакты',
	'Login' => 'Вход',
    'Logout ({username})' => 'Выход ({username})',
    'Logout' => 'Выход',
    'Profile ({username})' => 'Профиль ({username})',
    'All Rights Reserved.' => 'Все права защищены.',
    'Copyright &copy; {n} by {sitename}.' => '{sitename} &copy; {n}.',
);

