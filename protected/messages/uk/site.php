<?php

// http://www.yiiframework.com/doc/guide/1.1/ru/topics.i18n
return array(
    'My Web Application' => 'Моя інтернет сторінка',
	'Home' => 'Головна',
    'About' => 'Про нас',
	'Office' => 'Офіс',
	'Contacts' => 'Контакти',
	'Contact' => 'Зв`язок',
	'Login' => 'Вхід',
    'Logout ({username})' => 'Вихід ({username})',
    'Logout' => 'Вихід',
    'Profile ({username})' => 'Профіль ({username})',
    'All Rights Reserved.' => 'Всі права захищено.',
    'Copyright &copy; {n} by {sitename}.' => '{sitename} &copy; {n}.',
);

