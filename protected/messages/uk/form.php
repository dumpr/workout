<?php

return array(
    'ID' => 'ID',
    'Parent' => 'Батьківська категорія',
    'Author' => 'Автор',
    'User' => 'Користувач',
    'Your Name' => 'Ваше ім\'я',
    'Firstname' => 'Ім\'я',
    'Lastname' => 'Прізвище',
    'Phone or Email address' => 'Телефон або адреса електронної пошти',
    'Email' => 'Електронна пошта',
    'Message' => 'Повідомлення',
    'Subject' => 'Тема',
    'Name' => 'Назва',
    'Title' => 'HTML заголовок',
    'Description' => 'Опис',
    'Keywords' => 'Ключові слова',
    'Text' => 'Текст',
    'URL' => 'Алиас',
    'Order' => 'Порядок сортування',
    'Active' => 'Статус',
    'Deleted' => 'Вилучений',
    'Copy' => 'Копія',
    'Do Copy' => 'Створити копію',
    'Edit' => 'Редагувати',
    'Save' => 'Зберегти',
    'Create' => 'Створити',
    'Created' => 'Створено',
    'Confirm' => 'Підтвердити',
    'Submit' => 'Відправити',
    'Confirmation' => 'Підтвердження',
    'Short description' => 'Короткий опис',
    'Verification Code' => 'Код перевірки',
    'Thank you for contacting us. We will respond to you as soon as possible.' => 'Повідомлення надіслано. Ми зв\'яжемося з Вами найближчим часом.',

    'There is no error, the file uploaded with success' => 'Помилок не сталося. Файл успішно завантажено',
    'The uploaded file exceeds the upload_max_filesize directive in php.ini' => 'Розмір завантаження прівишаєт дозволений сервером розмір',
    'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form' => 'Розмір завантаження прівишаєт допустимий до завантаження розмір',
    'The uploaded file was only partially uploaded' => 'Файл завантажений неповністю',
    'No file was uploaded' => 'Жодного файлу не було завантажено',
    'Missing a temporary folder' => 'Відсутня тимчасовий каталог',
    'Please enter the letters as they are shown in the image above.<br/>Letters are not case-sensitive.' => 'Будь ласка, введіть літери з картинки. <br/> Регістр не має значення.',
    'Fields with <span class="required">*</span> are required.' => 'Поля відмічені <span class = "required"> * </ span> обов\'язкові до заповнення.',

    'Contact Form' => 'Форма зворотнього зв\'язку',
    'Contact Us' => 'Надішліть нам повідомлення',
    'To Admin' => 'Адміністратору',
    'From site' => 'Пісьмо с сайта',

    'in response to' => 'у відповідь на',
    'Reply' => 'Відповісти',
    'Leave a comment' => 'Залишити коментар',
);