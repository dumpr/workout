<?php

/**
 * Простой и удобный способ логирования и вывода ошибок при отладке
 * По крайней мере для меня ;)
 * Аналог var_dump
 *
 * Используем:
 * В самом начале скрипта подключаем этот файл.
 * Для вывода содержимого переменной или объекта или пр.
 * вызываем функцию qw('переменная','e'),
 * где e указывает, что после вывода следует остановить работу скрипта
 * можно вызвать так qw('переменная','l')
 * l - указывает, что следует записать в лог и продолжить выполнение
 *
 * http://dump.com.ua/
 * 2bemate@gmail.com
 */

define("QW_LOG_DIR",$_SERVER["DOCUMENT_ROOT"] . "/protected/runtime/");

dbginfo();

function dbginfo()
{
    if (defined("QW_START_TIME")) {
        $mt = explode(" ", microtime());
        $t = $mt[1] + $mt[0];
        $m = memory_get_usage();
        $mp = memory_get_peak_usage();
        return "t:" . (substr(($t - QW_START_TIME), 0, 4)) . " m:" .
            substr($m / 1024 / 1024, 0, 4) . " (" .
                substr($mp / 1024 / 1024, 0, 4) . ")";
    } else
        define("QW_START_TIME", $_SERVER['REQUEST_TIME_FLOAT']);
}


function qw()
{
    $s = func_get_args();
    $e = array_pop($s);

    // document.cookie="dbgbm=1"
    $admin = ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' || isset($_COOKIE['dbgbm']));

    if (strpos($e, 'l') !== false){
        $debug_backtrace = debug_backtrace();
        $echo = qw_trace($e,$debug_backtrace);
        $log_name = QW_LOG_DIR . date('z') . ".html";
        $str = "\n\n" . date('Y-m-d H:i:s') . substr(microtime(), 1, 4) . "\n" . $echo;
        file_put_contents($log_name,$str,FILE_APPEND);
    } else {
        if($admin){
            $debug_backtrace = debug_backtrace();
            echo "<pre>" . qw_trace($e,$debug_backtrace) . dbginfo() . "</pre>\n";
        }
    }
    if($admin && strpos($e,'e')!==false)
        exit;
}

function qw_trace($e,$debug_backtrace)
{
    ob_start();
    $f = $a = array();
    foreach($debug_backtrace as $trace){
        if(isset($trace['file']))
            $f[] = $trace['file'] . ':' . $trace['line'];
    }
    foreach($debug_backtrace[0]['args'] as $arg){
        $a[] = $arg;
    }
    if (strpos($e, 'a') !== false) {
        array_pop($a);
        var_dump(array_reverse($f));
    } else {
        $f = array_shift($f);
        var_dump($f);
    }
    var_dump($a);

    return ob_get_clean();
}

?>