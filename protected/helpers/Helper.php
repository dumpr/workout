<?php

class Helper
{

	/**
	 * @param $filename
	 * @param $filetype
	 * @return string
	 *
	 * used as so
	 * <style type="text/css">
	 * .logo{background: url("<?= Helper::base64EncodeImage('img/logo.png','png') ?>") no-repeat right 5px}
	 * </style>
	 *
	 * or
	 * <img src="<?= Helper::base64EncodeImage('img/logo.png','png'); ?>" />
	 */
	public static function base64EncodeImage($filename=string,$filetype=string)
	{
		if ($filename) {
			$imgbinary = fread(fopen($filename, "r"), filesize($filename));
			return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
		}
	}


	/**
	* http://php.net/manual/ru/function.usort.php#function.usort.examples.closure
	*/
	public static function uSorter($key) {
		return function ($a, $b) use ($key) {
			return strnatcmp($a[$key], $b[$key]);
		};
	}


	/**
	* Rendom Password Generator
	* @param $len int длина возвращаемого пароля
	* @return string возвращает рендомный пароль
	*/
	public static function passGen($len = 8)
	{
		$pass = '';
		$chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
		for($i=0;$i<$len;$i++){
			shuffle($chars);
			$pass .= $chars{0};
		}
		return $pass;
	}

	/**
	 * Пользоваться этой функцией точно так же как стандартной date(), например
	 * print rus_date("l, j F Y");
	 * Вывод будет точно такой же как в date(), только по русски
	 * http://webi.ru/webi_files/rus_date.html
	 */
	public static function rusDate()
	{
		$translate['ru'] = array("am" => "дп", "pm" => "пп", "AM" => "ДП", "PM" => "ПП",
		"Monday" => "Понедельник", "Mon" => "Пн", "Tuesday" => "Вторник",
		"Tue" => "Вт", "Wednesday" => "Среда", "Wed" => "Ср", "Thursday" =>
		"Четверг", "Thu" => "Чт", "Friday" => "Пятница", "Fri" => "Пт",
		"Saturday" => "Суббота", "Sat" => "Сб", "Sunday" => "Воскресенье",
		"Sun" => "Вс", "January" => "января", "Jan" => "янв", "February" =>
		"февраля", "Feb" => "фев", "March" => "марта", "Mar" => "мар", "April"
		=> "апреля", "Apr" => "апр", "May" => "мая", "June" =>
		"июня", "Jun" => "июн", "July" => "июля", "Jul" => "июл", "August" =>
		"августа", "Aug" => "авг", "September" => "сентября", "Sep" =>
		"сен", "October" => "октября", "Oct" => "окт", "November" => "ноября",
		"Nov" => "ноя", "December" => "декабря", "Dec" => "дек", "st" => "ое",
		"nd" => "ое", "rd" => "е", "th" => "ое");
		// січ., лют., берез., квіт., трав., черв., лип., серп., верес, жовт., лис-топ., груд.
		$translate['uk'] = array("am" => "дп", "pm" => "пп", "AM" => "ДП", "PM" => "ПП",
		"Monday" => "Понеділок", "Mon" => "Пн", "Tuesday" => "Вівторок",
		"Tue" => "Вт", "Wednesday" => "Середа", "Wed" => "Ср", "Thursday" =>
		"Четвер", "Thu" => "Чт", "Friday" => "П`ятниця", "Fri" => "Пт",
		"Saturday" => "Субота", "Sat" => "Сб", "Sunday" => "Неділя",
		"Sun" => "Вс", "January" => "січня", "Jan" => "січ", "February" =>
		"лютого", "Feb" => "лют", "March" => "березня", "Mar" => "берез", "April"
		=> "квітня", "Apr" => "квіт", "May" => "травня", "June" =>
		"червня", "Jun" => "черв", "July" => "липня", "Jul" => "лип", "August" =>
		"серпня", "Aug" => "серп", "September" => "вересня", "Sep" =>
		"верес", "October" => "жовтня", "Oct" => "жовт", "November" => "листопада",
		"Nov" => "листоп", "December" => "грудня", "Dec" => "груд", "st" => "ого",
		"nd" => "е", "rd" => "е", "th" => "ого");

		if(isset($translate[Yii::app()->language]))
			$t = $translate[Yii::app()->language];
		else $t = array("  " => " ");

		if (func_num_args() > 1) {
			$ts = func_get_arg(1);
			if ($ts == false)
				return false;
			if(!is_numeric($ts))
				$ts = strtotime($ts);
			return strtr(date(func_get_arg(0), $ts), $t);
		} else
			return strtr(date(func_get_arg(0)), $t);
	}


	// http://yiiframework.ru/forum/viewtopic.php?f=3&t=16671#p98111
	public static function transliteration($from, $to = false)
	{
		if($to && $to=self::transliteration($to))
			return $to;

		// ГОСТ 7.79B
		$transliteration = array(
			'А' => 'A', 'а' => 'a',
			'Б' => 'B', 'б' => 'b',
			'В' => 'V', 'в' => 'v',
			'Г' => 'G', 'г' => 'g',
			'Д' => 'D', 'д' => 'd',
			'Е' => 'E', 'е' => 'e',
			'Ё' => 'Yo', 'ё' => 'yo',
			'Ж' => 'Zh', 'ж' => 'zh',
			'З' => 'Z', 'з' => 'z',
			'И' => 'I', 'и' => 'i',
			'Й' => 'J', 'й' => 'j',
			'К' => 'K', 'к' => 'k',
			'Л' => 'L', 'л' => 'l',
			'М' => 'M', 'м' => 'm',
			'Н' => 'N', 'н' => 'n',
			'О' => 'O', 'о' => 'o',
			'П' => 'P', 'п' => 'p',
			'Р' => 'R', 'р' => 'r',
			'С' => 'S', 'с' => 's',
			'Т' => 'T', 'т' => 't',
			'У' => 'U', 'у' => 'u',
			'Ф' => 'F', 'ф' => 'f',
			'Х' => 'H', 'х' => 'h',
			'Ц' => 'Cz', 'ц' => 'cz',
			'Ч' => 'Ch', 'ч' => 'ch',
			'Ш' => 'Sh', 'ш' => 'sh',
			'Щ' => 'Shh', 'щ' => 'shh',
			'Ъ' => '', 'ъ' => '',
			'Ы' => 'Y`', 'ы' => 'y`',
			'Ь' => '', 'ь' => '',
			'Э' => 'E`', 'э' => 'e`',
			'Ю' => 'Yu', 'ю' => 'yu',
			'Я' => 'Ya', 'я' => 'ya',
			' ' =>'-'
		);

		$str = strtr($from, $transliteration);
		$str = mb_strtolower($str,'UTF-8');
		$str = preg_replace('/[^0-9a-z\-]/', '', $str);
		$str = preg_replace('|([-]+)|s', '-', $str);
		$str = trim($str, '-');

		return $str;
	}


	/**
	 * @param $array
	 * @return mixed
	 */
	/*public static function normContentKey($array)
	{
		$pattern = "/(_n\d{4})|(_\d+)$/";
		foreach(array_keys($array) as $key){
			$nk = preg_replace($pattern,'',$key,1,$count);
			if($count == 1){
				$array[$nk] = $array[$key];
				unset($array[$key]);
			}
		}
		return $array;
	}*/


	/**
	 * @param bool $phone
	 * @return bool|string
	 */
	public static function phoneFormat($phone=false)
	{
		$phoneNumber = preg_replace('/[^0-9]/','',$phone);
		if(empty($phoneNumber))
			return false;

		$country = '';

		$len = strlen($phoneNumber);
		if($len>10)
			$country = substr($phoneNumber, 0, $len-10);
		if($country) $country .= ' ';

		$code = substr($phoneNumber, -10, 3);
		$startTree = substr($phoneNumber, -7, 3);
		$nextTwo = substr($phoneNumber, -4, 2);
		$lastTwo = substr($phoneNumber, -2, 2);

		return $country.'('.$code.') '.$startTree.'-'.$nextTwo.'-'.$lastTwo;
	}


	/**
	 * @param $str
	 * @return int|string
	 */
	public static function toBytes($str) {
		$val = trim($str);
		$last = strtolower($str[strlen($str) - 1]);
		switch ($last) {
			case 'g':
				$val *= 1024;
			case 'm':
				$val *= 1024;
			case 'k':
				$val *= 1024;
		}
		return $val;
	}
	/**
	 * Get either a Gravatar URL or complete image tag for a specified email address.
	 *
	 * @param string $email The email address
	 * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
	 * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
	 * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
	 * @param boole $img True to return a complete IMG tag False for just the URL
	 * @param array $atts Optional, additional key/value attributes to include in the IMG tag
	 * @return String containing either just a URL or a complete image tag
	 * @source http://gravatar.com/site/implement/images/php/
	 */
	public static function get_gravatar( $email, $s = 80, $d = 'identicon', $r = 'g', $img = false, $atts = array() ) {
		$url = 'http://www.gravatar.com/avatar/';
		$url .= md5( strtolower( trim( $email ) ) );
		$url .= "?s=$s&d=$d&r=$r";
		if ( $img ) {
			$url = '<img src="' . $url . '"';
			foreach ( $atts as $key => $val )
				$url .= ' ' . $key . '="' . $val . '"';
			$url .= ' />';
		}
		return $url;
	}
}
 ?>
