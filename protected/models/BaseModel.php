<?php

/**
 *
 */
class BaseModel extends CActiveRecord
{

	protected function getTranslatedLanguages()
	{
		$arr = Yii::app()->params['translatedLanguages'];
		unset($arr[Yii::app()->params['defaultLanguage']]);
		return $arr;
	}

}

?>