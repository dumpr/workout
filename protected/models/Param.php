<?php

/**
 *
 */
class Param extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{param}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>128),
			array('hidden', 'numerical', 'integerOnly'=>true),
			array('type, par', 'length', 'max'=>16),
			array('label', 'length', 'max'=>64),
			array('value, description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			// array('name, value, type, label, description', 'safe', 'on'=>'search'),
		);
	}

    public function scopes()
    {
        return array(
            'list'=>array(
                'select'=>'name,par,value',
            ),
            'access'=>array(
                'select'=>'name,value',
                'condition'=>'par="access"',
            ),
            'pars'=>array(
                'select'=>'par',
                'condition'=>'hidden=0'.(Yii::app()->hasModule('user') ? '' : ' AND par!="access"'),
                'distinct'=>true,
                'order'=>'par ASC',
            ),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'par' => 'Раздел',
			'name' => 'Переменная',
			'value' => 'Значение',
			'type' => 'Тип',
			'label' => 'Название',
			'description' => 'Описание',
		);
	}

	/**
	 *
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('par',$this->par);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('description',$this->description,true);
        if(!Yii::app()->hasModule('user'))
            $criteria->addCondition('par!="access"');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
    //            'params' => $params,
                'pageSize'=> Yii::app()->user->getState(
                	'pageSize',
                	Yii::app()->params['defaultPageSize']),
            ),
		));
	}

	/**
	 *
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
