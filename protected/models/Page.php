<?php

/**
 * This is the model class for table "{{page}}".
 * @property mixed user
 * @property mixed path
 * @property mixed id
 * @property string created
 * @property bool|string modified
 * @property mixed anons
 * @property mixed alias
 * @property mixed short_name
 * @property mixed title
 * @property mixed menu
 * @property mixed active
 * @property mixed parent
 * @property mixed show_child
 * @property mixed order
 * @property mixed content
 * @property mixed note
 * @property mixed description
 * @property mixed name
 * @property mixed keywords
 * @property mixed sitemap
 */
class Page extends BaseModel
{

    public $countChildren;

    public $catWithPath = array();

    public $forbiddenIds = array();

    public $childNodes;

    public $new_alias;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{page}}';
    }

    /**
     *
     */
    public function defaultScope()
    {
        return array_merge($this->ml->localizedCriteria(),array('with'=>'ico'));
    }


    /**
     *
     */
    public function relations()
    {
        return array(
            'parent' => array(self::BELONGS_TO, 'Page', 'parent'),
            'children' => array(self::HAS_MANY, 'Page', 'parent','select'=>'name,alias,path'),
            'file' => array(self::HAS_MANY, 'File', '','on'=>'file.m="page" AND file.m_id=t.id AND file.active=1','order'=>'file.order'),
            'ico' => array(self::HAS_ONE, 'File', '','on'=>'ico.m="pageIco" AND ico.m_id=t.id AND ico.active=1'), // ,'order'=>'ico.id DESC'
            'comment' => array(self::HAS_MANY, 'Comment', '', 'on'=>'comment.m="page" AND comment.m_id=t.id'),
        );
    }


    /**
     * @return array
     */
    public function scopes()
    {
        return array(
            // http://www.yiiframework.ru/forum/viewtopic.php?f=3&t=5219
            'countChildren'=>array(
                'select'=>'count(parentTable.parent) as countChildren',
                'join'=>'LEFT JOIN '. self::tableName() .' parentTable ON parentTable.parent = t.id',
                'group'=>'t.id',
            ),
            'active'=>array(
                'condition'=>'active=1',
            ),
            'paths'=>array(
                'select'=>'t.path',
                'distinct'=>true,
                'order'=>'t.path ASC',
            ),
        );
    }

    /**
     *
     * @return type
     */
    public function behaviors()
    {
        return array(
            'ml' => array(
                'class' => 'ext.multilangual.MultilingualBehavior',
                'localizedAttributes' => array(
                    'name',
                    'short_name',
                    'anons',
                    'content',
                    'title',
                    'description',
                    'keywords'
                ),
                'langClassName' => 'PageLang',
                'langTableName' => 'page_lang',
                'languages' => $this->getTranslatedLanguages(),
                'defaultLanguage' => Yii::app()->params['defaultLanguage'],
                'langForeignKey' => 'page_id',
                'dynamicLangClass' => true,
            ),
        );
    }


    /**
     * @param bool $parent
     * @param int $limit
     * @return $this
     */
    public function latest($parent=false,$limit=5)
    {
        $add = $parent ? ' AND parent="'.$parent.'"' : '';
        $this->resetScope();
        $this->getDbCriteria()->mergeWith(array(
            'select'=>'path,alias,short_name,anons,ico,created',
            'condition'=>'active=1'.$add,
            'order'=>'created DESC',
            'limit'=>$limit,
        ));
        return $this;
    }


    /**
     * @param bool $parent
     * @return $this
     */
    public function latestOne($parent=false)
    {
	    $add = $parent ? ' AND parent="'.$parent.'"' : '';
        $this->resetScope();
        $this->getDbCriteria()->mergeWith(array(
            'select'=>'path,alias,short_name,anons,ico',
            'condition'=>'active=1'.$add,
            'order'=>'created DESC',
            'limit'=>'1',
        ));
        return $this;
    }


    /*public function forMenu($parent=false)
    {
	    $add = $parent ? ' AND parent="'.$parent.'"' : '';
        $this->getDbCriteria()->mergeWith(array(
            'select'=>'path,alias,short_name',
            'condition'=>'active=1 AND page_menu=1'.$add,
        ));
        return $this;
    }*/


    /**
     * @param int $parent
     * @return $this
     */
    public function forDropDownList($parent = 0)
    {
        $this->getDbCriteria()->mergeWith(array(
            'select'=>'id,name,short_name',
            'condition'=>'active=1 AND parent="'.$parent.'"',
        ));
        return $this;
    }


    /**
     *
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('parent', 'parentValidate'),
            array('user, active, order, parent, show_child, menu, sitemap', 'numerical', 'integerOnly'=>true),
            array('alias', 'length', 'max'=>127),
            array('short_name', 'length', 'max'=>54),
            array('title, description, keywords, name, path', 'length', 'max'=>255),
            array('anons', 'length', 'max'=>510),
            array('countChildren, id, alias, short_name, title, description, keywords, ico, name, anons, content, note, user, created, modified, active, order, parent, path, sitemap', 'safe'),
        );
    }


    /**
     *
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'alias' => 'Алиас',
            'user' => 'Автор',
            'short_name' => 'Краткое имя',
            'title' => 'Заголовок, meta tag',
            'description' => 'Описание, meta tag',
            'keywords' => 'Ключевые слова, meta tag',
            'countChildren' => '',
            'name' => 'Название',
            'anons' => 'Анонс',
            'content' => 'Содержимое',
            'note' => 'Примечание',
            'page_user' => 'Автор',
            'created' => 'Создан',
            'modified' => 'Редактировался',
            'active' => 'Показ',
            'order' => 'Порядок',
            'parent' => 'Родитель',
            'path' => 'Путь',
            'show_child' => 'Показ детей',
            'menu' => 'Меню',
            'sitemap' => 'Карта сайта',
        );
    }


    /**
     *
     */
    protected function beforeSave()
    {
        if(parent::beforeSave()) {

            if(empty($this->short_name))
                $this->short_name = $this->name;

            $this->alias = Helper::transliteration($this->short_name,$this->alias);

            $this->user = Yii::app()->user->id;

            if($this->isNewRecord)
                $this->created = date('Y-m-d H:i', strtotime($this->created)).':00';
            else
                unset($this->created);

            $this->modified = date('Y-m-d H:i:s');

            if(isset($this->parent))
                $this->path = $this->getPath($this->parent);
            else $this->path = '';

            return true;

        } else return false;
    }


    /**
     *
     */
    protected function afterSave()
    {
        if(parent::afterSave()) {

            if($this->new_alias)
                $this->rewritePathes($this->id,$this->path.'/'.$this->alias);

            return true;

        } else return false;
    }


    /**
    *
    */
    protected function getPath($parent, $path = array())
    {
        $sql = "SELECT parent, alias FROM {{page}} WHERE id=".(int)$parent;
        $row = Yii :: app()->db->createCommand($sql)->queryRow();
        $path[] = $row['alias'];
        if ($row['parent'] > 0)
            $path = $this->getPath($row['parent'], $path);
        else
            $path = implode("/", array_reverse($path));
        return $path;
    }

    /**
     * $id - идент конкретной статьи, изменийшей свой алиас
     * $new_path - подготовленный path для вставки детям
     */
    public function rewritePathes($id,$new_path)
    {
        $new_path = ltrim($new_path,'/');
        // обновляем пути у непосредственных детей
        $sql = 'UPDATE {{page}} ch SET ch.path="'.$new_path.'" WHERE ch.parent IN ('.$id.')';
        Yii::app()->db->createCommand($sql)->execute();

        // достаём данные каждого ребёнка по очереди
        $sql = 'SELECT ch.alias,ch.path,ch.id FROM {{page}} ch WHERE ch.parent IN ('.$id.')';
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
        if(!empty($rows))
            foreach($rows as $row){
                $this->rewritePathes($row['id'],$row['path'].'/'.$row['alias']);
            }
    }


    /**
     *
     */
    protected function beforeDelete()
    {
        if(self::model()->findAll('parent='.$this->id))
            return false;
        return parent::beforeDelete();
    }


    /**
     * http://blog.ideashower.com/post/15147134343/create-a-parent-child-array-structure-in-one-pass
     * @param bool $id
     * @return array|mixed
     */
    public function getPageTree($id=false)
    {
        $tree = Yii::app()->cache->get('pageTree');
        if(empty($tree)){
            $refs = array();
            $tree = array();

            $nodes = self::model()->findAll(array(
                'condition'=>'t.active=1',
                'select'=>'id,name,parent,path,alias',
                'order'=>'t.order'
                ));

            foreach($nodes as $data) {
                $theref = &$refs[ $data['id'] ];
                $theref['parent'] = $data['parent'];
                $theref['name'] = $data['name'];
                $theref['src'] = ltrim($data['path'].'/'.$data['alias'],'/');
                if ($data['parent'] == 0) {
                    $tree[ $data['id'] ] = &$theref;
                } else {
                    $refs[ $data['parent'] ]['children'][ $data['id'] ] = &$theref;
                }
            }
            Yii::app()->cache->set('pageTree',$tree);
        }
        if($id){
            if( array_key_exists($id, $tree))
                return $tree[$id];
            foreach($tree as $branch){
                if(isset($branch['children']) &&  array_key_exists($id, $branch['children']))
                    return $branch['children'][$id];
            }
        }
        return $tree;
    }


    /**
     * @param $cats
     * @param $id
     * @return array
     */
    public function prepBranch($cats,$id)
	{
        $t[] = $cats[$id]['n'];
        if($cats[$id]['p']>0)
            $t = array_merge($t,$this->prepBranch($cats,$cats[$id]['p']));
        return $t;
	}


    /**
     * @param null $current
     * @return array
     */
    public function getCatList($current = null)
    {
    	if(!empty($this->catWithPath))
			return $this->catWithPath;

        $models = self::model()->findAll(array('select'=>'id, parent, name', 'order'=>'`t`.order'));
        $cats = array();
        foreach ($models as $m) {
            $cats[$m['id']] = array(
                'p' => $m['parent'],
                'n' => $m['name']
            );
        }

        unset($models);

        $return[0] = '--';
        foreach($cats as $id=>$item){
            $return[$id][] = $item['n'];
            if($item['p']>0){
                $return[$id] = array_reverse(array_merge($return[$id],$this->prepBranch($cats,$item['p'])));
            }
            $return[$id] = implode(' // ',$return[$id]);
        }

        /*
        // удаляет родителей из списка
        if($current)
            $return = $this->deleBranch($return,$cats,$current);*/
        asort($return);

        // 7 это id каталога "Полный список"
        // его в выпадающем меню выводить не нужно
        // unset($return[7]);
	$this->catWithPath = $return;

        return $return;
    }


	/**
	 * Запрещает перемещать node по своей ветке
     * и производит транслитерацию URL
	 */
    public function parentValidate()
    {
        $this->alias = Helper::transliteration($this->name,$this->alias);
        if(isset($this->id)){
            array_push($this->forbiddenIds,$this->id);
            $this->forbiddenChildrenIds((array)$this->id);
        }
        if (!empty($this->parent)) {
            $this->forbiddenParentsIds($this->parent);
        }
        if(in_array($this->parent,$this->forbiddenIds)){
            $this->addError('parent',Yii::t('page', 'You can not move the page to its same branch.'));
            return false;
        }
        return true;
    }


    /**
     *
     */
    public function forbiddenChildrenIds($ids)
    {
        $sql = "SELECT id FROM ".$this->tableSchema->name." WHERE parent IN (".implode(',',$ids).")";
        $children = Yii::app($sql)->db->createCommand($sql)->queryColumn();
        if($children){
            $this->forbiddenIds = array_merge($this->forbiddenIds,$children);
            $this->forbiddenChildrenIds($children);
        }
    }


    /**
     * @param $id
     */
    public function forbiddenParentsIds($id)
    {
        $sql = "SELECT id, parent FROM ".$this->tableSchema->name." WHERE id=".$id;
        $q = Yii::app()->db->createCommand($sql)->queryRow();
        if($q['parent']){
            array_push($this->forbiddenIds,$q['parent']);
            $this->forbiddenParentsIds($q['parent']);
        }
    }


    /**
     *
     */
    public function search()
    {

	$criteria=new CDbCriteria;

        $criteria->select = 't.*';
        $criteria->scopes = array('countChildren');
        if(isset($_GET['parent']) && !empty($_GET['parent']))
            $criteria->addCondition('t.parent='.(int)$_GET['parent']);
        else $criteria->addCondition('t.parent=0');


        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.alias',$this->alias,true);
        $criteria->compare('t.short_name',$this->short_name,true);
        $criteria->compare('t.title',$this->title,true);
        $criteria->compare('t.description',$this->description,true);
        $criteria->compare('t.keywords',$this->keywords,true);
        $criteria->compare('t.name',$this->name,true);
        $criteria->compare('t.anons',$this->anons,true);
        $criteria->compare('t.content',$this->content,true);
        $criteria->compare('t.note',$this->note,true);
        $criteria->compare('t.user',$this->user);
        $criteria->compare('t.created',$this->created,true);
        $criteria->compare('t.modified',$this->modified);
        $criteria->compare('t.menu',$this->menu);
        $criteria->compare('t.active',$this->active);
        $criteria->compare('t.order',$this->order);
        $criteria->compare('t.parent',$this->parent);
        $criteria->compare('t.path',$this->path);
        $criteria->compare('t.show_child',$this->show_child);
        $criteria->compare('t.sitemap',$this->sitemap);

        // http://bytespree.com/2013/notes-about-active-record-in-yii-framework.html
        return new CActiveDataProvider(get_class($this),array(
            'criteria' => $this->ml->modifySearchCriteria($criteria),
            'sort' => array(
                'defaultOrder' => 't.order ASC',
            ),
            'pagination' => array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }


    /**
     * @param string $className
     * @return static
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
