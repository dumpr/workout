<?php


class FileMore extends CActiveRecord
{
	/**
	 *
	 */
	public function tableName()
	{
		return '{{file_more}}';
	}

	/**
	 *
	 */
	public function rules()
	{
		return array(
			array('src_id, created', 'required'),
			array('created, height, width', 'numerical', 'integerOnly'=>true),
			array('id, src_id', 'length', 'max'=>11),
			array('lat, lon', 'length', 'max'=>12),
			array('make, model', 'length', 'max'=>255),
			array('id, src_id, lat, lon, make, model, created, height, width', 'safe', 'on'=>'search'),
		);
	}

	/**
	 *
	 */
	public function relations()
	{
		return array(
			'src0' => array(self::BELONGS_TO, 'FileBody', 'src_id'),
		);
	}

	/**
	 *
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'src' => 'Src',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'make' => 'Make',
			'model' => 'Model',
			'created' => 'Created',
			'height' => 'Height',
			'width' => 'Width',
		);
	}

	/**
	 *
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('src_id',$this->src_id);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('lon',$this->lon,true);
		$criteria->compare('make',$this->make,true);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('height',$this->height);
		$criteria->compare('width',$this->width);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 *
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
