<?php


/**
 * @property mixed src
 * @property int is_image
 * @property int size
 * @property  ext
 */
class FileBody extends CActiveRecord
{
	/**
	 *
	 */
	public function tableName()
	{
		return '{{file_body}}';
	}

	/**
	 *
	 */
	public function rules()
	{
		return array(
			array('src, size, ext, is_image', 'required'),
			array('size, is_image', 'numerical', 'integerOnly'=>true),
			array('src', 'length', 'max'=>40),
			array('ext', 'length', 'max'=>8),
			array('id, src, size, ext, is_image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 *
	 */
	public function relations()
	{
		return array(
			'files' => array(self::HAS_MANY, 'File', 'src'),
			'fileMores' => array(self::HAS_MANY, 'FileMore', 'id'),
		);
	}

	/**
	 *
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'src' => 'Src',
			'size' => 'Size',
			'ext' => 'Ext',
			'is_image' => 'Is Image',
		);
	}

	/**
	 *
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('src',$this->src,true);
		$criteria->compare('size',$this->size);
		$criteria->compare('ext',$this->ext,true);
		$criteria->compare('is_image',$this->is_image);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 *
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
