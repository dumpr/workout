<?php

/**
 * @property mixed m
 */
class File extends BaseModel
{
	/**
	 *
	 */
	public function tableName()
	{
            return '{{file}}';
	}

	/**
	 *
	 */
	public function rules()
	{
            return array(
                array('m, m_id, src_id, name, time, user_id', 'required'),
                array('order, view', 'numerical', 'integerOnly'=>true),
                array('m', 'length', 'max'=>30),
                array('m_id, src_id', 'length', 'max'=>11),
                array('name', 'length', 'max'=>127),
                array('description', 'length', 'max'=>255),
                array('time', 'length', 'max'=>15),
                array('view', 'length', 'max'=>10),
                array('id, m, m_id, src_id, name, description, order, time, view', 'safe', 'on'=>'search'),
            );
	}

	/**
	 *
	 */
	public function relations()
	{
            return array(
                'src0' => array(self::BELONGS_TO, 'FileBody', 'src_id'),
                'user0' => array(self::BELONGS_TO, 'User', 'user_id'),
            );
	}

    public function behaviors()
    {
        return array(
            'ml' => array(
                'class' => 'ext.multilangual.MultilingualBehavior',
                'localizedAttributes' => array('description'),
                'langClassName' => 'FileLang',
                'langTableName' => 'file_lang',
                'languages' => $this->getTranslatedLanguages(),
                'defaultLanguage' => Yii::app()->params['defaultLanguage'],
                'langForeignKey' => 'file_id',
                'dynamicLangClass' => true,
            ),
        );
    }

	/**
	 *
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'm' => 'M',
			'm_id' => 'M',
			'src_id' => 'Src',
			'name' => 'Name',
			'description' => 'Description',
			'order' => 'Order',
			'time' => 'Time',
			'view' => 'View',
		);
	}

	/**
	 *
	 */
	public function search()
	{
            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);
            $criteria->compare('m',$this->m,true);
            $criteria->compare('m_id',$this->m_id);
            $criteria->compare('src_id',$this->src_id);
            $criteria->compare('name',$this->name,true);
            $criteria->compare('description',$this->description,true);
            $criteria->compare('order',$this->order);
            $criteria->compare('user_id',$this->user_id);
            $criteria->compare('time',$this->time,true);
            $criteria->compare('view',$this->view);

            return new CActiveDataProvider($this, array(
                'criteria' => $this->ml->modifySearchCriteria($criteria),
            ));
	}

	/**
	 *
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
