<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class CommentForm extends CFormModel
{
	public $firstname;
	public $lastname;
	public $email;
	public $text;
	public $m;
	public $m_id;
	public $to;
	public $lang;
	public $user_id;
	public $created;
	public $verifyCode;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		$required = Yii::app()->user->isGuest ? array('text, m, m_id, firstname, lastname, email', 'required') : array('text, m, m_id, user_id', 'required');
		return array(
			array('firstname, lastname, text, to', 'filter', 'filter' => 'trim'),
			$required,
			array('email', 'email'),
			array('firstname, lastname', 'length', 'max'=>255),
			array('lang', 'length', 'max'=>6),
			array('verifyCode', 'captcha', 'allowEmpty'=>!Yii::app()->user->isGuest || !CCaptcha::checkRequirements()),
		);
	}

	/**
	 *
	 */
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>Yii::t('form','Verification Code'),
			'firstname'=>Yii::t('form','Firstname'),
			'lastname'=>Yii::t('form','Lastname'),
			'email'=>Yii::t('form','Email'),
			'text'=>Yii::t('form','Text'),
		);
	}

	/**
	 *
	 */
	public function beforeValidate()
	{
		if(parent::beforeValidate()) {
			$data = explode("_", $this->m);
			$this->m = (string)$data[0];
			$this->m_id = (int)$data[1];
			$this->lang = isset($data[2]) ? (string)$data[2] : Yii::app()->language;
			$this->created = time();
			if(Yii::app()->user->id)
				$this->user_id = Yii::app()->user->id;

			return true;
		}
	}
}