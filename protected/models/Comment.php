<?php

/**
 * This is the model class for table "{{comment}}".
 *
 * The followings are the available columns in table '{{comment}}':
 * @property string $id
 * @property string $user_id
 * @property integer $reply_to
 * @property string $m
 * @property integer $m_id
 * @property string $text
 * @property integer $created
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property User $user
 */
class Comment extends CActiveRecord
{
    public $date_first;
    public $date_last;
    public $obj_name;
    public $statuses = array(
        '-1' => 'Удалён',
        '0'  => 'Новый',
        '1'  => 'Активный'
    );
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{comment}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('user_id, m, m_id, text, created', 'required'),
            array('to, m_id, created, user_id, active', 'numerical', 'integerOnly'=>true),
            array('lang', 'length', 'max'=>6),
            array('m', 'length', 'max'=>30),
            array('text', 'length', 'max'=>1023),
            array('id, user_id, to, m, m_id, text, created, lang, active, date_first, date_last, obj_name', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'reply' => array(self::HAS_ONE, 'Comment', '', 'on'=>'reply.id = comment.to'),
        );
    }


    /**
     * @return array
     */
    public function scopes()
    {
        return array(
            'active'=>array(
                'condition'=>'active=1',
            ),
        );
    }

    public function defaultScope()
    {
        return array(
            'order' => $this->getTableAlias(FALSE, FALSE).'.created ASC', // ASC DESC
        );

    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => Yii::t('form','User'),
            'to' => 'Reply To',
            'm' => 'M',
            'm_id' => 'M',
            'text' => Yii::t('form','Text'),
            'created' => Yii::t('form','Created'),
            'lang' => 'Language',
            'active' => Yii::t('form','Active'),
        );
    }

    /**
     *
     */
    protected function beforeSave()
    {
        if(parent::beforeSave()) {
            $this->created = time();
            return true;

        } else return false;
    }

    /**
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
//        qw($this, 'e');
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.user_id',$this->user_id,true);
        //$criteria->compare('t.to',$this->to);
        //$criteria->compare('m',$this->objName(),true);
        //$criteria->join('{{t.m}} target on target.id = t.m_id');
        $criteria->compare('t.m_id',$this->m_id);
        $criteria->compare('t.text',$this->text,true);
        $criteria->compare('t.lang',$this->lang);
        $criteria->compare('t.active',$this->active);

        if(!empty($this->date_first) && !empty($this->date_last)) {
            if($this->date_first > $this->date_last)
                $this->date_first = $this->date_last;
            $last = strtotime($this->date_last)+24*60*60;
            $criteria->addBetweenCondition('t.created', strtotime($this->date_first), $last);
        } elseif(!empty($this->date_first) && empty($this->date_last))
            $criteria->addCondition('t.created > '.strtotime($this->date_first));
        elseif(empty($this->date_first) && !empty($this->date_last)){
            $last = strtotime($this->date_last)+24*60*60;
            $criteria->addCondition('t.created < '.$last);
        }

        return new CActiveDataProvider(get_class($this),array(
            'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.id DESC',
            ),
            'pagination' => array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function objName()
    {
        $name = Yii::app()->cache->get('name_'.$this->m.'_'.$this->m_id);
        if(empty($name)){
            $sql = "SELECT name FROM {{".$this->m."}} WHERE id=".$this->m_id;
            $name = Yii::app()->db->createCommand($sql)->queryScalar();
            Yii::app()->cache->set('name_'.$this->m.'_'.$this->m_id, $name);
        }
        return $name;

//        $mName = ucfirst($this->m);
//        $model = $mName::model()->findByPk($this->m_id);
//        return $model->name;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Comment the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
