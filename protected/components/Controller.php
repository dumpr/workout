<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    //public $layout='//layouts/column1';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $mainmenu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    /**
     *
     * @var суффикс текущего языка
     */
    public $l;

    /**
     * @throws CDbException
     */
    public function init() {
        parent::init();
        if (Yii::app()->language !== Yii::app()->params['defaultLanguage'])
            $this->l = '_' . Yii::app()->language;
        if (isset(Yii::app()->user->id)) //  && '127.0.0.1' != $_SERVER['REMOTE_ADDR']
            Yii::app()->db->createCommand('UPDATE {{user}} SET last_visit_on = now() WHERE id=' . Yii::app()->user->id)->execute();

        $c = array(
            'select' => 't.short_name as name,t.path,t.alias', // CONCAT_WS("/",t.path,t.alias) as alias
            'condition' => 't.menu=1 AND t.active=1 AND t.parent=0',
            'order' => 't.order ASC',
        );
        foreach (Page::model()->multilang()->findAll($c) as $attr) {
            //$url = trim($attr['path'],'/') . '/' . trim($attr['alias'],'/');
            $url = $this->createUrl(implode("/", array($attr['path'], $attr['alias'])));
            $this->mainmenu[] = array('label' => $attr['name' . Yii::app()->controller->l], 'url' => $url);
        }
    }

    /**
     * http://stackoverflow.com/a/15618288
     * @param CAction $action
     * @return bool
     */
    /* protected function beforeAction($action)
      {
      if(defined('YII_DEBUG') && YII_DEBUG){
      Yii::app()->assetManager->forceCopy = true;
      }
      return parent::beforeAction($action);
      } */

    /**
     * http://stackoverflow.com/a/13031383/524459
     * Return data to browser as JSON and end application.
     * @param array $data
     */
    protected function renderJSON($data)
    {
        $this->layout = false;
        header('Content-type: application/json');
        echo CJSON::encode($data);

        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute) {
                $route->enabled = false; // disable any weblogroutes
            }
        }
        Yii::app()->end();
    }

}
