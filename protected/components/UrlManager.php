<?php

class UrlManager extends CUrlManager
{
	public function init()
	{
		$this->urlFormat = 'path';
		$this->showScriptName = false;
		$this->caseSensitive = true;
		$this->useStrictParsing = true;

		$rules = array(
				'' => 'site/index',

				'getfile/<src:.*>'=>'site/getfile',
				'contact'=>'site/contact',
				'<action:(login|logout|register|password|recovery|profile|reactivate|verify)>' => 'usr/default/<action>',
				'recovery/activationKey/<activationKey:\w+>/username/<username:\w+>'=>'usr/default/recovery',
				'verify/activationKey/<activationKey:\w+>/username/<username:\w+>'=>'usr/default/verify',

				/*'<_c:(gii|admin|site|usr)>'=>'<_c>', //gii|
				'<_c:(gii|admin|site|usr)>/<_a:.*>'=>'<_c>/<_a>',*/

				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',

				'<module:(gii|admin|rbac)>/<controller:\w+>/<id:\d+>/<action:\w+>'=>'<module>/<controller>/<action>',
				'<module:(gii|admin|rbac)>/<controller:\w+>/<id:\d+>'=>'<module>/<controller>/view',
				'<module:(gii|admin|rbac)>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
				'<module:(gii|admin|rbac)>/<controller:\w+>'=>'<module>/<controller>',
				'<module:(gii|admin|rbac)>'=>'<module>',
				'<route:.*>'=>'site/view',


		);

		$this->addRules($rules);

		return parent::init();
	}
    public function createUrl($route, $params=array(), $ampersand='&')
    {
        $url = parent::createUrl($route, $params, $ampersand);
        return Multilang::addLangToUrl($url);
    }
}

?>