<?php
class CImage extends CApplicationComponent
{
	public $toolkit;

	public $presets=array();

	public $exif;

	public function init()
	{
		parent::init();

		Yii::import('ext.imageapi.ImageToolkit');
		Yii::import('ext.imageapi.GDImageToolkit');
		$this->toolkit = new GDImageToolkit;
	}

        /**
         *
         * @param type $file
         * @return type
         */
	public function getInfo($file)
	{
		return $this->toolkit->getInfo($file);
	}

        /**
         *
         * @param type $presetName
         * @param type $file
         * @param type $ext
         * @return string|boolean
         */
	public function createPath($presetName, $file, $ext='')
	{
		$file_original = str_replace(array('\\', '/'), DIRECTORY_SEPARATOR, $file);

		if (!file_exists($file_original)) return false;

		if (isset($this->presets[$presetName])) {
			$f = explode('protected'.DIRECTORY_SEPARATOR.'data',$file_original);
			$targetPath = str_replace(array('\\', '/'), DIRECTORY_SEPARATOR, Yii::getPathOfAlias($this->presets[$presetName]['cacheIn']));
			if(isset($f[1]))
				$targetPath .= substr($f[1], 0, 6);
			$basename = basename($file_original);
			$targetFile = $targetPath . DIRECTORY_SEPARATOR . $basename;
			if($ext)
				$targetFile .= '.'.$ext;
			if (!file_exists($targetPath))
				mkdir($targetPath, 0777, true); // mkdir recursive
			if (!file_exists($targetFile)) {
				copy($file_original, $targetFile);

				$this->exif_orient($targetFile);
				foreach($this->presets[$presetName]['actions'] as $action=>$params) {
					switch($action) {
					case 'scaleAndCrop':
						$this->scaleAndCrop(
								$targetFile,
								$targetFile,
								$params['width'],
								$params['height']);
						break;
					case 'scale':
						$this->scale(
								$targetFile,
								$targetFile,
								$params['width'],
								$params['height']);
						break;
					case 'resize':
						$this->resize(
								$targetFile,
								$targetFile,
								$params['width'],
								$params['height']);
						break;
					case 'rotate':
						$this->rotate(
								$targetFile,
								$targetFile,
								$params['degrees'],
								$params['background']);
						break;
					case 'crop':
						$this->crop(
								$targetFile,
								$targetFile,
								$params['x'],
								$params['y'],
								$params['width'],
								$params['height']);
						break;
					}
				}
			}
			return $targetFile;
		} else
			return false;
	}

	/**
	 * @param $presetName
	 * @param $file
	 * @return bool|mixed
	 */
	public function createUrl($presetName, $file)
	{
		$ext = '';
                if(is_array($file)) {
			$ext = $file['ext'];
                        $file = $file['src']; // Yii::getPathOfAlias('webroot.protected.data') . '/' .
		} elseif(is_numeric($file)) {
			$model = File::model()->with('src0')->findByPk($file);
			list($file,$ext) = $this->fileFromModel($model);
		} elseif(is_object($file)) {
			list($file,$ext) = $this->fileFromModel($file);
		} else {
                    $arr = explode('.', $file);
                    if (isset($arr[1]))
                        $ext = end($arr);
                }

                $generatedPath = str_replace('\\', '/', $this->createPath($presetName, $file, $ext));
                /*$webRootPath = Yii::getPathOfAlias('webroot');
                if(strpos($generatedPath, $webRootPath) !== FALSE)
                        $generatedPath = substr($generatedPath, strlen($webRootPath));*/

                $home = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']);
                return str_replace($home, '', $generatedPath);

	}

	/**
	 * @param string $preset
	 * @return array info about file: id, name, src, ext or html block
	 * @throws CException
	 */
	public function getResult($preset='')
	{
            if(empty($preset) && isset($_GET['preset']) && !empty($_GET['preset']))
                    $preset = $_GET['preset'];

            $data['id'] = Yii::app()->filestore->id;
            $data['name'] = Yii::app()->filestore->filename;
            $data['basename'] = Yii::app()->filestore->basename;
            $data['is_image'] = Yii::app()->filestore->isImage;
            $data['ext'] = Yii::app()->filestore->extension;
            $data['description'] = '';

            if($data['is_image'])
                $data['src'] = Yii::app()->image->createUrl($preset, array(
                    'src' => Yii::app()->filestore->newPath,
                    'ext' => Yii::app()->filestore->extension,
                ));
            else
                $data['src'] = Yii::app()->filestore->downloadLink(Yii::app()->filestore->newPath);

            if(isset($preset) && isset(Yii::app()->image->presets[$preset]['template'])){
                $data['html'] = Yii::app()->controller->renderPartial(Yii::app()->image->presets[$preset]['template'],array('data'=>$data),true);
            }
            return $data;
	}

	/**
	 * @param $model
	 * @return array
     */
	public function fileFromModel($model)
	{
		return array(Yii::getPathOfAlias('application.data').'/'.$model->src0['src'],'.'.$model->src0['ext']);
	}

	/**
	 * @param $presetName
	 * @param $file
	 * @return string
     */
	public function createAbsoluteUrl($presetName, $file)
	{
		return Yii::app()->getRequest()->getHostInfo().$this->createUrl($presetName, $file);
	}

	/**
	 * @param $source
	 * @param $destination
	 * @param $width
	 * @param $height
	 * @return bool
     */
	public function scaleAndCrop($source, $destination, $width, $height)
	{
		$info = self::getInfo($source);

		$scale = max($width / $info['width'], $height / $info['height']);
		$x = round(($info['width'] * $scale - $width) / 2);
		$y = round(($info['height'] * $scale - $height) / 2);

		if ($this->toolkit->resize($source, $destination, $info['width'] * $scale, $info['height'] * $scale)) {
			return $this->toolkit->crop($destination, $destination, $x, $y, $width, $height);
		}
		return false;
	}

	/**
	 * @param $source
	 * @param $destination
	 * @param $width
	 * @param $height
	 * @return bool
     */
	public function scale($source, $destination, $width, $height)
	{
		$info = self::getInfo($source);

		// Don't scale up.
		if ($width >= $info['width'] && $height >= $info['height']) {
			return false;
		}

		$aspect = $info['height'] / $info['width'];
		if ($aspect < $height / $width) {
			$width = (int)min($width, $info['width']);
			$height = (int)round($width * $aspect);
		} else {
			$height = (int)min($height, $info['height']);
			$width = (int)round($height / $aspect);
		}

		return $this->toolkit->resize($source, $destination, $width, $height);
	}

	/**
	 * @param $source
	 * @param $destination
	 * @param $width
	 * @param $height
	 * @return mixed
     */
	public function resize($source, $destination, $width, $height)
	{
		return $this->toolkit->resize($source, $destination, $width, $height);
	}

	/**
	 * @param $source
	 * @param $destination
	 * @param $degrees
	 * @param int $background
	 * @return mixed
     */
	public function rotate($source, $destination, $degrees, $background = 0x000000)
	{
		return $this->toolkit->rotate($source, $destination, $degrees, $background);
	}

	/**
	 * @param $source
	 * @param $destination
	 * @param $x
	 * @param $y
	 * @param $width
	 * @param $height
	 * @return mixed
     */
	public function crop($source, $destination, $x, $y, $width, $height)
	{
		return $this->toolkit->crop($source, $destination, $x, $y, $width, $height);
	}

	/**
	 * @param $file
	 * @return array
     */
	public function getExifInfo($file)
	{
		$info = self::getInfo($file);
		if($info['extension'] == "tiff" || $info['extension'] == "jpg" || $info['extension'] == "jpeg")
		{
			$this->exif = @exif_read_data($file, 0, true);
			return $this->exif;
		}
	}

	/**
	 * @param $file
	 * @return bool
     */
	public function exif_orient($file)
	{
		$this->getExifInfo($file);
		if(empty($this->exif) || !isset($this->exif['IFD0']['Orientation']))
			return false;

		$ort = $this->exif['IFD0']['Orientation'];
		switch($ort)
	    {
			case 2: // horizontal flip
				//$this->image->flip(EasyImage::FLIP_HORIZONTAL);
				break;
			case 3: // 180 rotate left
				//$this->image->rotate('180');
				$this->rotate($file,$file,'180');
				break;
			case 4: // vertical flip
				//$this->image->flip(EasyImage::FLIP_VERTICAL);
				break;
			case 5: // vertical flip + 90 rotate right
				//$this->image->flip(EasyImage::FLIP_VERTICAL);
				//$this->image->rotate('90');
				break;
			case 6: // 90 rotate right
				//$this->image->rotate('90');
				$this->rotate($file,$file,'-90');
				break;
			case 7: // horizontal flip + 90 rotate right
				//$this->image->flip(EasyImage::FLIP_HORIZONTAL);
				//$this->image->rotate('90');
				break;
			case 8: // 90 rotate left
				//$this->image->rotate('-90');
				$this->rotate($file,$file,'90');
				break;
		}
	}
}