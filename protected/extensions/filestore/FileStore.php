<?php

class FileStore extends CApplicationComponent
{

    public $downloadPath='getfile/';

    public $dummy_ico;
    public $allowedExts;
    public $allowedImgExts = array('jpeg', 'jpg', 'png', 'gif');
    public $allowedDocExts = array('doc', 'docx', 'xls', 'xlsx', 'pdf', 'txt', 'zip', 'rar', '7z', 'djvu', 'aac');
    public $newPath;
    public $id;
    public $body_id;
    public $basename;
    public $extension;
    public $filename;
    public $isImage;
    public $exif;

    public $m;
    public $m_id;

    private $data;
    private $hash;

    /**
     * Разрешена ли загрузка файлов с данным расширением
     * @return boolean
     */
    public function isAllowedByExt()
    {
        return in_array(strtolower($this->extension), array_merge($this->allowedImgExts,$this->allowedDocExts));
    }

    /**
     * Возвращает ссылку по которой файл будет доступен для скачивания
     * @param  string $src данные поля src из таблицы file
     * @return string
     */
    public function downloadLink($src)
    {
        return trim($this->downloadPath,'/').'/'.(strlen($src)==34 ? $src : substr($src,-34));
    }

    /**
     * Загружает файл принятый по php://input
     * Сохраняет его по уникальному пути
     * Записывает данные о нём в таблицу file
     * @param  boolean $m    имя модели
     * @param  boolean $m_id id модели
     * @return string        id модели file
     */
    public function store($m = false,$m_id = false, $active = false)
    {
		if (!empty($m))
			$this->m = $m;
		if (!empty($m_id))
			$this->m_id = $m_id;

            $filepath = urldecode($_SERVER['HTTP_X_FILE_NAME']);
            $this->utfPathinfo($filepath);
            if (!$this->isAllowedByExt())
                    return false;

            $this->data = file_get_contents("php://input");
            $this->hash = md5($this->data);
            $this->isImage = (in_array($this->extension, $this->allowedImgExts) && substr($_SERVER['HTTP_X_FILE_TYPE'], 0, 6) == "image/") ? 1 : 0;

            sscanf($this->hash, "%2s%2s%s", $one, $two, $three);
            $body = FileBody::model()->find('src="' . $one . '/' . $two . '/' . $three . '"');
            $newPath = Yii::getPathOfAlias('application.data') . '/' . $one . '/' . $two;

            if (empty($body)) {
                    @mkdir($newPath, 0777, true);
                    if (!file_put_contents($newPath . '/' . $three, $this->data))
                            return false;

                    $body = new FileBody;
                    $body->src = $one . '/' . $two . '/' . $three;
                    $body->ext = $this->extension;
                    $body->size = filesize($newPath . '/' . $three);
                    $body->is_image = $this->isImage;
                    if (!$body->save()) {
                            qw($body->errors, 'l');
                            return false;
                    }
                    $this->body_id = $body->id;
                    if($this->exif = @exif_read_data($newPath.'/'.$three,0,true))
                            $this->collectExifInfo();
            }
            $this->newPath = $newPath.'/'.$three;

            $file = new File;
            $file->name = $this->filename;
            $file->time = time();
            $file->m = $this->m;
            $file->m_id = $this->m_id;
            $file->user_id = Yii::app()->user->id;
            $file->src_id = $body->id;
            $file->active = (int)$active;

            if (!$file->save())
                    qw($file->errors, 'l');

            $this->id = $file->id;
            return $this->id;
    }

    /**
     *
     * @param type $id
     */
    public function activeFile($id)
    {
        $sql = "UPDATE {{file}} SET active=1 WHERE id=:id";
        Yii::app()->db->createCommand($sql)->execute(array(':id'=>$id));
    }

    /**
     *
     * @param type $m
     * @param type $m_id
     */
    public function activeFiles($m, $m_id)
    {
        $sql = "UPDATE {{file}} SET active=1 WHERE m=:m AND m_id=:m_id";
        Yii::app()->db->createCommand($sql)->execute(array(':m'=>$m,':m_id'=>$m_id));
    }

    /**
     *
     * @param type $id
     */
    public function hideFile($id)
    {
        $sql = "UPDATE {{file}} SET active=0 WHERE id=:id";
        Yii::app()->db->createCommand($sql)->execute(array(':id'=>$id));
    }

    /**
     *
     * @param type $m
     * @param type $m_id
     */
    public function hideFiles($m, $m_id)
    {
        $sql = "UPDATE {{file}} SET active=0 WHERE m=:m AND m_id=:m_id";
        Yii::app()->db->createCommand($sql)->execute(array(':m'=>$m,':m_id'=>$m_id));
    }

    /**
     *
     * @param $id int - идент таблицы file
     */
    public function deleteFile($id)
    {
        $sql = 'SELECT t.id, t.src_id, '
                . '(SELECT count(c.src_id) FROM {{file}} c WHERE c.src_id=t.src_id) as unic '
                . 'FROM {{file}} t WHERE t.id="'.$id.'"';
        $ids = Yii::app()->db->createCommand($sql)->queryAll();
        $this->doDelete($ids);
    }

    /**
     * Функция находит все файлы, прикреплённые к данной модели
     * @param $m string имя модели
     * @param $m_id int идент модели
     */
    public function deleteFiles($m,$m_id)
    {
        $sql = 'SELECT t.id, t.src_id, '
                . '(SELECT count(c.src_id) FROM {{file}} c WHERE c.src_id=t.src_id) as unic '
                . 'FROM {{file}} t WHERE t.m="'.$m.'" AND t.m_id="'.$m_id.'" GROUP BY t.src_id';
        $ids = Yii::app()->db->createCommand($sql)->queryAll();
        $this->doDelete($ids);
    }

    /**
     * @param $ids array
     */
    private function doDelete($ids)
    {
        foreach($ids as $item){
            // файл используется только в одном месте - удаляем всё о нём
            if($item['unic'] == 1){
                    $this->deleteBody($item['src_id']);
            // файл где то используется ещё, тогда удаляем только привязку к нему нашей модели
            } else File::model()->deleteByPk($item['id']);
        }
    }

    /**
     * Удаляет файл с диска и всю инфу о нём из базы
     * @param $id идент таблицы body
     * @throws CDbException
     */
    private function deleteBody($id)
    {
        $model = FileBody::model()->findByPk($id);
        @unlink(Yii::getPathOfAlias('application.data') . '/' . $model->src);
        $model->delete();
    }

	/**
	 * @return bool
         */
    public function collectExifInfo()
    {
        if(empty($this->body_id))
                return false;

        if(isset($this->exif['GPS']['GPSLatitude']))
                list($info['lat'],$info['lon']) = $this->getGpsData();
        if(isset($this->exif['IFD0']['Make']))
                $info['make'] = trim($this->exif['IFD0']['Make']);
        if(isset($this->exif['IFD0']['Make']))
                $info['model'] = trim($this->exif['IFD0']['Model']);
        if(isset($this->exif['IFD0']['DateTime']))
                $info['created'] = strtotime($this->exif['IFD0']['DateTime']);
        if(isset($this->exif['COMPUTED']['Height']))
                $info['height'] = $this->exif['COMPUTED']['Height'];
        if(isset($this->exif['COMPUTED']['Width']))
                $info['width'] = $this->exif['COMPUTED']['Width'];
        if(!isset($info))
                return false;

        $more = new FileMore;
        $more->attributes = $info;
        $more->src_id = $this->body_id;
        if(!$more->save())
                qw($more->errors,'l');
    }


    /**
     * @return array
     */
    public function getGpsData()
    {
            $latitude = $this->gps($this->exif['GPS']['GPSLatitude'], $this->exif['GPS']['GPSLatitudeRef']);
            $longitude = $this->gps($this->exif['GPS']['GPSLongitude'], $this->exif['GPS']['GPSLongitudeRef']);

            return array($latitude,$longitude);
    }


    /**
     * @param $coordinate
     * @param $hemisphere
     * @return int
     */
    public function gps($coordinate, $hemisphere) {
            for ($i = 0; $i < 3; $i++) {
                    $part = explode('/', $coordinate[$i]);
                    if (count($part) == 1) {
                            $coordinate[$i] = $part[0];
                    } else if (count($part) == 2) {
                            $coordinate[$i] = floatval($part[0])/floatval($part[1]);
                    } else {
                            $coordinate[$i] = 0;
                    }
            }
            list($degrees, $minutes, $seconds) = $coordinate;
            $sign = ($hemisphere == 'W' || $hemisphere == 'S') ? -1 : 1;
            return $sign * ($degrees + $minutes/60 + $seconds/3600);
    }

    /*
     * http://php.net/manual/en/function.pathinfo.php#107461
     * Use this function in place of pathinfo to make it work with UTF-8 encoded file names too
     */
    public function utfPathinfo($filepath)
    {
            preg_match('%^(.*?)[\\\\/]*(([^/\\\\]*?)(\.([^\.\\\\/]+?)|))[\\\\/\.]*$%im',$filepath,$m);
            // if($m[1]) $ret['dirname']=$m[1];
            if($m[2]) $this->basename = $m[2]; // имя файла с раширением
            if($m[5]) $this->extension = strtolower($m[5]); // расширение
            if($m[3]) $this->filename = $m[3]; // имя файла
    }

}