<?php
return array(
			'connectionString' => 'mysql:host=localhost;dbname=workout',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			'tablePrefix' => 'wo_',
		);