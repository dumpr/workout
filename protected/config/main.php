<?php return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'theme'=>'bootstrap', // classic bootstrap
    'name'=>'My Web Application',
    'preload'=>array('log'),
    'sourceLanguage'=>'en',
    'language'=>'ru', // en ru uk
    'timeZone' => 'Europe/Kiev', // Europe/London Europe/Moscow Europe/Kiev
    'import'=>array(
        'application.models.*',
        'application.helpers.*',
        'application.widgets.*',
        'application.components.*',
        'application.extensions.*',
        'application.modules.usr.*',
        'application.modules.usr.models.*',
    ),
    'modules'=>array(
        'admin',
        'rbac'=>array(
            'class'=>'application.modules.admin.modules.rbacui.RbacuiModule',
            'userClass' => 'User',
            'userIdColumn' => 'id',
            'userNameColumn' => 'username',
            'rbacUiAdmin' => true,
            'rbacUiAssign' => true,
        ),
        'usr' => array(
           'userIdentityClass' => 'UserIdentity',
        ),
        'gii' => array(
            'class'=>'system.gii.GiiModule',
            'password'=>'password',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1'),
        ),
    ),

    'components'=>array(
        'user'=>array(
            'allowAutoLogin'=>true,
            'class'=>'WebUser',
        ),
        'request'=>array(
            'class'=>'HttpRequest',
        ),
        'urlManager'=>array(
                'class'=>'UrlManager',
        ),
        'authManager'=>array(
                'class'=>'CDbAuthManager',
                'connectionID'=>'db',
        ),
        'format'=>array(
                'class'=>'application.extensions.timeago.TimeagoFormatter',
        ),
        'db'=>require($_SERVER['REMOTE_ADDR']=='127.0.0.1' ? 'db_loc.php' : 'db.php'),
        'errorHandler'=>array(
                // use 'site/error' action to display errors
                // 'errorAction'=>'site/error',
        ),
        'clientScript' => array(
            'scriptMap' => array(
                'jquery.js' => false,
                'jquery.min.js' => false,
                'jquery.yiitab.js' => false,
                'jquery.yiigridview.js' => false,
                //'jquery.ba-bbq.js' => false,
                //'jquery-ui.min.js' => false,
                //'ckeditor.js' => false,
                'fileuploader.js' => false,
                'fileuploader.css' => false,
            ),
            'coreScriptPosition'=>CClientScript::POS_END, // POS_HEAD POS_READY POS_END
            'defaultScriptPosition'=>CClientScript::POS_END,
            'defaultScriptFilePosition'=>CClientScript::POS_END
        ),
        'cache'=>array(
            'class'=>'system.caching.CFileCache', // CDummyCache
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),

        'image'=>  require ('image.php'),

        'filestore' => array(
                'class'=>'ext.filestore.FileStore',
                'dummy_ico' => '/images/dummy_file.png',
                'downloadPath'=>'getfile/',
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
            // this is used in contact page
            'adminEmail'=>'webmaster@example.com',
            // http://www.elisdn.ru/blog/39/yii-based-multilanguage-site-interface-and-urls
        'translatedLanguages'=>array(
            'ru'=>'Русский',
            'uk'=>'Українська',
            //'en'=>'English',
        ),
        'defaultLanguage'=>'ru',
        'defaultPageSize'=>'10'
    ),
);