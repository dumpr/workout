<?php

class LanguageSwitcherWidget extends CWidget
{
	public function run()
	{
		$currentUrl = ltrim(Yii::app()->request->url, '/');
		$links = array();
		foreach (Multilang::suffixList() as $suffix => $name){
			$suffix = trim($suffix, '_');
			$url = '/' . ($suffix ? $suffix . '/' : '') . $currentUrl;
			if(empty($suffix))
				$suffix = Yii::app()->params['defaultLanguage'];
			if($suffix == Yii::app()->language)
				$links[] = '<a class="lang '.$suffix.'" title="'.$name.'"></a>';
			else
				$links[] = CHtml::link('', $url, array('class'=>'lang ' . $suffix,'title'=>$name));
		}
		echo CHtml::tag('div', array('id'=>'langs'), implode("\n", $links));
	}
}

?>