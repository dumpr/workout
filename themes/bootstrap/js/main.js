$('.comments').on('click','.addcomm',function(e){
	e.preventDefault();
	$('.addcomm').show();
	$(this).hide();
	$('#CommentForm_m').val($(this).closest('.article').data('articleInfo'));
	$('#CommentForm_text').val('');
	$('#CommentForm_to').val($(this).closest('.comment_wrap').data('commentId'));
	$('#comment-form').insertAfter(this).show();
});
$('body').on('submit','#comment-form',function(e){
	e.preventDefault();
	var comments = $(this).closest('.comments');
	$(comments).css('opacity',0.4);
    $.ajax({
        type: "POST",
        url: "site/addcomment",
        data: $('#comment-form').serialize(),
        success: function(data){$(comments).html(data).css('opacity',1)},
        error: function(data) {resMessage('Произошла ошибка. '+data.responseText)},
        dataType:'html'
    });
});
