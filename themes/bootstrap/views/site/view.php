<div class="article" data-article-info="page_<?= $model['id'].$this->l ?>">

	<h1><?= $model['name'.$this->l] ?></h1>
	<div class="anons">
		<p><?= $model['anons'.$this->l] ?></p>
	</div>

    <div class="content"><?= $model['content'.$this->l] ?></div>

    <div class="comments">
    <?php $this->renderPartial('comment/all_comments',array(
        'comments'=>$model->comment,'commform'=>new CommentForm)); ?>
    </div>

</div>

<aside>
	<?php
	/*$menu = $model->menu($model['id']);
	if($menu){ ?>
		<div class="information">
			<ul>
		<?php foreach($menu as $name=>$url){ ?>
				<li><a href="<?= $url ?>"><?= $name ?></a></li>
		<?php } ?>
			</ul>
		</div>
	<?php } */?>
</aside>