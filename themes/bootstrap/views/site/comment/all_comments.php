<a href="#" class="addcomm"><?= Yii::t('form','Leave a comment') ?></a>
<?php foreach ($comments as $comment){
		$this->renderPartial('comment/comment',array('comment'=>$comment));
	} ?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'comment-form',
    'enableClientValidation'=>true,
    'action'=>'site/addcomment',
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

    <?php // echo $form->errorSummary($commform); ?>

    <?php if(Yii::app()->user->isGuest){ ?>
    <div class="form-group">
        <?php echo $form->labelEx($commform,'firstname'); ?>
        <?php echo $form->textField($commform,'firstname'); ?>
        <?php echo $form->error($commform,'firstname'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($commform,'lastname'); ?>
        <?php echo $form->textField($commform,'lastname'); ?>
        <?php echo $form->error($commform,'lastname'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($commform,'email'); ?>
        <?php echo $form->textField($commform,'email'); ?>
        <?php echo $form->error($commform,'email'); ?>
    </div>
    <?php } ?>

    <div class="form-group">
        <?php echo $form->labelEx($commform,'text'); ?>
        <?php echo $form->textArea($commform,'text',array('rows'=>6)); ?>
        <?php echo $form->error($commform,'text'); ?>
    </div>

    <?php if(Yii::app()->user->isGuest && CCaptcha::checkRequirements()): ?>
    <div class="form-group">
        <?php echo $form->labelEx($commform,'verifyCode'); ?>
        <div>
        <?php $this->widget('CCaptcha'); ?>
        <?php echo $form->textField($commform,'verifyCode'); ?>
        </div>
        <div class="hint"><?= Yii::t('form','Please enter the letters as they are shown in the image above.<br/>Letters are not case-sensitive.') ?></div>
        <?php echo $form->error($commform,'verifyCode'); ?>
    </div>
    <?php endif; ?>

    <input name="CommentForm[to]" id="CommentForm_to" type="hidden" value="" />
    <input name="CommentForm[m]" id="CommentForm_m" type="hidden" value="" />

    <div class="form-group buttons">
        <?php echo CHtml::submitButton(Yii::t('form','Submit'),array('class'=>'btn btn-primary')); ?>
    </div>

<?php $this->endWidget(); ?>



<?php /* ?><form id="comment-form" action="site/addcomment" method="post">
<?php if(Yii::app()->user->id){ ?>
    <label for="Comment_firstname"><?= Yii::t('form','Firstname') ?>:</label>
    <input name="Comment[firstname]" id="Comment_firstname" type="text" />

    <label for="Comment_lastname"><?= Yii::t('form','Lastname') ?>:</label>
    <input name="Comment[lastname]" id="Comment_lastname" type="text" />

    <label for="Comment_contact"><?= Yii::t('form','Phone or Email address') ?>:</label>
    <input name="Comment[contact]" id="Comment_contact" type="text" />
<?php } ?>
    <label for="Comment_text" class="required"><?= Yii::t('form','Text') ?>: <span class="required">*</span></label>
	<textarea name="Comment[text]" id="Comment_text"></textarea>

    <input name="Comment[to]" id="Comment_to" type="hidden" value="" />
	<input name="Comment[m]" id="Comment_m" type="hidden" value="" />
	<button class="btn btn-primary"><?= Yii::t('form','Submit') ?></button>
</form><?php */ ?>