<?php $user = UsrModule::user($comment->user_id); ?>
<div class="comment_wrap" data-comment-id="<?= $comment->id ?>">
	<div class="header">
		<img class="avatar" src="<?= Helper::get_gravatar($user['email']) ?>" alt="" width="80" height="80">
		<div class="author">
			<a rel="nofollow" href="/profile/<?= $user['username'] ?>"><?= $user['fullname'] ?></a>
			<span class="like"></span>
		</div>
		<div class="created">
			<span title="<?= Helper::rusDate("l, j F H:i",$comment->created) ?>"><?= Yii::app()->format->timeago($comment->created); ?></span>
			<?php if($comment->reply) {
				$replr = UsrModule::user($comment->reply->user_id); ?>
			<?= Yii::t('form','in response to') ?>
			<span class="show_reply">@<?= $replr['username'] ?></span>
			<div class="reply">
				<a rel="nofollow" href="/profile/<?= $replr['username'] ?>"><?= $replr['fullname'] ?></a><br />
				<?= $comment->reply->text ?>
			</div>
			<?php } ?>
		</div>
	</div>
	<div class="body"><?= nl2br($comment->text) ?></div>
	<a href="#" class="addcomm"><?= Yii::t('form','Reply') ?></a>
</div>