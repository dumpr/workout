<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= Yii::app()->language ?>" lang="<?= Yii::app()->language ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="language" content="<?= Yii::app()->language ?>">
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

    <div id="header">
        <div id="usermenu"><?php $this->widget('zii.widgets.CMenu',array(
            'items'=>array(
                array('label'=>Yii::t('site','Login'), 'url'=>array('/usr/default/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>Yii::t('site','Profile ({username})', array('{username}'=>Yii::app()->user->name)), 'url'=>array('/usr/default/profile'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>Yii::t('site','AdminPanel'), 'url'=>array('../../admin'), 'visible'=>Yii::app()->user->checkAccess('adminPanel')),
                array('label'=>Yii::t('site','Logout'), 'url'=>array('/usr/default/logout'), 'visible'=>!Yii::app()->user->isGuest),
            ),
        )); ?><!-- usermenu -->
        </div>
        <?php $this->widget('LanguageSwitcherWidget'); ?>
        <div id="logo"><a href="<?= $this->createUrl('/site/index') ?>"><?= Yii::t('site',Yii::app()->name) ?></a></div>
    </div><!-- header -->

    <div id="mainmenu"><?= isset($this->mainmenu) ? $this->widget('zii.widgets.CMenu',array(
            'items'=> array_merge($this->mainmenu,array(
                // array('label'=>Yii::t('site','Home'), 'url'=>array('/site/index')),
                // array('label'=>Yii::t('site','About'), 'url'=>array('/site/page', 'view'=>'about')),
                array('label'=>Yii::t('site','Contact'), 'url'=>array('/site/contact'))
            )),
        )) : '' ?>
    </div><!-- mainmenu -->

    <?php if(isset($this->breadcrumbs)):?>
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                'links'=>$this->breadcrumbs,
        )); ?><!-- breadcrumbs -->
    <?php endif?>

    <?php echo $content; ?>

    <div class="clear"></div>

    <div id="footer">
        <?= Yii::t('site','Copyright &copy; {n} by {sitename}.',array('{n}'=>date('Y'),'{sitename}'=>Yii::app()->name)) ?><br/>
        <?= Yii::t('site','All Rights Reserved.') ?><br/>
        <?= Yii::powered(); ?><br/>
        <?= dbginfo() ?>
    </div><!-- footer -->

</div><!-- page -->
<?php /* ?><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script><?php */ ?>
    <script src="../../dev/jquery.min.js"></script>

    <?php
        $package = array(
                'basePath'    => 'webroot.themes.bootstrap.assets',
                'css'         => array('css/bootstrap.min.css','css/bootstrap-responsive.min.css'), // ,'css/main.css'
                'js'          => array('js/bootstrap.min.js'), //,'js/main.js'
                //'depends'     => array('jquery'),
        );
        Yii::app()->clientScript->addPackage('bootstrap',$package)->registerPackage('bootstrap');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/main.js');
        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/main.css');
    ?>
</body>
</html>
