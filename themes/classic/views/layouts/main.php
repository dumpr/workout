<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= Yii::app()->language ?>" lang="<?= Yii::app()->language ?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/form.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<?php $this->widget('LanguageSwitcherWidget'); ?>
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>Yii::t('site','Home'), 'url'=>array('/site/index')),
				array('label'=>Yii::t('site','About'), 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>Yii::t('site','Contact'), 'url'=>array('/site/contact')),
				array('label'=>Yii::t('site','Login'), 'url'=>array('/usr/default/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>Yii::t('site','Profile ({username})', array('{username}'=>Yii::app()->user->name)), 'url'=>array('/usr/default/profile'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>Yii::t('site','AdminPanel'), 'url'=>array('/admin'), 'visible'=>Yii::app()->user->checkAccess('adminPanel')),
				array('label'=>Yii::t('site','Logout'), 'url'=>array('/usr/default/logout'), 'visible'=>!Yii::app()->user->isGuest),
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		<?= Yii::t('site','Copyright &copy; {n} by {sitename}.',array('{n}'=>date('Y'),'{sitename}'=>Yii::app()->name)) ?><br/>
		<?= Yii::t('site','All Rights Reserved.') ?><br/>
		<?= Yii::powered(); ?><br/>
		<?= dbginfo() ?>
	</div><!-- footer -->

</div><!-- page -->
	<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
	<script src="<?= Yii::app()->theme->baseUrl ?>/js/main.js" type="text/javascript"></script>

</body>
</html>
